// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "Ascension.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Ascension, "Ascension" );
 
// Setup the Ascension general logging category
DEFINE_LOG_CATEGORY(LogAscension);