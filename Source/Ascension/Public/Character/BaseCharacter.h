#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Perception/AIPerceptionStimuliSourceComponent.h"
#include "AbilitySystemInterface.h"
#include "Animation/AnimInstance.h"
#include "GameplayEffectTypes.h"
#include "../Ascension.h"
#include "Core/AscensionCore.h"
#include "DataTable/DataTableInitializable.h"
#include "Ability/Effect/StatRegenEffect.h"
#include "BaseCharacter.generated.h"

UENUM(BlueprintType)
enum class ECharacterActions : uint8
{
	Recover UMETA(DisplayName = "Recover"),
	Sprint UMETA(DisplayName = "Sprint")
};

UCLASS(ABSTRACT)
class AUBaseCharacter : public ACharacter, public IAbilitySystemInterface, public IDataTableInitializable
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AUBaseCharacter();

	/** AI Perception Stimuli */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI, meta = (AllowPrivateAccess = "true"))
	UAIPerceptionStimuliSourceComponent* PerceptionStimuliComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Abilities, meta = (AllowPrivateAccess = "true"))
	class UCharacterAbilitySystemComponent* AbilitySystem;

	// IAbilitySystemInterface
	class UAbilitySystemComponent* GetAbilitySystemComponent() const override;
	void SetAbilitySystemComponent(UCharacterAbilitySystemComponent* NewAbilitySystem);
	// end IAbilitySystemInterface

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Stats, meta = (AllowPrivateAccess = "true"))
	class UBaseStatSet* BaseStatSet;
	class UBaseStatSet* GetBaseStatSet();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Stats)
	float CurrentHealthPercentage;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Stats)
	float CurrentEnergyPercentage;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Stats)
	float CurrentEtherPercentage;

	void UpdateStatPercentages();

	UFUNCTION(BlueprintCallable)
	bool IsAlive() const;

	UFUNCTION(BlueprintCallable)
	virtual FVector GetMovementVector();

	// Called before executing any actions(movement, abilities etc)
	virtual void PreActionExecution();

	void StartRecover();

	void StopRecover();

	virtual void OnCharacterHit();

	virtual void OnCharacterDeath();

protected:
	/******** Initialized from a data table, plays when hit *******/
	UPROPERTY()
	TSoftObjectPtr<UAnimMontage> HitMontage;

	UPROPERTY()
	FName HitMontageStartSection;

	UPROPERTY()
	float HitMontagePlayRate;
	/****************************************************/

	// Abilities with these tags will be interrupted by other actions
	FGameplayTagContainer InterruptTagContainer;

	// Handle to the OnHealthAttributeChanged callback
	FDelegateHandle HealthChangedHandle;

	// Handle to the OnHealthAttributeChanged callback
	FDelegateHandle EnergyChangedHandle;

	// Handle to the OnHealthAttributeChanged callback
	FDelegateHandle EtherChangedHandle;

	// Callback for on health change
	virtual void HealthChanged(const FOnAttributeChangeData& Data);

	// Callback for on health change
	virtual void EnergyChanged(const FOnAttributeChangeData& Data);

	// Callback for on health change
	virtual void EtherChanged(const FOnAttributeChangeData& Data);

	// Handle post death tasks like destroy and respawn
	virtual void FinishDeath();

	void Tick(float DeltaTime) override;

	void BeginPlay() override;

private:
	FTimerHandle DeathTimerHandle;

	UPROPERTY()
	UStatRegenEffect* StatRegenEffect;

	UPROPERTY()
	TMap<EAffinity, int32> Affinities;
};