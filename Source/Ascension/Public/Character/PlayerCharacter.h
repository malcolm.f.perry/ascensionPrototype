#pragma once

#include "CoreMinimal.h"
#include "BaseCharacter.h"
#include "PlayerCharacter.generated.h"

UENUM(BlueprintType)
enum class AbilityInputEnum : uint8
{
	AbilitySlot1 UMETA(DisplayName = "AbilitySlot1"),
	AbilitySlot2 UMETA(DisplayName = "AbilitySlot2"),
	AbilitySlot3 UMETA(DisplayName = "AbilitySlot3"),
	AbilitySlot4 UMETA(DisplayName = "AbilitySlot4"),
	AbilitySlot5 UMETA(DisplayName = "AbilitySlot5")
};

UCLASS(config = Game)
class AUPlayerCharacter : public AUBaseCharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;

public:
	AUPlayerCharacter();

	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseLookUpRate;

protected:
	/** Called for forwards/backward input */
	void MoveForwardBackward(float Value);

	/** Called for side to side input */
	void MoveLeftRight(float Value);

	/**
	 * Called via input to turn at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	void PlayerJump();

	void PlayerStopJumping();

	void Roll();

	void StartSprint();

	void StopSprint();

	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

	void TogglePlayerInput(bool Enable);

	void PossessedBy(AController* NewController) override;

	void BeginPlay() override;

public:
	FVector GetMovementVector() override;

	void DisablePlayerInput();

	void EnablePlayerInput();

private:
	UPROPERTY()
	FTimerHandle AnimTimerHandle;

	UPROPERTY()
	bool bShouldSprint = false;

	UPROPERTY()
	bool bIsSprinting = false;
};