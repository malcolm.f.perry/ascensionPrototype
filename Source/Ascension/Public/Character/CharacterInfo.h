#pragma once

#include "CoreMinimal.h"
#include "Core/AscensionCore.h"
#include "DataTable/DataTableInitializable.h"
#include "CharacterInfo.generated.h"

/**
 * This class holds the information used to initialize the character and its
 * abilities. Stored on the GameInstance and read from the Character's BeginPlay.
 */
UCLASS(BlueprintType)
class ASCENSION_API UCharacterInfo : public UObject, public IDataTableInitializable
{
	GENERATED_BODY()

public:
	UCharacterInfo(const FObjectInitializer& ObjectInitializer);

	UFUNCTION(BlueprintCallable)
	bool AddAffinity(EAffinity Affinity, int32 Value);

	UFUNCTION(BlueprintCallable)
	int32 RemoveAffinity(EAffinity Affinity);

	UFUNCTION(BlueprintCallable)
	TMap<EAffinity, int32> GetAffinities() const;

	UFUNCTION(BlueprintCallable)
	bool AddAttributes(int32 Index, UAbilityAttributes* Attributes);

	UFUNCTION(BlueprintCallable)
	bool RemoveAttributes(int32 Index);

	UFUNCTION(BlueprintCallable)
	TArray<UAbilityAttributes*> GetAttributesList() const;

protected:
	void OnGameInstanceStart();

private:
	UPROPERTY()
	TMap<EAffinity, int32> Affinities;

	UPROPERTY()
	TArray<UAbilityAttributes*> AttributesList;
};