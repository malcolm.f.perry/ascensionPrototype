#pragma once

#include "CoreMinimal.h"
#include "BaseCharacter.h"
#include "Components/WidgetComponent.h"
#include "Perception/AIPerceptionComponent.h"
#include "Ability/BaseGameplayAbility.h"
#include "AICharacter.generated.h"

UCLASS()
class ASCENSION_API AUAICharacter : public AUBaseCharacter
{
	GENERATED_BODY()
	
public:
	AUAICharacter();

	/** AI Perception */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI, meta = (AllowPrivateAccess = "true"))
	UAIPerceptionComponent* PerceptionComponent;

	UPROPERTY(EditDefaultsOnly, Category = Ability)
	TMap<EAffinity, int32> AffinitiesMap;

	FVector GetMovementVector() override;

	virtual void GetActorEyesViewPoint(FVector& OutLocation, FRotator& OutRotation) const override;

protected:
	void AddAbility(int32 Index, TSubclassOf<UBaseGameplayAbility> AbilityType, int32 Power, int32 Penetration, int32 Range, int32 Speed, 
		int32 Size, int32 CastTime, int32 Duration, int32 Delay, int32 Cooldown, int32 Cost, EAffinity Affinity);

	virtual void BeginPlay() override;
};