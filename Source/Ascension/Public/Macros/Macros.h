#pragma once

#include "CoreMinimal.h"

/**
 * 
 */
class ASCENSION_API Macros
{
public:
	static void PrintDebugMessage(float DisplayTime, FColor TextColor, FString Message)
	{
		if (GEngine)
		{
			GEngine->AddOnScreenDebugMessage(-1, DisplayTime, TextColor, Message);
		}
	}

	static void PrintDebugMessage(FString Message)
	{
		Macros::PrintDebugMessage(5, FColor::Red, Message);
	}

	static FString FloatToString(float InFloat)
	{
		return FString::SanitizeFloat(InFloat);
	}
};