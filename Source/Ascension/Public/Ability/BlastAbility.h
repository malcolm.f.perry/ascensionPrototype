#pragma once

#include "CoreMinimal.h"
#include "BaseGameplayAbility.h"
#include "BlastAbility.generated.h"

UCLASS()
class ASCENSION_API UBlastAbility : public UBaseGameplayAbility
{
	GENERATED_BODY()

public:
	UBlastAbility(const FObjectInitializer& ObjectInitializer);

protected:
	virtual FVector GetSpawnTransformLocation(const AUBaseCharacter* Character) override;

	virtual FRotator GetSpawnTransformRotation(const AUBaseCharacter* Character) override;

	UFUNCTION()
	void OnActorHit(FGameplayEventData GameplayEventData) override;

	UFUNCTION()
	void OnActorOverlap(FGameplayEventData GameplayEventData) override;

	UFUNCTION()
	void OnActorDestroyed(FGameplayEventData GameplayEventData) override;

public:
	void OnAvatarSet(const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilitySpec& Spec) override;
};