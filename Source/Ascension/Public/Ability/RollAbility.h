#pragma once

#include "CoreMinimal.h"
#include "Ability/BaseGameplayAbility.h"
#include "RollAbility.generated.h"

UCLASS()
class ASCENSION_API URollAbility : public UBaseGameplayAbility
{
	GENERATED_BODY()

public:
	URollAbility(const FObjectInitializer& ObjectInitializer);

private:
	UPROPERTY()
	UAbilityAttributes* Attributes;

protected:
	void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
		const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;

	void EndRollAbility();

public:
	void OnAvatarSet(const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilitySpec& Spec) override;
};