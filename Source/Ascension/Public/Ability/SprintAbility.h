#pragma once

#include "CoreMinimal.h"
#include "Ability/BaseGameplayAbility.h"
#include "Ability/Effect/PeriodicEnergyCostEffect.h"
#include "Ability/Effect/SprintMovementSpeedEffect.h"
#include "SprintAbility.generated.h"

UCLASS()
class ASCENSION_API USprintAbility : public UBaseGameplayAbility
{
	GENERATED_BODY()
	
public:
	USprintAbility(const FObjectInitializer& ObjectInitializer);

private:
	UPROPERTY()
	UPeriodicEnergyCostEffect* SprintCostEffect;

	UPROPERTY()
	FActiveGameplayEffectHandle SprintCostEffectHandle;

	UPROPERTY()
	USprintMovementSpeedEffect* SprintMovementSpeedEffect;

	UPROPERTY()
	FActiveGameplayEffectHandle SprintMovementSpeedEffectHandle;

	UPROPERTY()
	UAbilityAttributes* Attributes;

protected:
	void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
		const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;

	void EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
		const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateCancelAbility, bool bWasCancelled) override;

public:
	void OnAvatarSet(const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilitySpec& Spec) override;
};