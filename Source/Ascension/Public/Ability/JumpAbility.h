#pragma once

#include "CoreMinimal.h"
#include "Ability/BaseGameplayAbility.h"
#include "JumpAbility.generated.h"

UCLASS()
class ASCENSION_API UJumpAbility : public UBaseGameplayAbility
{
	GENERATED_BODY()

public:
	UJumpAbility(const FObjectInitializer& ObjectInitializer);

private:
	UPROPERTY()
	UAbilityAttributes* Attributes;

protected:
	void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
		const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;

	void EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
		const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateCancelAbility, bool bWasCancelled) override;

public:
	void OnAvatarSet(const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilitySpec& Spec) override;
};