#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/PrimitiveComponent.h"
#include "GameplayEffectTypes.h"
#include "../Ascension.h"
#include "Core/AscensionCore.h"
#include "DataTable/DataTableInitializable.h"
#include "Perception/AIPerceptionStimuliSourceComponent.h"
#include "Ability/Effect/BaseGameplayEffect.h"
#include "BaseAbilityActor.generated.h"

UCLASS()
class ASCENSION_API ABaseAbilityActor : public AActor, public IDataTableInitializable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABaseAbilityActor();

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	UAIPerceptionStimuliSourceComponent* StimuliSourceComponent;

	// The GameplayEffect class for damage
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ActorEffects")
	TSubclassOf<UBaseGameplayEffect> DamageEffect;

	// The GameplayEffect handle for damage
	UPROPERTY()
	FGameplayEffectSpecHandle DamageEffectSpecHandle;

	FActorAttributes ActorAttributes;

	FVector OriginLocation;

	virtual void FinishExecute();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
	virtual void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit);

	UFUNCTION()
	virtual void OnOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, 
		bool bFromSweep, const FHitResult &SweepResult);

	virtual void DoHit(AActor* OtherActor, const FHitResult& HitResult, FGameplayTag EventTag);

	virtual void DestroyAndSendEvent(AActor* OtherActor, AActor* AbilityInstigator, FGameplayTag EventTag);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void LifeSpanExpired() override;

	virtual void ExecuteAbility(FActorAttributes NewActorAttributes, const FVector& Direction);
};