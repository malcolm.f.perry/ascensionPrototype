#pragma once

#include "CoreMinimal.h"
#include "Ability/Actor/BaseAbilityActor.h"
//#include "Components/BoxComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "BeamProjectile.generated.h"

UCLASS()
class ASCENSION_API ABeamProjectile : public ABaseAbilityActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ABeamProjectile();

	UPROPERTY(VisibleDefaultsOnly, Category = Projectile)
	USceneComponent* DefaultSceneComponent;

	UPROPERTY(VisibleDefaultsOnly, Category = Projectile)
	UStaticMeshComponent* StaticMeshComponent;

	UPROPERTY(VisibleAnywhere, Category = "ActorEffects")
	UParticleSystemComponent* ParticleSystemComponent;

	void ExecuteAbility(FActorAttributes NewActorAttributes, const FVector& Direction) override;

private:
	float DeltaSeconds = 0.0f;

	// Current Actors being overlapped
	UPROPERTY()
	TMap<uint32, AActor*> OverlappedActors;

protected:
	UPROPERTY(VisibleAnywhere, Category = "ActorEffects")
	TSoftObjectPtr<UParticleSystem> ParticleSystem;

	UPROPERTY(VisibleAnywhere, Category = "ActorEffects")
	TSoftObjectPtr<UStaticMesh> StaticMesh;

	void FinishExecute() override;

	void BeginPlay() override;

	void OnOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex,
		bool bFromSweep, const FHitResult& SweepResult) override;

	UFUNCTION()
	void OnEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* Other, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	void DoHit(AActor* OtherActor, const FHitResult& HitResult, FGameplayTag EventTag) override;

	void Tick(float DeltaTime) override;
};