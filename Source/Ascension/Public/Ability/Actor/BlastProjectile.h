#pragma once

#include "CoreMinimal.h"
#include "BaseAbilityActor.h"
#include "Components/SphereComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "BlastProjectile.generated.h"

UCLASS()
class ASCENSION_API ABlastProjectile : public ABaseAbilityActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABlastProjectile();

	UPROPERTY(VisibleDefaultsOnly, Category = Projectile)
	USphereComponent* CollisionComponent;

	UPROPERTY(VisibleAnywhere, Category = Movement)
	UProjectileMovementComponent* ProjectileMovementComponent;

	UPROPERTY(VisibleAnywhere, Category = "ActorEffects")
	UParticleSystemComponent* ParticleSystemComponent;

	void ExecuteAbility(FActorAttributes NewActorAttributes, const FVector& Direction) override;

protected:
	UPROPERTY(VisibleAnywhere, Category = "ActorEffects")
	TSoftObjectPtr<UParticleSystem> ParticleSystem;

	UPROPERTY(VisibleAnywhere, Category = "ActorEffects")
	TSoftObjectPtr<UParticleSystem> OnHitFX;

	void FinishExecute() override;

	void BeginPlay() override;

	void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit) override;

	void OnOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex,
		bool bFromSweep, const FHitResult &SweepResult) override;

	void DoHit(AActor* OtherActor, const FHitResult& HitResult, FGameplayTag EventTag) override;
};