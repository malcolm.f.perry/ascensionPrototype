#pragma once

#include "CoreMinimal.h"
#include "Ability/BaseGameplayAbility.h"
#include "Ability/Effect/PeriodicEtherCostEffect.h"
#include "BeamAbility.generated.h"

UCLASS()
class ASCENSION_API UBeamAbility : public UBaseGameplayAbility
{
	GENERATED_BODY()

public:
	UBeamAbility(const FObjectInitializer& ObjectInitializer);

private:
	UPROPERTY()
	UPeriodicEtherCostEffect* EtherCostEffect;

	UPROPERTY()
	FActiveGameplayEffectHandle EtherCostEffectHandle;

protected:
	virtual FVector GetSpawnTransformLocation(const AUBaseCharacter* Character) override;

	virtual FRotator GetSpawnTransformRotation(const AUBaseCharacter* Character) override;

	void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
		const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;

	UFUNCTION()
	void OnActorHit(FGameplayEventData GameplayEventData) override;

	UFUNCTION()
	void OnActorOverlap(FGameplayEventData GameplayEventData) override;

	UFUNCTION()
	void OnActorDestroyed(FGameplayEventData GameplayEventData) override;

	void EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
		const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateCancelAbility, bool bWasCancelled) override;

public:
	void OnAvatarSet(const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilitySpec& Spec) override;
};