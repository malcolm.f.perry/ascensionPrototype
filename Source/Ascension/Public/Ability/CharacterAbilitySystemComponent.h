#pragma once

#include "CoreMinimal.h"
#include "AbilitySystemComponent.h"
#include "Ability/BaseGameplayAbility.h"
#include "Ability/AbilityAttributes.h"
#include "DataTable/DataTableInitializable.h"
#include "CharacterAbilitySystemComponent.generated.h"

UCLASS()
class ASCENSION_API UCharacterAbilitySystemComponent : public UAbilitySystemComponent, public IDataTableInitializable
{
	GENERATED_BODY()

public:
	UCharacterAbilitySystemComponent(const FObjectInitializer& ObjectInitializer);

	UPROPERTY()
	UBaseGameplayAbility* CurrentActiveAbility;

	UFUNCTION(BlueprintCallable)
	UAbilityAttributes* GetAttributesForAbility(int32 Index) const;

	// Check if the ability at the provided index has the given descriptor
	UFUNCTION(BlueprintCallable)
	bool HasAbilityDescriptor(int32 Index, EAbilityDescriptor Descriptor);

	UFUNCTION(BlueprintCallable)
	bool HasAbilityAtIndex(int32 Index);

	// Check if the provided ability handle is located at the provided index
	UFUNCTION(BlueprintCallable)
	bool IsAbilityAtIndex(int32 Index, FGameplayAbilitySpecHandle Handle);

	// Check if an ability at the provided index is available to activate
	UFUNCTION(BlueprintCallable)
	bool IsAbilityAtIndexAvailable(int32 Index);

	UFUNCTION(BlueprintCallable)
	bool ActivateAbilityAtIndex(int32 Index);

	UFUNCTION()
	bool ActivateRecoverAbility();

	UFUNCTION()
	void EndRecoverAbility();

	UFUNCTION()
	bool ActivateSprintAbility();

	UFUNCTION()
	void EndSprintAbility();

	UFUNCTION()
	bool ActivateRollAbility();

	UFUNCTION()
	bool ActivateJumpAbility();

	UFUNCTION()
	void EndJumpAbility();

	UFUNCTION(BlueprintCallable)
	bool AddAbilityToIndex(int32 Slot, UAbilityAttributes* Attributes);

	UFUNCTION(BlueprintCallable)
	bool RemoveAbilityFromIndex(int32 Index);

	// Called when a button is pressed to activate an ability
	virtual void AbilityLocalInputPressed(int32 InputID);

	// Called when a button is released to cancel an ability
	virtual void AbilityLocalInputReleased(int32 InputID);

	void CancelEnergyAbilities();

	void CancelEtherAbilities();

protected:
	FGameplayAbilitySpecHandle GiveBaseGameplayAbility(TSubclassOf<UBaseGameplayAbility> AbilityClass, int32 Level, int32 Id);

	virtual void BeginPlay() override;

private:
	/*** This is only used to initialize the abilities then the object is deleted from the array/memory ***/
	UPROPERTY()
	TArray<UAbilityAttributes*> ActiveAbilityAttributes;
	/*******************************************************************************************************/

	UPROPERTY()
	TArray<TSubclassOf<class UBaseGameplayAbility>> ActiveAbilities;

	UPROPERTY()
	TArray<FGameplayAbilitySpecHandle> ActiveAbilityHandles;

	UPROPERTY()
	FGameplayAbilitySpecHandle RecoverAbilityHandle;

	UPROPERTY()
	FGameplayAbilitySpecHandle SprintAbilityHandle;

	UPROPERTY()
	FGameplayAbilitySpecHandle RollAbilityHandle;

	UPROPERTY()
	FGameplayAbilitySpecHandle JumpAbilityHandle;

	bool ShouldCancelIfReleased(TSubclassOf<class UBaseGameplayAbility> Ability);
};