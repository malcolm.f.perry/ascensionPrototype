#pragma once

#include "CoreMinimal.h"
#include "Ability/Effect/BaseGameplayEffect.h"
#include "PeriodicEnergyCostEffect.generated.h"

UCLASS()
class ASCENSION_API UPeriodicEnergyCostEffect : public UBaseGameplayEffect
{
	GENERATED_BODY()

public:
	UPeriodicEnergyCostEffect(const FObjectInitializer& ObjectInitializer);
};