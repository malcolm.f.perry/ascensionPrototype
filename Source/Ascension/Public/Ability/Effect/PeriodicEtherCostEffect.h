#pragma once

#include "CoreMinimal.h"
#include "Ability/Effect/BaseGameplayEffect.h"
#include "PeriodicEtherCostEffect.generated.h"

UCLASS()
class ASCENSION_API UPeriodicEtherCostEffect : public UBaseGameplayEffect
{
	GENERATED_BODY()

public:
	UPeriodicEtherCostEffect(const FObjectInitializer& ObjectInitializer);
};