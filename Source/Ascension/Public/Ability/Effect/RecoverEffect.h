#pragma once

#include "CoreMinimal.h"
#include "Ability/Effect/BaseGameplayEffect.h"
#include "RecoverEffect.generated.h"

UCLASS()
class ASCENSION_API URecoverEffect : public UBaseGameplayEffect
{
	GENERATED_BODY()

public:
	URecoverEffect(const FObjectInitializer& ObjectInitializer);
};