#pragma once

#include "CoreMinimal.h"
#include "Ability/Effect/BaseGameplayEffect.h"
#include "EtherCostEffect.generated.h"

UCLASS(DefaultToInstanced)
class ASCENSION_API UEtherCostEffect : public UBaseGameplayEffect
{
	GENERATED_BODY()
	
public:
	UEtherCostEffect(const FObjectInitializer& ObjectInitializer);
};