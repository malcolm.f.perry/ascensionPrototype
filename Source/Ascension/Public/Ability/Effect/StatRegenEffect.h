#pragma once

#include "CoreMinimal.h"
#include "Ability/Effect/BaseGameplayEffect.h"
#include "StatRegenEffect.generated.h"

UCLASS()
class ASCENSION_API UStatRegenEffect : public UBaseGameplayEffect
{
	GENERATED_BODY()

public:
	UStatRegenEffect(const FObjectInitializer& ObjectInitializer);
};