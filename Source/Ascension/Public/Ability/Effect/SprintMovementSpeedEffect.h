#pragma once

#include "CoreMinimal.h"
#include "Ability/Effect/BaseGameplayEffect.h"
#include "SprintMovementSpeedEffect.generated.h"

UCLASS()
class ASCENSION_API USprintMovementSpeedEffect : public UBaseGameplayEffect
{
	GENERATED_BODY()

public:
	USprintMovementSpeedEffect(const FObjectInitializer& ObjectInitializer);
};