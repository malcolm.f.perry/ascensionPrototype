#pragma once

#include "CoreMinimal.h"
#include "GameplayModMagnitudeCalculation.h"
#include "HealthRegenCalculation.generated.h"

UCLASS()
class ASCENSION_API UHealthRegenCalculation : public UGameplayModMagnitudeCalculation
{
	GENERATED_BODY()
	
public:
	UHealthRegenCalculation(const FObjectInitializer& ObjectInitializer);

	float CalculateBaseMagnitude_Implementation(const FGameplayEffectSpec & Spec) const override;

private:
	FGameplayEffectAttributeCaptureDefinition HealthRegenRateDef;
};