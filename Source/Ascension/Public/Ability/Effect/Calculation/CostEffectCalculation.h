#pragma once

#include "CoreMinimal.h"
#include "GameplayModMagnitudeCalculation.h"
#include "CostEffectCalculation.generated.h"

/**
 * Class responsible for retrieving the Cost value from AbilityAttributes
 */
UCLASS()
class ASCENSION_API UCostEffectCalculation : public UGameplayModMagnitudeCalculation
{
	GENERATED_BODY()

public:
	UCostEffectCalculation(const FObjectInitializer& ObjectInitializer);

	float CalculateBaseMagnitude_Implementation(const FGameplayEffectSpec & Spec) const override;
};