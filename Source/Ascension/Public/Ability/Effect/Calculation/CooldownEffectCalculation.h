#pragma once

#include "CoreMinimal.h"
#include "GameplayModMagnitudeCalculation.h"
#include "CooldownEffectCalculation.generated.h"

/**
 * Class responsible for retrieving the Cooldown value from AbilityAttributes
 */
UCLASS()
class ASCENSION_API UCooldownEffectCalculation : public UGameplayModMagnitudeCalculation
{
	GENERATED_BODY()

public:
	UCooldownEffectCalculation(const FObjectInitializer& ObjectInitializer);

	float CalculateBaseMagnitude_Implementation(const FGameplayEffectSpec & Spec) const override;
};