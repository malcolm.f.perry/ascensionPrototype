// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayModMagnitudeCalculation.h"
#include "EtherRegenCalculation.generated.h"

/**
 * 
 */
UCLASS()
class ASCENSION_API UEtherRegenCalculation : public UGameplayModMagnitudeCalculation
{
	GENERATED_BODY()
	
public:
	UEtherRegenCalculation(const FObjectInitializer& ObjectInitializer);

	float CalculateBaseMagnitude_Implementation(const FGameplayEffectSpec & Spec) const override;

private:
	FGameplayEffectAttributeCaptureDefinition EtherRegenRateDef;
};
