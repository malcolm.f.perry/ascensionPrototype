#pragma once

#include "CoreMinimal.h"
#include "GameplayModMagnitudeCalculation.h"
#include "DamageEffectCalculation.generated.h"

/**
 * Class responsible for retrieving the Power value from AbilityAttributes
 */
UCLASS()
class ASCENSION_API UDamageEffectCalculation : public UGameplayModMagnitudeCalculation
{
	GENERATED_BODY()
	
public:
	UDamageEffectCalculation(const FObjectInitializer& ObjectInitializer);

	float CalculateBaseMagnitude_Implementation(const FGameplayEffectSpec & Spec) const override;
};