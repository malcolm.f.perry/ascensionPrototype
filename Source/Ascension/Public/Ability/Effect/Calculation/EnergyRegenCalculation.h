#pragma once

#include "CoreMinimal.h"
#include "GameplayModMagnitudeCalculation.h"
#include "EnergyRegenCalculation.generated.h"

/**
 * 
 */
UCLASS()
class ASCENSION_API UEnergyRegenCalculation : public UGameplayModMagnitudeCalculation
{
	GENERATED_BODY()

public:
	UEnergyRegenCalculation(const FObjectInitializer& ObjectInitializer);

	float CalculateBaseMagnitude_Implementation(const FGameplayEffectSpec & Spec) const override;

private:
	FGameplayEffectAttributeCaptureDefinition EnergyRegenRateDef;
};