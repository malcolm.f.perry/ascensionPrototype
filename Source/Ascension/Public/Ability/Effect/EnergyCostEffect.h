#pragma once

#include "CoreMinimal.h"
#include "Ability/Effect/BaseGameplayEffect.h"
#include "EnergyCostEffect.generated.h"

UCLASS()
class ASCENSION_API UEnergyCostEffect : public UBaseGameplayEffect
{
	GENERATED_BODY()

public:
	UEnergyCostEffect(const FObjectInitializer& ObjectInitializer);
};