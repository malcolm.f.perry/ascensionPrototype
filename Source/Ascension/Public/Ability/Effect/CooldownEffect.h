#pragma once

#include "CoreMinimal.h"
#include "Ability/Effect/BaseGameplayEffect.h"
#include "CooldownEffect.generated.h"

UCLASS()
class ASCENSION_API UCooldownEffect : public UBaseGameplayEffect
{
	GENERATED_BODY()

public:
	UCooldownEffect(const FObjectInitializer& ObjectInitializer);
};