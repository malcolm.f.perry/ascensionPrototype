#pragma once

#include "CoreMinimal.h"
#include "GameplayEffect.h"
#include "BaseGameplayEffect.generated.h"

UCLASS()
class ASCENSION_API UBaseGameplayEffect : public UGameplayEffect
{
	GENERATED_BODY()
	
protected:
	void AddModifierInfo(FGameplayAttribute Attribute, FGameplayEffectModifierMagnitude Magnitude,
		EGameplayModOp::Type Operation);

	void AddAdditiveModifierInfo(FGameplayAttribute Attribute, FGameplayEffectModifierMagnitude Magnitude);

	void AddMultiplicativeModifierInfo(FGameplayAttribute Attribute, FGameplayEffectModifierMagnitude Magnitude);

	void AddCalculationFloatModifierInfo(FGameplayAttribute Attribute, TSubclassOf<UGameplayModMagnitudeCalculation> CalculationClass);
};