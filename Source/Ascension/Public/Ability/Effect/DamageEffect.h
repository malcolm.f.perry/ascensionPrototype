#pragma once

#include "CoreMinimal.h"
#include "Ability/Effect/BaseGameplayEffect.h"
#include "DamageEffect.generated.h"

UCLASS()
class ASCENSION_API UDamageEffect : public UBaseGameplayEffect
{
	GENERATED_BODY()

public:
	UDamageEffect(const FObjectInitializer& ObjectInitializer);
};