#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbility.h"
#include "Character/BaseCharacter.h"
#include "Ability/Actor/BaseAbilityActor.h"
#include "Math/Vector.h"
#include "Math/Rotator.h"
#include "Animation/AnimInstance.h"
#include "Ability/Effect/CooldownEffect.h"
#include "Ability/Effect/DamageEffect.h"
#include "../Ascension.h"
#include "BaseGameplayAbility.generated.h"

UCLASS()
class ASCENSION_API UBaseGameplayAbility : public UGameplayAbility, public IDataTableInitializable
{
	GENERATED_BODY()
	
public:
	UBaseGameplayAbility(const FObjectInitializer& ObjectInitializer);

	UFUNCTION(BlueprintCallable)
	bool Initialize(UAbilityAttributes* NewAttributes);

	UFUNCTION(BlueprintCallable)
	int32 GetPower() const;

	UFUNCTION(BlueprintCallable)
	int32 GetPenetration() const;

	UFUNCTION(BlueprintCallable)
	int32 GetRange() const;

	UFUNCTION(BlueprintCallable)
	int32 GetSpeed() const;

	UFUNCTION(BlueprintCallable)
	int32 GetSize() const;

	UFUNCTION(BlueprintCallable)
	int32 GetCastTime() const;

	UFUNCTION(BlueprintCallable)
	int32 GetDuration() const;

	UFUNCTION(BlueprintCallable)
	int32 GetDelay() const;

	UFUNCTION(BlueprintCallable)
	int32 GetCooldown() const;

	UFUNCTION(BlueprintCallable)
	int32 GetCost() const;

	UFUNCTION(BlueprintCallable)
	EAffinity GetAffinity() const;

	UFUNCTION(BlueprintCallable)
	EAbilityDescriptor GetAbilityDescriptor() const;

	UFUNCTION(BlueprintCallable)
	FActorAttributes GetActorAttributes();

	// This is the animation that will play when the ability is activated
	UPROPERTY(EditDefaultsOnly, Category = "AbilityConfig")
	TSoftObjectPtr<UAnimMontage> AnimMontage;

	// Start section of the montage
	UPROPERTY(EditDefaultsOnly, Category = "AbilityConfig")
	FName AnimMontageStartSection = "start";

	// The rate the montage will play at
	UPROPERTY(EditDefaultsOnly, Category = "AbilityConfig")
	float MontagePlayRate = 1.f;

	// This is the actor that will be spawned for this ability
	UPROPERTY(EditDefaultsOnly, Category = "AbilityConfig")
	TSubclassOf<ABaseAbilityActor> AbilityActor;

	UPROPERTY(EditDefaultsOnly, Category = "AbilityConfig")
	float SpawnActorDelay = 0.5f;

	UFUNCTION()
	virtual void SpawnActor();

	virtual UGameplayEffect* GetCostGameplayEffect() const override;

	virtual UGameplayEffect* GetCooldownGameplayEffect() const override;

	virtual UGameplayEffect* GetDamageGameplayEffect() const;

	virtual bool ShouldCancelIfInputReleased();

	// Inject our custom CooldownTag so this Ability can look for it on the parent AbilitySystemComponent
	virtual const FGameplayTagContainer* GetCooldownTags() const override;

	// Make sure that our custom CooldownTag is applied
	virtual void ApplyCooldown(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo * ActorInfo, 
		const FGameplayAbilityActivationInfo ActivationInfo) const override;

private:
	// Whether the initialize function has been called
	bool bWasInitialized = false;

	UPROPERTY()
	FGameplayTagContainer CooldownTagContainer;

	/***** Internal attributes for the Ability, initialized during OnAvatarSet using an AbilityAttributes object *****/
	UPROPERTY()
	int32 Power = 0;

	UPROPERTY()
	int32 Penetration = 0; //0-100%

	UPROPERTY()
	int32 Range = 0; //cm

	UPROPERTY()
	int32 Speed = 0; //cm/s

	UPROPERTY()
	int32 Size = 0; //cm

	UPROPERTY()
	int32 CastTime = 0; //seconds

	UPROPERTY()
	int32 Duration = 0; //seconds

	UPROPERTY()
	int32 Delay = 0; //seconds

	UPROPERTY()
	int32 Cooldown = 0; //seconds

	UPROPERTY()
	int32 Cost = 0;

	UPROPERTY()
	EAffinity Affinity = EAffinity::Fire;

	UPROPERTY()
	EAbilityDescriptor AbilityDescriptor = EAbilityDescriptor::Attack;

	UPROPERTY()
	FActorAttributes ActorAttributes;
	/**************************************************************************************************/

protected:
	bool bHasCostEffect = true;

	bool bShouldCancelIfInputReleased = false;

	UPROPERTY()
	FTimerHandle AnimTimerHandle;

	UPROPERTY()
	AUBaseCharacter* CharacterRef;

	UPROPERTY()
	ABaseAbilityActor* SpawnedAbilityActor;

	UPROPERTY()
	UBaseGameplayEffect* CostEffect;

	UPROPERTY()
	UCooldownEffect* CooldownEffect;

	UPROPERTY()
	UDamageEffect* DamageEffect;

	virtual FVector GetSpawnTransformLocation(const AUBaseCharacter* Character);

	virtual FRotator GetSpawnTransformRotation(const AUBaseCharacter* Character);

	virtual void PreActivate(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, 
		const FGameplayAbilityActivationInfo ActivationInfo, FOnGameplayAbilityEnded::FDelegate* OnGameplayAbilityEndedDelegate) override;

	void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
		const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;

	virtual void EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, 
		const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled);

	// Needs to be a UFUNCTION in the inherited class
	virtual void OnActorHit(FGameplayEventData GameplayEventData);

	// Needs to be a UFUNCTION in the inherited class
	virtual void OnActorOverlap(FGameplayEventData GameplayEventData);

	// Needs to be a UFUNCTION in the inherited class
	virtual void OnActorDestroyed(FGameplayEventData GameplayEventData);

public:
	void OnAvatarSet(const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilitySpec& Spec) override;
};