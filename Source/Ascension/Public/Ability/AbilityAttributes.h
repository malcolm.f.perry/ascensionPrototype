#pragma once

#include "CoreMinimal.h"
#include "../Ascension.h"
#include "Ability/BaseGameplayAbility.h"
#include "Core/AscensionCore.h"
#include "DataTable/DataTableInitializable.h"
#include "AbilityAttributes.generated.h"

UCLASS(BlueprintType)
class ASCENSION_API UAbilityAttributes : public UObject, public IDataTableInitializable
{
	GENERATED_BODY()

private:
	UPROPERTY()
	TSubclassOf<UBaseGameplayAbility> AbilityType;

	UPROPERTY()
	int32 Power = 1; // 1-10

	UPROPERTY()
	int32 Penetration = 0; //0-100%

	UPROPERTY()
	int32 Range = 100; //cm

	UPROPERTY()
	int32 Speed = 1000; //cm/s

	UPROPERTY()
	int32 Size = 25; //cm

	UPROPERTY()
	int32 CastTime = 5; //seconds

	UPROPERTY()
	int32 Duration = 0; //seconds

	UPROPERTY()
	int32 Delay = 0; //seconds

	UPROPERTY()
	int32 Cooldown = 3; //seconds

	UPROPERTY()
	int32 Cost = 10; // Stat cost(Health/Energy/Ether)

	UPROPERTY()
	EAffinity Affinity = EAffinity::None;

	UPROPERTY()
	EAbilityDescriptor AbilityDescriptor = EAbilityDescriptor::Attack;

public:
	UAbilityAttributes(const FObjectInitializer& ObjectInitializer);

	bool Validate(EAbilityAttributeName AttributeName, int32 Value);

	UFUNCTION(BlueprintCallable)
	TSubclassOf<UBaseGameplayAbility> GetAbilityType() const;

	UFUNCTION(BlueprintCallable)
	bool SetAbilityType(TSubclassOf<UBaseGameplayAbility> NewAbilityType);

	UFUNCTION(BlueprintCallable)
	int32 GetPower() const;

	UFUNCTION(BlueprintCallable)
	bool SetPower(int32 NewPower);

	UFUNCTION(BlueprintCallable)
	int32 GetPenetration() const;

	UFUNCTION(BlueprintCallable)
	bool SetPenetration(int32 NewPenetration);

	UFUNCTION(BlueprintCallable)
	int32 GetRange() const;

	UFUNCTION(BlueprintCallable)
	bool SetRange(int32 NewRange);

	UFUNCTION(BlueprintCallable)
	int32 GetSpeed() const;

	UFUNCTION(BlueprintCallable)
	bool SetSpeed(int32 NewSpeed);

	UFUNCTION(BlueprintCallable)
	int32 GetSize() const;

	UFUNCTION(BlueprintCallable)
	bool SetSize(int32 NewSize);

	UFUNCTION(BlueprintCallable)
	int32 GetCastTime() const;

	UFUNCTION(BlueprintCallable)
	bool SetCastTime(int32 NewCastTime);

	UFUNCTION(BlueprintCallable)
	int32 GetDuration() const;

	UFUNCTION(BlueprintCallable)
	bool SetDuration(int32 NewDuration);

	UFUNCTION(BlueprintCallable)
	int32 GetDelay() const;

	UFUNCTION(BlueprintCallable)
	bool SetDelay(int32 NewDelay);

	UFUNCTION(BlueprintCallable)
	int32 GetCooldown() const;

	UFUNCTION(BlueprintCallable)
	bool SetCooldown(int32 NewCooldown);

	UFUNCTION(BlueprintCallable)
	int32 GetCost() const;

	UFUNCTION(BlueprintCallable)
	bool SetCost(int32 NewCost);

	UFUNCTION(BlueprintCallable)
	EAffinity GetAffinity() const;

	UFUNCTION(BlueprintCallable)
	bool SetAffinity(EAffinity NewAffinity);

	UFUNCTION(BlueprintCallable)
	EAbilityDescriptor GetAbilityDescriptor() const;

	UFUNCTION(BlueprintCallable)
	bool SetAbilityDescriptor(EAbilityDescriptor NewAbilityDescriptor);
};