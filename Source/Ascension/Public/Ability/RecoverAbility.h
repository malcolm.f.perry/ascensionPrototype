#pragma once

#include "CoreMinimal.h"
#include "Ability/BaseGameplayAbility.h"
#include "Ability/Effect/RecoverEffect.h"
#include "RecoverAbility.generated.h"

UCLASS()
class ASCENSION_API URecoverAbility : public UBaseGameplayAbility
{
	GENERATED_BODY()

public:
	URecoverAbility(const FObjectInitializer& ObjectInitializer);

private:
	UPROPERTY()
	UAbilityAttributes* Attributes;

	UPROPERTY()
	URecoverEffect* RecoverEffect;

	FActiveGameplayEffectHandle RecoverEffectHandle;

	UPROPERTY(VisibleAnywhere, Category = "Particles")
	TSoftObjectPtr<UParticleSystem> RecoverParticles;

	UPROPERTY(VisibleAnywhere, Category = "ActorEffects")
	UParticleSystemComponent* ParticleSystemComponent;

protected:
	virtual FVector GetSpawnTransformLocation(const AUBaseCharacter* Character) override;

	virtual FRotator GetSpawnTransformRotation(const AUBaseCharacter* Character) override;

	void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
		const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;

	UFUNCTION()
	void EndRecover();

	void EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, 
		const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled) override;

public:
	void OnAvatarSet(const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilitySpec& Spec) override;
};