#pragma once

#include "CoreMinimal.h"
#include "UMG/BaseUMGWidget.h"
#include "AbilityWidget.generated.h"

DECLARE_MULTICAST_DELEGATE_OneParam(FOnAbilityWidgetClicked, int32);

UCLASS()
class ASCENSION_API UAbilityWidget : public UBaseUMGWidget
{
	GENERATED_BODY()

public:
    UAbilityWidget(const FObjectInitializer& ObjectInitializer);

private:
    UPROPERTY()
    FSlateColor BaseColor = FSlateColor(FLinearColor(0, 0, 0, 1));

    UPROPERTY()
    FSlateColor SelectedColor = FSlateColor(FLinearColor(255, 255, 0, 1));

protected:
    UPROPERTY(BlueprintReadOnly)
    int32 Id = 0;

    UPROPERTY(BlueprintReadOnly)
    FSlateColor CurrentColor = BaseColor;

public:
    FOnAbilityWidgetClicked OnAbilityWidgetClicked;

    UFUNCTION()
    void Init(int32 NewId);

    UFUNCTION(BlueprintCallable)
    void Update();

    UFUNCTION()
    void ShowSelected(bool bIsSelected);
};
