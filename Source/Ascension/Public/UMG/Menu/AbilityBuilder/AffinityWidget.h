#pragma once

#include "CoreMinimal.h"
#include "UMG/BaseUMGWidget.h"
#include "AffinityWidget.generated.h"

DECLARE_MULTICAST_DELEGATE_TwoParams(FOnAffinityWidgetClicked, EAffinity, bool);

UCLASS()
class ASCENSION_API UAffinityWidget : public UBaseUMGWidget
{
	GENERATED_BODY()

public:
	UAffinityWidget(const FObjectInitializer& ObjectInitializer);

private:
	bool bIsActive = false;

	UPROPERTY()
	FSlateColor BaseColor = FSlateColor(FLinearColor(0, 0, 0, 1));

	UPROPERTY()
	FSlateColor HighlightColor = FSlateColor(FLinearColor(255, 255, 0, 1));

	void ToggleHighlight(bool bShouldHighlight);

protected:
	UPROPERTY(BlueprintReadOnly)
	EAffinity Affinity = EAffinity::None;

	UPROPERTY(BlueprintReadOnly)
	FSlateColor CurrentColor = BaseColor;

	UPROPERTY(BlueprintReadOnly)
	UTexture* IconTexture;

public:
	FOnAffinityWidgetClicked OnAffinityWidgetClicked;

	UFUNCTION(BlueprintCallable)
	FText GetWidgetName();

	UFUNCTION()
	bool IsActive();

	UFUNCTION(BlueprintCallable)
	void ToggleActive();

	UFUNCTION(BlueprintCallable)
	void Init(EAffinity NewAffinity);

	UFUNCTION(BlueprintCallable)
	void Update();

	UFUNCTION()
	void Reset();
};