#pragma once

#include "CoreMinimal.h"
#include "UMG/BaseUMGWidget.h"
#include "AbilityTypeWidget.generated.h"

DECLARE_MULTICAST_DELEGATE_OneParam(FOnAbilityTypeValueChanged, FString);

UCLASS()
class ASCENSION_API UAbilityTypeWidget : public UBaseUMGWidget
{
	GENERATED_BODY()

public:
    UAbilityTypeWidget(const FObjectInitializer& ObjectInitializer);

protected:
	UPROPERTY(BlueprintReadOnly)
	FString SelectedOption = FString();

	// Defined in blueprints ONLY
	UFUNCTION(BlueprintImplementableEvent)
	void OnValueReset();

	// Defined in blueprints ONLY
	UFUNCTION(BlueprintImplementableEvent)
	void OnValueSet();

public:
	FOnAbilityTypeValueChanged OnAbilityTypeValueChanged;

	UFUNCTION(BlueprintCallable)
	void Update(FString NewOption);

	UFUNCTION()
	FString GetValue();

	UFUNCTION()
	void SetValue(FString NewOption);

	UFUNCTION()
	void Reset();
};