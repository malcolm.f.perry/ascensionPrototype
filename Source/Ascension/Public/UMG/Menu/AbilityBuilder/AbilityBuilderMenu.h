#pragma once

#include "CoreMinimal.h"
#include "UMG/BaseUMGWidget.h"
#include "Ability/AbilityAttributes.h"
#include "UMG/Menu/AbilityBuilder/AbilityWidget.h"
#include "UMG/Menu/AbilityBuilder/AffinityWidget.h"
#include "UMG/Menu/AbilityBuilder/AbilityTypeWidget.h"
#include "UMG/Menu/AbilityBuilder/AttributeWidget.h"
#include "AbilityBuilderMenu.generated.h"

UCLASS()
class ASCENSION_API UAbilityBuilderMenu : public UBaseUMGWidget
{
	GENERATED_BODY()

public:
	UAbilityBuilderMenu(const FObjectInitializer& ObjectInitializer);

private:
	bool bIsValid = false;

	/****************************** Attribute array properties *****************************/
	UPROPERTY()
	int32 CurrentAbilityIndex = -1;

	UPROPERTY()
	TArray<UAbilityAttributes*> AbilityAttributesArray;

	UPROPERTY()
	TArray<int32> AbilityAttributesCostArray;
	/***************************************************************************************/

	/******************************** Widget properties ********************************/
	UPROPERTY()
	TArray<UAbilityWidget*> AbilityWidgetsArray;

	UPROPERTY()
	TMap<EAffinity, UAffinityWidget*> AffinityWidgetsMap;

	UPROPERTY()
	UAbilityTypeWidget* AbilityTypeWidget;

	UPROPERTY()
	TMap<EAbilityAttributeName, UAttributeWidget*> AttributeWidgetsMap;
	/************************************************************************************/

protected:
	/***************************** Constants from the DataTable ******************************/
	UPROPERTY(BlueprintReadOnly)
	int32 BaseAbilityCost = 0;

	UPROPERTY(BlueprintReadOnly)
	int32 BaseAffinityCost = 0;

	UPROPERTY(BlueprintReadOnly)
	int32 MaxNumberOfAbilities = 0;

	UPROPERTY(BlueprintReadOnly)
	int32 TotalAbilityPoints = 0;

	UPROPERTY(BlueprintReadOnly)
	int32 RemainingAbilityPoints = 0;
	/****************************************************************************************/

public:
	// The current cost of all attributes for this ability
	UFUNCTION(BlueprintCallable)
	int32 GetCurrentAttributesCost();

	// The current cost of this ability (ability + affinity + attributes)
	UFUNCTION(BlueprintCallable)
	int32 GetCurrentAbilityCost();

	// The current total cost of all abilities and attributes
	UFUNCTION(BlueprintCallable)
	int32 GetTotalAbilitiesCost();

	UFUNCTION(BlueprintCallable)
	void RecalculateRemainingPoints();

	/********************************* Ability Builder form interactions *******************************/
	UFUNCTION()
	bool CanAddAbility();

	UFUNCTION()
	void UpdateActiveAbility(int32 CurrentIndex, int32 NewIndex);

	UFUNCTION(BlueprintCallable)
	bool AddAbilityWidget(UAbilityWidget* AbilityWidget);
	
	UFUNCTION(BlueprintCallable)
	void RemoveAbilityWidget(int32 Index);

	UFUNCTION()
	void AddAbilityAttributes();

	UFUNCTION()
	void RemoveAbilityAttributes(int32 Index);

	UFUNCTION()
	void ResetAffinities();

	UFUNCTION()
	void ResetAttributes();

	UFUNCTION()
	void Reset();

	// Checks whether any of the ability components(affinity/type/attributes) is null
	UFUNCTION()
	bool HasAllComponents();

	UFUNCTION()
	bool Validate();

	// Refresh the validation and cost displays
	UFUNCTION()
	void Refresh();
	/********************************************************************************************/

	/******************************** Menu Components Events ************************************/
	UFUNCTION()
	void SwitchAbility(int32 Index);

	UFUNCTION()
	void UpdateAffinity(EAffinity Affinity, bool bIsActive);
	
	UFUNCTION()
	void UpdateAbilityType(FString AbilityType);

	UFUNCTION()
	void UpdateAttributeValue(EAbilityAttributeName Name, int32 Value);
	/********************************************************************************************/

	// Commit the form updates and add them to the game instance
	UFUNCTION(BlueprintCallable)
	void UpdateGameInstance();

protected:
	// Defined in blueprints ONLY
	UFUNCTION(BlueprintImplementableEvent)
	void OnValidationStateChange(bool bValidationState);

	UFUNCTION(BlueprintCallable)
	void InitAffinityWidget(UAffinityWidget* AffinityWidget, EAffinity Affinity);

	UFUNCTION(BlueprintCallable)
	void InitAbilityTypeWidget(UAbilityTypeWidget* NewAbilityTypeWidget);

	UFUNCTION(BlueprintCallable)
	void InitAttributeWidget(UAttributeWidget* AttributeWidget, EAbilityAttributeName Name);

	UFUNCTION(BlueprintCallable)
	void Init();
};