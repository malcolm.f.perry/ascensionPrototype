#pragma once

#include "CoreMinimal.h"
#include "UMG/BaseUMGWidget.h"
#include "AttributeWidget.generated.h"

DECLARE_MULTICAST_DELEGATE_TwoParams(FOnAttributeWidgetValueChanged, EAbilityAttributeName, int32);

UCLASS()
class ASCENSION_API UAttributeWidget : public UBaseUMGWidget
{
	GENERATED_BODY()

public:
	UAttributeWidget(const FObjectInitializer& ObjectInitializer);

private:
	// Flag to invert cost scaling. If true, as value increases, cost will decrease
	UPROPERTY()
	bool bCostIsInverted = false;

	UPROPERTY()
	EAbilityAttributeName EnumName = EAbilityAttributeName::Power;

	void UpdateCost();

	void UpdateMinValue(FGameConstantsTableRow Data);

	void UpdateMaxValue(FGameConstantsTableRow Data);

protected:
	UPROPERTY(BlueprintReadOnly)
	FString Name = FString();

	UPROPERTY(BlueprintReadOnly)
	int32 Value = 0;

	UPROPERTY(BlueprintReadOnly)
	int32 Cost = 0;

	UPROPERTY(BlueprintReadOnly)
	int32 MinValue;

	UPROPERTY(BlueprintReadOnly)
	int32 MaxValue;
	
	// Defined in blueprints ONLY
	UFUNCTION(BlueprintImplementableEvent)
	void OnMinValueChanged();

	// Defined in blueprints ONLY
	UFUNCTION(BlueprintImplementableEvent)
	void OnMaxValueChanged();

	// Defined in blueprints ONLY
	UFUNCTION(BlueprintImplementableEvent)
	void OnValueReset();

public:
	FOnAttributeWidgetValueChanged OnAttributeWidgetValueChanged;

	UFUNCTION()
	void Init(EAbilityAttributeName NewName);

	UFUNCTION()
	int32 GetCost();

	UFUNCTION(BlueprintCallable)
	void Update(int32 NewValue);

	UFUNCTION()
	void SetValue(int32 NewValue);

	UFUNCTION()
	void Reset();
};