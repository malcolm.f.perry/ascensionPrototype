#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "DataTable/AbilityBuilderDataTable.h"
#include "DataTable/GameConstantsDataTable.h"
#include "../Ascension.h"
#include "BaseUMGWidget.generated.h"

UCLASS()
class ASCENSION_API UBaseUMGWidget : public UUserWidget, public IDataTableInitializable
{
	GENERATED_BODY()

protected:
	UFUNCTION(BlueprintCallable)
	void SwitchGameMenu(TSubclassOf<UBaseUMGWidget> WidgetClass);

	UFUNCTION(BlueprintCallable)
	FAbilityBuilderTableRow GetAbilityBuilderData(FString RowName);

	UFUNCTION(BlueprintCallable)
	FGameConstantsTableRow GetGameConstantsData();
};