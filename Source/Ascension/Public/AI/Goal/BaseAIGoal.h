#pragma once

#include "CoreMinimal.h"
#include "AI/GOAP/GOAPAction.h"
#include "DataTable/DataTableInitializable.h"
#include "BaseAIGoal.generated.h"

/**
 * The base class for Utility AI goals. These goals will be used to populate the desired
 * world state of the AI Controller, which GOAP will build a sequence of actions to achieve.
 */
UCLASS(Blueprintable)
class ASCENSION_API UBaseAIGoal : public UObject
{
	GENERATED_BODY()

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Properties)
	TArray<FAtom> DesiredWorldState;

public:
	UBaseAIGoal(const FObjectInitializer& ObjectInitializer);

	// Calculate the utility of pursuing the goal based on the current world state.
	virtual float CalculateUtility(TMap<EAIWorldStateAttribute, bool> CurrentWorldState);

	// Get the goals DesiredWorldState for the GOAP Planner
	TArray<FAtom> GetDesiredWorldState();
};