#pragma once

#include "CoreMinimal.h"
#include "AI/Goal/BaseAIGoal.h"
#include "RetreatGoal.generated.h"

/**
 * 
 */
UCLASS()
class ASCENSION_API URetreatGoal : public UBaseAIGoal
{
	GENERATED_BODY()

public:
	URetreatGoal(const FObjectInitializer& ObjectInitializer);

	// Calculate the utility of pursuing the goal based on the current world state.
	float CalculateUtility(TMap<EAIWorldStateAttribute, bool> CurrentWorldState) override;
};
