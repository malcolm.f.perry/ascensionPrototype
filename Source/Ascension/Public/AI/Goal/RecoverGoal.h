#pragma once

#include "CoreMinimal.h"
#include "AI/Goal/BaseAIGoal.h"
#include "RecoverGoal.generated.h"

/**
 * 
 */
UCLASS()
class ASCENSION_API URecoverGoal : public UBaseAIGoal
{
	GENERATED_BODY()

public:
	URecoverGoal(const FObjectInitializer& ObjectInitializer);

	// Calculate the utility of pursuing the goal based on the current world state.
	float CalculateUtility(TMap<EAIWorldStateAttribute, bool> CurrentWorldState) override;
};
