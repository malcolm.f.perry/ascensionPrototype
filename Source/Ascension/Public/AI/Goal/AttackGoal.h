#pragma once

#include "CoreMinimal.h"
#include "AI/Goal/BaseAIGoal.h"
#include "AttackGoal.generated.h"

UCLASS()
class ASCENSION_API UAttackGoal : public UBaseAIGoal
{
	GENERATED_BODY()

public:
	UAttackGoal(const FObjectInitializer& ObjectInitializer);

	// Calculate the utility of pursuing the goal based on the current world state.
	float CalculateUtility(TMap<EAIWorldStateAttribute, bool> CurrentWorldState) override;
};