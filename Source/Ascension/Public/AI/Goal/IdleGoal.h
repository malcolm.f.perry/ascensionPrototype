#pragma once

#include "CoreMinimal.h"
#include "AI/Goal/BaseAIGoal.h"
#include "IdleGoal.generated.h"

/**
 * 
 */
UCLASS()
class ASCENSION_API UIdleGoal : public UBaseAIGoal
{
	GENERATED_BODY()

public:
	UIdleGoal(const FObjectInitializer& ObjectInitializer);

	// Calculate the utility of pursuing the goal based on the current world state.
	float CalculateUtility(TMap<EAIWorldStateAttribute, bool> CurrentWorldState) override;
};
