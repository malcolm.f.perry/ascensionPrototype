#pragma once

#include "CoreMinimal.h"
#include "AI/Action/BaseAIAction.h"
#include "Ability/CharacterAbilitySystemComponent.h"
#include "BaseAbilityAction.generated.h"

/**
 * 
 */
UCLASS()
class ASCENSION_API UBaseAbilityAction : public UBaseAIAction
{
	GENERATED_BODY()
	
protected:
	// TODO: Possibly remove all the AbilitSlot_* classes, make the Index variable editable on the BP. Make AbilitySlot BPs inherit from this directly
	UPROPERTY()
	int32 Index = -1;

	UPROPERTY()
	UCharacterAbilitySystemComponent* AbilitySystem;

public:
	UBaseAbilityAction();

	void doAction_Implementation(APawn* Pawn) override;

	UFUNCTION()
	void AbilityEnded(const FAbilityEndedData& AbilityEndedData);
};
