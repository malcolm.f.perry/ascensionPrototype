#pragma once

#include "CoreMinimal.h"
#include "AI/Action/BaseAIAction.h"
#include "MoveAction.generated.h"

UCLASS()
class ASCENSION_API UMoveAction : public UBaseAIAction
{
	GENERATED_BODY()

public:
	UMoveAction();

	void doAction_Implementation(APawn* Pawn) override;

	UFUNCTION()
	bool TryMovingForward(const FVector& ActorLocation, float DistanceToMove, float Radius, FVector& TargetLocation);

	UFUNCTION()
	bool TryMovingBackward(const FVector& ActorLocation, float DistanceToMove, float Radius, FVector& TargetLocation);

	UFUNCTION()
	bool TryMovingDiagonally(const FVector& ActorLocation, float DistanceToMove, float Radius, FVector& TargetLocation);

	UFUNCTION()
	bool TryMovingLeftRight(const FVector& ActorLocation, float DistanceToMove, float Radius, FVector& TargetLocation);

	UFUNCTION()
	bool TryMovingRearDiagonally(const FVector& ActorLocation, float DistanceToMove, float Radius, FVector& TargetLocation);

	UFUNCTION()
	bool SelectFirstValidLocation(TArray<FVector> Locations, float Radius, FVector& TargetLocation);

	UFUNCTION()
	virtual void MoveCompleted(FAIRequestID RequestID, EPathFollowingResult::Type Result);
};