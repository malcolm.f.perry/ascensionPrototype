#pragma once

#include "CoreMinimal.h"
#include "AI/Action/BaseAbilityAction.h"
#include "RecoverAction.generated.h"

UCLASS()
class ASCENSION_API URecoverAction : public UBaseAbilityAction
{
	GENERATED_BODY()

public:
	URecoverAction();

	void doAction_Implementation(APawn* Pawn) override;
};