#pragma once

#include "CoreMinimal.h"
#include "AI/Action/BaseAbilityAction.h"
#include "AbilitySlot_5_Action.generated.h"

/**
 * 
 */
UCLASS()
class ASCENSION_API UAbilitySlot_5_Action : public UBaseAbilityAction
{
	GENERATED_BODY()

public:
	UAbilitySlot_5_Action();

	void doAction_Implementation(APawn* Pawn) override;
};
