#pragma once

#include "CoreMinimal.h"
#include "AI/Action/BaseAbilityAction.h"
#include "AbilitySlot_1_Action.generated.h"

UCLASS()
class ASCENSION_API UAbilitySlot_1_Action : public UBaseAbilityAction
{
	GENERATED_BODY()

public:
	UAbilitySlot_1_Action();

	void doAction_Implementation(APawn* Pawn) override;
};