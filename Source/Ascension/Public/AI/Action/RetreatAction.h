#pragma once

#include "CoreMinimal.h"
#include "AI/Action/MoveAction.h"
#include "RetreatAction.generated.h"

/**
 * 
 */
UCLASS()
class ASCENSION_API URetreatAction : public UMoveAction
{
	GENERATED_BODY()

protected:
	UPROPERTY()
	AUAICharacter* Self;

public:
	URetreatAction();

	void doAction_Implementation(APawn* Pawn) override;

	void MoveCompleted(FAIRequestID RequestID, EPathFollowingResult::Type Result) override;
};
