#pragma once

#include "CoreMinimal.h"
#include "AI/Action/BaseAbilityAction.h"
#include "DodgeAction.generated.h"

/**
 * 
 */
UCLASS()
class ASCENSION_API UDodgeAction : public UBaseAbilityAction
{
	GENERATED_BODY()

public:
	UDodgeAction();

	void doAction_Implementation(APawn* Pawn) override;

	void AbilityEnded(const FAbilityEndedData& AbilityEndedData);

protected:
	UPROPERTY()
	FRotator OriginalActorRotation = FRotator();

	UPROPERTY()
	AUAICharacter* Self;

private:
	void DoDodge(bool HasEnergy);
};
