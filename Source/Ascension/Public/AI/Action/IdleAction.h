#pragma once

#include "CoreMinimal.h"
#include "AI/Action/BaseAIAction.h"
#include "IdleAction.generated.h"

/**
 * 
 */
UCLASS()
class ASCENSION_API UIdleAction : public UBaseAIAction
{
	GENERATED_BODY()

private:
	UPROPERTY()
	float IdleDelay = 1.0;

	UPROPERTY()
	FTimerHandle IdleDelayTimerHandle;

public:
	UIdleAction();

	void doAction_Implementation(APawn* Pawn) override;

	void IdleDelayCallback();
};
