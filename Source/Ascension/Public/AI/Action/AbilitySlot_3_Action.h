#pragma once

#include "CoreMinimal.h"
#include "AI/Action/BaseAbilityAction.h"
#include "AbilitySlot_3_Action.generated.h"

/**
 * 
 */
UCLASS()
class ASCENSION_API UAbilitySlot_3_Action : public UBaseAbilityAction
{
	GENERATED_BODY()

public:
	UAbilitySlot_3_Action();

	void doAction_Implementation(APawn* Pawn) override;
};
