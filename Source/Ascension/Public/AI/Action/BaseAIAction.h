#pragma once

#include "CoreMinimal.h"
#include "AI/GOAP/GOAPAction.h"
#include "Core/AscensionCore.h"
#include "DataTable/DataTableInitializable.h"
#include "Controller/AscensionAIController.h"
#include "BaseAIAction.generated.h"

UCLASS()
class ASCENSION_API UBaseAIAction : public UGOAPAction
{
	GENERATED_BODY()

protected:
	UPROPERTY()
	AAscensionAIController* Controller;

public:
	UBaseAIAction();

	virtual void doAction_Implementation(APawn* Pawn) override;
};