#pragma once

#include "CoreMinimal.h"
#include "AI/Action/BaseAbilityAction.h"
#include "AbilitySlot_2_Action.generated.h"

/**
 * 
 */
UCLASS()
class ASCENSION_API UAbilitySlot_2_Action : public UBaseAbilityAction
{
	GENERATED_BODY()
	
public:
	UAbilitySlot_2_Action();

	void doAction_Implementation(APawn* Pawn) override;
};
