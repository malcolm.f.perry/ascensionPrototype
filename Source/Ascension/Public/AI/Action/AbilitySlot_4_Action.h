// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AI/Action/BaseAbilityAction.h"
#include "AbilitySlot_4_Action.generated.h"

/**
 * 
 */
UCLASS()
class ASCENSION_API UAbilitySlot_4_Action : public UBaseAbilityAction
{
	GENERATED_BODY()
	
public:
	UAbilitySlot_4_Action();

	void doAction_Implementation(APawn* Pawn) override;
};
