#pragma once

#include "CoreMinimal.h"
#include "../Ascension.h"
#include "AI/GOAP/GOAPController.h"
#include "DataTable/DataTableInitializable.h"
#include "AI/Goal/BaseAIGoal.h"
#include "DataTable/AIConfigDataTable.h"
#include "Character/AICharacter.h"
#include "AscensionAIController.generated.h"

// Function called after an ability actor hits
DECLARE_MULTICAST_DELEGATE_OneParam(FOnHitByAbilityActor, FVector);

/**
 * Controller class for the AI that uses utility theory to select goals for the extended
 * GOAP which selects and executes actions to achieve the selected goal.
 */
UCLASS()
class ASCENSION_API AAscensionAIController : public AGOAPController, public IDataTableInitializable
{
	GENERATED_BODY()

public:
	FOnHitByAbilityActor OnHitByAbilityActorEvent;

	AAscensionAIController();

	TArray<FAtom> GetCurrentWorldState();

	TMap<EAIWorldStateAttribute, bool> GetCurrentWorldStateMap();

	void UpdateWorldState();

	void AddToWorldState(EAIWorldStateAttribute Attribute, FAtom AttributeAtom);

	AUBaseCharacter* GetCurrentTarget();

	FVector GetLastKnownStimulusLocation();

	bool GetRandomPointInRadius(const FVector& Origin, float Radius, FVector& Result);

	bool CanReachLocation(FVector PathStart, FVector PathEnd);

	UBaseAIGoal* GetCurrentGoal();

private:
	UPROPERTY()
	AUAICharacter* Self;

	UPROPERTY()
	AUBaseCharacter* CurrentTarget;

	UPROPERTY()
	FVector LastKnownStimulusLocation;

	/************************** World state variables for GOAP and for the AI Controller *******************************/
	TArray<FAtom> CurrentWorldState;

	TMap<EAIWorldStateAttribute, bool> CurrentWorldStateMap;

	FDateTime WorldStateUpdateTime;
	/********************************************************************************************************************/

	// The list of all possible goals the AI could pursue
	UPROPERTY()
	TArray<UBaseAIGoal*> GoalsArray;

	// The current goal the AI is pursuing
	UPROPERTY()
	UBaseAIGoal* CurrentGoal;

	UBaseAIGoal* FindBestGoal();

	void SelectNewGoal();

	AUBaseCharacter* FindBestTarget(TArray<AActor*> PerceivedActors);

	UFUNCTION()
	void OnPerceptionUpdated(const TArray<AActor*>& UpdatedActors);

	void InitializeGoals(FAIConfigTableRow Data);

	/********************* TODO: Debug only ***********************/
	void PrintCurrentAndDesiredWorldStates();

	void PrintState(TArray<FAtom> State);
	/*************************************************************/

protected:
	void OnPlanCompleted() override;

	void OnHitByAbilityActor(FVector ActorOrigin);

	void Tick(float DeltaTime) override;

	void BeginPlay() override;
};