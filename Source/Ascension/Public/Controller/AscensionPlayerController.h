#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "AscensionPlayerController.generated.h"

/**
 *
 */
UCLASS()
class ASCENSION_API AUAscensionPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	virtual void BeginPlay() override;
};
