#pragma once

#include "CoreMinimal.h"
#include "AscensionCore.generated.h"

/********************** Enums **********************/
UENUM(BlueprintType)
enum class EAffinity : uint8
{
	None UMETA(DisplayName = "None"),
	Fire UMETA(DisplayName = "Fire"),
	Water UMETA(DisplayName = "Water"),
	Earth UMETA(DisplayName = "Earth"),
	Air UMETA(DisplayName = "Air")
};

/**
 * Represents the names of the data tables the manager holds for easy referencing by other classes
 */
UENUM(BlueprintType)
enum class EDataTableName : uint8
{
	NullTable UMETA(DisplayName = "NullTable"),
	GameConstants UMETA(DisplayName = "GameConstants"),
	AIConfig UMETA(DisplayName = "AIConfig"),
	AbilityBuilder UMETA(DisplayName = "AbilityBuilder"),
	AffinityBonus UMETA(DisplayName = "AffinityBonus"),
	BlastAbility UMETA(DisplayName = "BlastAbility"),
	BeamAbility UMETA(DisplayName = "BeamAbility"),
	RecoverAbility UMETA(DisplayName = "RecoverAbility"),
	SprintAbility UMETA(DisplayName = "SprintAbility"),
	RollAbility UMETA(DisplayName = "RollAbility"),
	JumpAbility UMETA(DisplayName = "JumpAbility")
};

/**
 * The names of attributes used to construct abilities
 */
UENUM(BlueprintType)
enum class EAbilityAttributeName : uint8
{
	Power UMETA(DisplayName = "Power"),
	Penetration UMETA(DisplayName = "Penetration"),
	Range UMETA(DisplayName = "Range"),
	Speed UMETA(DisplayName = "Speed"),
	Size UMETA(DisplayName = "Size"),
	CastTime UMETA(DisplayName = "CastTime"),
	Duration UMETA(DisplayName = "Duration"),
	Delay UMETA(DisplayName = "Delay"),
	Cooldown UMETA(DisplayName = "Cooldown"),
	Cost UMETA(DisplayName = "Cost"),
	Affinity UMETA(DisplayName = "Affinity"),
	AbilityType UMETA(DisplayName = "AbilityType")
};

/**
 * The descriptors used for abilities
 */
UENUM(BlueprintType)
enum class EAbilityDescriptor : uint8
{
	Attack UMETA(DisplayName = "Attack"),
	Defense UMETA(DisplayName = "Defense"),
	Movement UMETA(DisplayName = "Movement"),
	Recovery UMETA(DisplayName = "Recovery")
};

/**
 * TODO: not currently used but may fork the plugin and enforce enums
 * The names of all of the possible actions the AI can take
 */
/*UENUM(BlueprintType)
enum class EAIAction : uint8
{
	Move UMETA(DisplayName = "Move"),
	Attack UMETA(DisplayName = "Attack"),
	Block UMETA(DisplayName = "Block"),
	Boost UMETA(DisplayName = "Boost"),
	Recover UMETA(DisplayName = "Recover"),
	Roll UMETA(DisplayName = "Roll"),
	Sprint UMETA(DisplayName = "Sprint"),
	Idle UMETA(DisplayName = "Idle")
};*/

/**
 * TODO: not currently used but may fork the plugin and enforce enums
 * The names of all of the possible goals the AI can have
 */
/*UENUM(BlueprintType)
enum class EAIGoal : uint8
{
	AttackEnemies UMETA(DisplayName = "AttackEnemies"),
	ProtectSelf UMETA(DisplayName = "ProtectSelf"),
	Idle UMETA(DisplayName = "Idle")
};*/

/**
 * TODO: for the plugin and apply / enforce this enum?
 * The names of all of the possible world state attributes for the AI to consider
 */
UENUM(BlueprintType)
enum class EAIWorldStateAttribute : uint8
{
	EMPTY UMETA(DisplayName = "EMPTY"), // Added for the datatable map which conflicts on the first key when adding new entries
	IsAlive UMETA(DisplayName = "IsAlive"),
	EnemyIsAlive UMETA(DisplayName = "EnemyIsAlive"),
	HasTarget UMETA(DisplayName = "HasTarget"),
	IsInRange UMETA(DisplayName = "IsInRange"),
	EnemiesInRange UMETA(DisplayName = "EnemiesInRange"),
	ShouldIdle UMETA(DisplayName = "ShouldIdle"),
	ProjectilesIncoming UMETA(DisplayName = "ProjectilesIncoming"),
	NeedHealing UMETA(DisplayName = "NeedHealing"),
	NeedToDodge UMETA(DisplayName = "NeedToDodge"),
	TargetEnemyIsHealthier UMETA(DisplayName = "TargetEnemyIsHealthier"),
	AbilityOneAvailable UMETA(DisplayName = "AbilityOneAvailable"),
	AbilityTwoAvailable UMETA(DisplayName = "AbilityTwoAvailable"),
	AbilityThreeAvailable UMETA(DisplayName = "AbilityThreeAvailable"),
	AbilityFourAvailable UMETA(DisplayName = "AbilityFourAvailable"),
	AbilityFiveAvailable UMETA(DisplayName = "AbilityFiveAvailable"),
	HasAttackAvailable UMETA(DisplayName = "HasAttackAvailable"),
	RecoverAvailable UMETA(DisplayName = "RecoverAvailable"),
	JumpAvailable UMETA(DisplayName = "JumpAvailable"),
	RollAvailable UMETA(DisplayName = "RollAvailable"),
	SprintAvailable UMETA(DisplayName = "SprintAvailable")
};

/********************* Structs *********************/
// Attributes used for Ability Actors
USTRUCT(Blueprintable)
struct FActorAttributes
{
	GENERATED_BODY()

	UPROPERTY()
	int32 Range = 0; //cm

	UPROPERTY()
	int32 Speed = 0; //cm/s

	UPROPERTY()
	int32 Size = 0; //cm

	UPROPERTY()
	int32 Delay = 0; //seconds

	UPROPERTY()
	EAffinity Affinity = EAffinity::Fire;

	FActorAttributes() {}

	FActorAttributes(int32 NewRange, int32 NewSpeed, int32 NewSize, int32 NewDelay, EAffinity NewAffinity)
	{
		Range = NewRange;
		Speed = NewSpeed;
		Size = NewSize;
		Delay = NewDelay;
		Affinity = NewAffinity;
	}
};

/* Wrapper object for the project's core enums and structs */
UCLASS(MinimalApi)
class UAscensionEnums : public UObject
{
	GENERATED_BODY()
};