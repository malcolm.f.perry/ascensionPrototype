#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "DataTable/DataTableManager.h"
#include "Character/CharacterInfo.h"
#include "AscensionGameInstance.generated.h"

UCLASS()
class ASCENSION_API UAscensionGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	UAscensionGameInstance(const FObjectInitializer& ObjectInitializer);

	UFUNCTION(BlueprintCallable)
	TMap<EAffinity, int32> GetAffinities() const;

	UFUNCTION(BlueprintCallable)
	bool AddAffinity(EAffinity Affinity, int32 Value);

	UFUNCTION(BlueprintCallable)
	int32 RemoveAffinity(EAffinity Affinity);

	UFUNCTION(BlueprintCallable)
	TArray<UAbilityAttributes*> GetAbilityAttributesArray() const;

	UFUNCTION(BlueprintCallable)
	bool AddAbilityAttributes(int32 Index, UAbilityAttributes* NewAttributes);

	bool RemoveAbilityAttributes(int32 Index);

	UFUNCTION(BlueprintCallable)
	UDataTableManager* GetDataTableManager() const;

	UFUNCTION(BlueprintCallable)
	TArray<AUBaseCharacter*> GetGameMembers();

	UFUNCTION(BlueprintCallable)
	void AddGameMember(AUBaseCharacter* NewCharacter);

protected:
	void OnStart() override;

private:
	UPROPERTY()
	UCharacterInfo* CharacterInfo;

	UPROPERTY(EditDefaultsOnly, Category = "DataTableManager")
	TSubclassOf<UDataTableManager> DataTableManager;

	UPROPERTY()
	TArray<AUBaseCharacter*> GameMembers;
};