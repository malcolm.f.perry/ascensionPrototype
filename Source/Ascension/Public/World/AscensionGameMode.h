#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "AscensionGameMode.generated.h"

UCLASS(minimalapi)
class AUAscensionGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AUAscensionGameMode();
};
