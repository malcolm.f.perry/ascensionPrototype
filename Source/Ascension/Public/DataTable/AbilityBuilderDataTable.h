#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "Core/AscensionCore.h"
#include "Ability/BaseGameplayAbility.h"
#include "AbilityBuilderDataTable.generated.h"

/**
 * Structure that defines a row entry for the AbilityBuilder table.
 * RowName is the AbilityName, must match name in EDataTableName
 */
USTRUCT(BlueprintType)
struct FAbilityBuilderTableRow : public FTableRowBase
{
	GENERATED_BODY()

public:
	FAbilityBuilderTableRow() {}

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Attributes)
	TSubclassOf<UBaseGameplayAbility> AbilityType;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Attributes)
	TMap<EAbilityAttributeName, bool> UtilizedAttributesMap;
};

/* The wrapper object for the row data struct used in data table configs*/
UCLASS()
class ASCENSION_API UAbilityBuilderDataTable : public UObject
{
	GENERATED_BODY()
};