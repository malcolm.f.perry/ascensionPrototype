#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "Animation/AnimInstance.h"
#include "Core/AscensionCore.h"
#include "../Ascension.h"
#include "GameConstantsDataTable.generated.h"

/**
 * Structure that defines a row entry for the GameConstants table.
 * Will only contain 1 row named Constants.
 */
USTRUCT(BlueprintType)
struct FGameConstantsTableRow : public FTableRowBase
{
	GENERATED_BODY()

public:
	FGameConstantsTableRow() {}

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = AbilityBuilder)
	int32 MaxStartingAbilityPoints = 120;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = AbilityBuilder)
	int32 BaseAffinityCost = 10;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = AbilityBuilder)
	int32 BaseAbilityCost = 10;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Abilities)
	int32 MaxNumAbilities = 5;

	// Not all attributes will have a min value
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Attributes)
	TMap<EAbilityAttributeName, int32> AttributeMinValueMap;

	// Not all attributes will have a max value
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Attributes)
	TMap<EAbilityAttributeName, int32> AttributeMaxValueMap;

	// Whether or not the cost of an attribute should scale with increases in value or decreases
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Attributes)
	TMap<EAbilityAttributeName, bool> AttributeInvertedCostMap;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Attributes)
	TMap<EAffinity, UTexture*> AffinityIconMap;

	/*************** Hit Montage ****************/
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = HitMontage)
	TSoftObjectPtr<UAnimMontage> OnHitMontage;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = HitMontage)
	FName HitMontageStartSection;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = HitMontage)
	float HitMontagePlayRate = 1;
	/*********************************************/

	// The Vector offset of the camera from the player
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = CameraSettings)
	FVector CameraOffset = FVector(0.f, 60.f, 80.f);

	// Abilities with these tags will be interrupted by other actions
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = InterruptTags)
	FGameplayTagContainer InterruptTagContainer;

	int32 FindMinValue(EAbilityAttributeName AbilityName)
	{
		return FindValue(AttributeMinValueMap, AbilityName);
	}

	int32 FindMaxValue(EAbilityAttributeName AbilityName)
	{
		return FindValue(AttributeMaxValueMap, AbilityName);
	}

	bool ValueInRange(EAbilityAttributeName AbilityName, int32 Value)
	{
		int32 MinValue = FindMinValue(AbilityName);
		int32 MaxValue = FindMaxValue(AbilityName);
		bool ValidMin = MinValue != -1 ? Value >= MinValue : true;
		return ValidMin && (MaxValue != -1 ? Value <= MaxValue : true);
	}

private:
	int32 FindValue(TMap<EAbilityAttributeName, int32> Map, EAbilityAttributeName AbilityName)
	{
		auto Value = Map.Find(AbilityName);
		if (Value)
		{
			return *Value;
		}
		else
		{
			UE_LOG(LogAscension, Error, TEXT("Could not find value for attribute name: %s."), 
				*GET_STRING_FROM_ENUM("EAbilityAttributeName", AbilityName));
		}
		return -1;
	}
};

/* The wrapper object for the row data struct used in data table configs*/
UCLASS()
class ASCENSION_API UGameConstantsDataTable : public UObject
{
	GENERATED_BODY()

public:
	static FString GetRowName()
	{
		return FString("Constants");
	}
};