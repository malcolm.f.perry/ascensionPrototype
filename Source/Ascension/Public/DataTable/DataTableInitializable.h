#pragma once

#include "CoreMinimal.h"
#include "../Ascension.h"
#include "DataTable/DataTableManager.h"
#include "DataTableInitializable.generated.h"

/**
 * An abstract base class that contains functions for reading from and initializing
 * properties from a data table
 */
UINTERFACE()
class ASCENSION_API UDataTableInitializable : public UInterface
{
	GENERATED_BODY()
};

class ASCENSION_API IDataTableInitializable
{
	GENERATED_BODY()

public:
	// Use the world reference to obtain the DataTableManager
	static UDataTableManager* GetDataTableManager(UWorld* World);

	// Asynchronously load an Asset from a path, no callback
	static FORCEINLINE void LoadAsynchronous(FSoftObjectPath Path);

	// Asynchronously load an Asset from a path
	static FORCEINLINE void LoadAsynchronous(FSoftObjectPath Path, FStreamableDelegate Delegate);

	// Asynchronously load an array of Asset paths, no callback
	static FORCEINLINE void LoadAsynchronous(TArray<FSoftObjectPath> Paths);

	// Asynchronously load an array of Asset paths
	static void LoadAsynchronous(TArray<FSoftObjectPath> Paths, FStreamableDelegate Delegate);

	// Get a Row of data from a DataTable
	template <typename ObjClass>
	static FORCEINLINE ObjClass* GetRowDataFromTable(
		UDataTable* DataTable,
		const FString& RowName
	) {
		if (DataTable)
		{
			static const FString Context(TEXT("Data Table Context"));
			return DataTable->FindRow<ObjClass>(FName(*RowName), Context, true);
		}
		return nullptr;
	}

	// Get a row of data from a specified table
	template <typename ObjClass>
	static FORCEINLINE ObjClass* GetRowData(
		UWorld* World,
		EDataTableName DataTableName,
		FString RowName
	) {
		UDataTableManager* DataTableManager = GetDataTableManager(World);
		if (DataTableManager)
		{
			ObjClass* Data = GetRowDataFromTable<ObjClass>(DataTableManager->GetDataTable(DataTableName), RowName);
			if (Data)
			{
				return Data;
			}
			else
			{
				FString DataTableNameString = GET_STRING_FROM_ENUM("EDataTableName", DataTableName);
				UE_LOG(LogAscension, Error, TEXT("Could not find Row: %s in DataTable: %s."), *RowName, *DataTableNameString);
			}
		}
		else {
			FString DataTableNameString = GET_STRING_FROM_ENUM("EDataTableName", DataTableName);
			UE_LOG(LogAscension, Error, TEXT("Could not find a reference to the DataTableManager. Table: %s, Row: %s"), 
				*DataTableNameString, *RowName);
		}
		return nullptr;
	}

	// Get the list of row names for the specified table
	static FORCEINLINE TArray<FName> GetRowNames(
		UWorld* World,
		EDataTableName DataTableName
	) {
		UDataTableManager* DataTableManager = GetDataTableManager(World);
		if (DataTableManager)
		{
			return DataTableManager->GetDataTable(DataTableName)->GetRowNames();
		}
		else {
			FString DataTableNameString = GET_STRING_FROM_ENUM("EDataTableName", DataTableName);
			UE_LOG(LogAscension, Error, TEXT("Could not find a reference to the DataTableManager. Table: %s"), *DataTableNameString);
		}
		return TArray<FName>();
	}

//TODO: Update once the global events dispatcher is added
//protected:
//	virtual void OnGameInstanceStart();
};