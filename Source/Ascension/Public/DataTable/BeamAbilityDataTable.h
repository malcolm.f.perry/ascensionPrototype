#pragma once

#include "CoreMinimal.h"
#include "DataTable/BaseAbilityDataTable.h"
#include "BeamAbilityDataTable.generated.h"

/**
  * Structure that defines a row entry for the BeamAbility table.
  * RowName is the Affinity name(EAffinity)
 */
USTRUCT(BlueprintType)
struct FBeamAbilityTableRow : public FBaseAbilityTableRow
{
	GENERATED_BODY()

public:
	FBeamAbilityTableRow() {}

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Ability)
	TSoftObjectPtr<UStaticMesh> AbilityShape;
};

/* The wrapper object for the row data struct used in data table configs*/
UCLASS()
class ASCENSION_API UBeamAbilityDataTable : public UObject
{
	GENERATED_BODY()
};