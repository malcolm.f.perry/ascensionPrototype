#pragma once

#include "CoreMinimal.h"
#include "DataTable/BaseAbilityDataTable.h"
#include "RollAbilityDataTable.generated.h"

/**
  * Structure that defines a row entry for the RollAbility table.
  * Will only contain 1 row named Constants.
  */
USTRUCT(BlueprintType)
struct FRollAbilityTableRow : public FBaseAbilityTableRow
{
	GENERATED_BODY()

public:
	FRollAbilityTableRow() {}

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Ability)
	int32 Cost = 0;
};

/* The wrapper object for the row data struct used in data table configs*/
UCLASS()
class ASCENSION_API URollAbilityDataTable : public UObject
{
	GENERATED_BODY()

public:
	static FString GetRowName()
	{
		return FString("Constants");
	}
};