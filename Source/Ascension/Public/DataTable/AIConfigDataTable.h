#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "AI/Goal/BaseAIGoal.h"
#include "AI/GOAP/GOAPAction.h"
#include "Core/AscensionCore.h"
#include "AIConfigDataTable.generated.h"

/**
 * Structure that defines a row entry for the AIConfig table.
 * Will only contain 1 row named Constants.
 */
USTRUCT(BlueprintType)
struct FAIConfigTableRow : public FTableRowBase
{
	GENERATED_BODY()

public:
	FAIConfigTableRow() {}

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = AIGoals)
	TArray<TSubclassOf<UBaseAIGoal>> GoalsList;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = AIActions)
	TArray<TSubclassOf<UGOAPAction>> ActionsList;
};

/* The wrapper object for the row data struct used in data table configs*/
UCLASS()
class ASCENSION_API UAIConfigDataTable : public UObject
{
	GENERATED_BODY()

public:
	static FString GetRowName()
	{
		return FString("Constants");
	}
};