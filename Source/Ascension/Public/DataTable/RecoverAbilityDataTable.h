#pragma once

#include "CoreMinimal.h"
#include "DataTable/BaseAbilityDataTable.h"
#include "RecoverAbilityDataTable.generated.h"

/**
  * Structure that defines a row entry for the RecoverAbility table.
  * Will only contain 1 row named Constants.
  */
USTRUCT(BlueprintType)
struct FRecoverAbilityTableRow : public FBaseAbilityTableRow
{
	GENERATED_BODY()

public:
	FRecoverAbilityTableRow() {}

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Ability)
	int32 Cooldown = 300; // 5 mins

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Ability)
	int32 Cost = 0;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Ability)
	int32 Duration = 10;
};

/* The wrapper object for the row data struct used in data table configs*/
UCLASS()
class ASCENSION_API URecoverAbilityDataTable : public UObject
{
	GENERATED_BODY()

public:
	static FString GetRowName()
	{
		return FString("Constants");
	}
};