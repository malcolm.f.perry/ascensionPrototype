#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "AffinityBonusDataTable.generated.h"

/**
 * Structure that defines a row entry for the AffinityBonus table.
 * We represent the bonuses here as positive or negative percentages.
 * Values in the table represent a maximum bonus from 100% Affinity,
 * lower Affinities will get a corresponding percentage of the bonus.
 * RowName is the Affinity name(EAffinity)
 */
USTRUCT(BlueprintType)
struct FAffinityBonusTableRow : public FTableRowBase
{
	GENERATED_BODY()

public:
	FAffinityBonusTableRow() {}

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Bonus)
	int32 MightBonus = 0;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Bonus)
	int32 FinesseBonus = 0;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Bonus)
	int32 HealthBonus = 0;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Bonus)
	int32 EnergyBonus = 0;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Bonus)
	int32 EtherBonus = 0;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Bonus)
	int32 MightRegenRateBonus = 0;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Bonus)
	int32 FinesseRegenRateBonus = 0;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Bonus)
	int32 HealthRegenRateBonus = 0;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Bonus)
	int32 EnergyRegenRateBonus = 0;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Bonus)
	int32 EtherRegenRateBonus = 0;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Bonus)
	int32 EvasionBonus = 0;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Bonus)
	int32 BludgeonResistanceBonus = 0;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Bonus)
	int32 PiercingResistanceBonus = 0;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Bonus)
	int32 SlashingResistanceBonus = 0;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Bonus)
	int32 FireResistanceBonus = 0;

	// TODO: Add a movement speed attribute and use it to control the movement speed
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Bonus)
	int32 MovementSpeedBonus = 0;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Bonus)
	int32 CastingSpeedBonus = 0;

	// TODO: Incorporate this into the DamageEffect
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Bonus)
	int32 DamageBonus = 0;
};

/* The wrapper object for the row data struct used in data table configs*/
UCLASS()
class ASCENSION_API UAffinityBonusDataTable : public UObject
{
	GENERATED_BODY()
};