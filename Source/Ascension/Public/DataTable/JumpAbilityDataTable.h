#pragma once

#include "CoreMinimal.h"
#include "DataTable/BaseAbilityDataTable.h"
#include "JumpAbilityDataTable.generated.h"

/**
  * Structure that defines a row entry for the JumpAbility table.
  * Will only contain 1 row named Constants.
  */
USTRUCT(BlueprintType)
struct FJumpAbilityTableRow : public FBaseAbilityTableRow
{
	GENERATED_BODY()

public:
	FJumpAbilityTableRow() {}

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Ability)
	int32 Cost = 0;
};

/* The wrapper object for the row data struct used in data table configs*/
UCLASS()
class ASCENSION_API UJumpAbilityDataTable : public UObject
{
	GENERATED_BODY()

public:
	static FString GetRowName()
	{
		return FString("Constants");
	}
};