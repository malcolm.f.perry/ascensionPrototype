#pragma once

#include "CoreMinimal.h"
#include "DataTable/BaseAbilityDataTable.h"
#include "Ability/Actor/BaseAbilityActor.h"
#include "Math/Rotator.h"
#include "BlastAbilityDataTable.generated.h"

/** 
  * Structure that defines a row entry for the BlastAbility table.
  * RowName is the Affinity name(EAffinity)
  */
USTRUCT(BlueprintType)
struct FBlastAbilityTableRow : public FBaseAbilityTableRow
{
	GENERATED_BODY()

public:
	FBlastAbilityTableRow() {}

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Ability)
	FRotator ActorParticlesRotation = FRotator(0.0, 0.0, 0.0);

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Ability)
	TSoftObjectPtr<UParticleSystem> OnHitParticles;
};

/* The wrapper object for the row data struct used in data table configs*/
UCLASS()
class ASCENSION_API UBlastAbilityDataTable : public UObject
{
	GENERATED_BODY()
};