#pragma once

#include "CoreMinimal.h"
#include "DataTable/BaseAbilityDataTable.h"
#include "SprintAbilityDataTable.generated.h"

/**
  * Structure that defines a row entry for the SprintAbility table.
  * Will only contain 1 row named Constants.
  */
USTRUCT(BlueprintType)
struct FSprintAbilityTableRow : public FBaseAbilityTableRow
{
	GENERATED_BODY()

public:
	FSprintAbilityTableRow() {}

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Ability)
	int32 Cost = 10;
};

/* The wrapper object for the row data struct used in data table configs*/
UCLASS()
class ASCENSION_API USprintAbilityDataTable : public UObject
{
	GENERATED_BODY()

public:
	static FString GetRowName()
	{
		return FString("Constants");
	}
};