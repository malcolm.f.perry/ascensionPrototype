#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "Core/AscensionCore.h"
#include "Ability/Actor/BaseAbilityActor.h"
#include "BaseAbilityDataTable.generated.h"

/**
  * Structure that defines a basic row entry for an ability data table.
  * RowName is the Affinity name(EAffinity)
  */
USTRUCT()
struct FBaseAbilityTableRow : public FTableRowBase
{
	GENERATED_BODY()

public:
	FBaseAbilityTableRow() {}

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Ability)
	EAffinity Affinity = EAffinity::Fire;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Ability)
	TSoftObjectPtr<UAnimMontage> Montage;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Ability)
	FName MontageStartSection;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Ability)
	float MontagePlayRate = 1;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Ability)
	float SpawnActorDelay = 1;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Ability)
	TSubclassOf<ABaseAbilityActor> AbilityActor;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Ability)
	TSoftObjectPtr<UParticleSystem> ActorParticles;

	// Tags that the ability will have
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = AbilityTags)
	FGameplayTagContainer AbilityTags;
};

/* The wrapper object for the row data struct used in data table configs*/
UCLASS(Abstract)
class ASCENSION_API UBaseAbilityDataTable : public UObject
{
	GENERATED_BODY()
};