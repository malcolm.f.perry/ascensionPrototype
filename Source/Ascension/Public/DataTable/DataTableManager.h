#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Engine/DataTable.h"
#include "Engine/StreamableManager.h"
#include "Engine/AssetManager.h"
#include "Core/AscensionCore.h"
#include "DataTableManager.generated.h"

/**
 * The class responsible for loading and serving data table references to other classes
 */
UCLASS(Blueprintable, BlueprintType)
class ASCENSION_API UDataTableManager : public UObject
{
	GENERATED_BODY()

public:
	UDataTableManager(const FObjectInitializer& ObjectInitializer);

	UFUNCTION(BlueprintCallable)
	UDataTable* GetDataTable(EDataTableName DataTableName) const;

protected:
	UPROPERTY(EditDefaultsOnly, Category = "DataTables")
	TMap<EDataTableName, UDataTable*> DataTablesMap;
};