#pragma once

#include "CoreMinimal.h"
#include "AttributeSet.h"
#include "Character/BaseCharacter.h"
#include "AbilitySystemComponent.h"
#include "Core/AscensionCore.h"
#include "BaseStatSet.generated.h"

// Macros from AttributeSet.h
#define ATTRIBUTE_ACCESSORS(ClassName, PropertyName) \
	GAMEPLAYATTRIBUTE_PROPERTY_GETTER(ClassName, PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_GETTER(PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_SETTER(PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_INITTER(PropertyName)

/**
 * Base set of stats for characters
 */
UCLASS()
class ASCENSION_API UBaseStatSet : public UAttributeSet
{
	GENERATED_BODY()

private:
	UPROPERTY()
	TMap<EAffinity, int32> Affinities;

public:
	// Set default values
	UBaseStatSet();

	int32 GetAffinityValue(EAffinity Affinity) const;

	bool SetAffinities(TMap<EAffinity, int32> NewAffinities);

	/* Base Attribute Stats */
	UPROPERTY(Category = "Base Stats", EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData Might;
	ATTRIBUTE_ACCESSORS(UBaseStatSet, Might)
	
	UPROPERTY(Category = "Base Stats", EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData Finesse;
	ATTRIBUTE_ACCESSORS(UBaseStatSet, Finesse)

	UPROPERTY(Category = "Base Stats", EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData Health;
	ATTRIBUTE_ACCESSORS(UBaseStatSet, Health)

	UPROPERTY(Category = "Base Stats", EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData Energy;
	ATTRIBUTE_ACCESSORS(UBaseStatSet, Energy)

	UPROPERTY(Category = "Base Stats", EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData Ether;
	ATTRIBUTE_ACCESSORS(UBaseStatSet, Ether)

	UPROPERTY(Category = "Base Stats", EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData MaxHealth;
	ATTRIBUTE_ACCESSORS(UBaseStatSet, MaxHealth)

	UPROPERTY(Category = "Base Stats", EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData MaxEnergy;
	ATTRIBUTE_ACCESSORS(UBaseStatSet, MaxEnergy)

	UPROPERTY(Category = "Base Stats", EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData MaxEther;
	ATTRIBUTE_ACCESSORS(UBaseStatSet, MaxEther)

	/* Regen Stats */
	UPROPERTY(Category = "Base Stats", EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData MightRegenRate;
	ATTRIBUTE_ACCESSORS(UBaseStatSet, MightRegenRate)

	UPROPERTY(Category = "Base Stats", EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData FinesseRegenRate;
	ATTRIBUTE_ACCESSORS(UBaseStatSet, FinesseRegenRate)

	UPROPERTY(Category = "Base Stats", EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData AffinityRegenRate;
	ATTRIBUTE_ACCESSORS(UBaseStatSet, AffinityRegenRate)

	UPROPERTY(Category = "Base Stats", EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData HealthRegenRate;
	ATTRIBUTE_ACCESSORS(UBaseStatSet, HealthRegenRate)

	UPROPERTY(Category = "Base Stats", EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData EnergyRegenRate;
	ATTRIBUTE_ACCESSORS(UBaseStatSet, EnergyRegenRate)

	UPROPERTY(Category = "Base Stats", EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData EtherRegenRate;
	ATTRIBUTE_ACCESSORS(UBaseStatSet, EtherRegenRate)

	/* Defensive Stats and Resistances */
	UPROPERTY(Category = "Base Stats", EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData Evasion;
	ATTRIBUTE_ACCESSORS(UBaseStatSet, Evasion)

	UPROPERTY(Category = "Base Stats", EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData BludgeoningResistance;
	ATTRIBUTE_ACCESSORS(UBaseStatSet, BludgeoningResistance)

	UPROPERTY(Category = "Base Stats", EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData PiercingResistance;
	ATTRIBUTE_ACCESSORS(UBaseStatSet, PiercingResistance)

	UPROPERTY(Category = "Base Stats", EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData SlashingResistance;
	ATTRIBUTE_ACCESSORS(UBaseStatSet, SlashingResistance)

	UPROPERTY(Category = "Base Stats", EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData FireResistance;
	ATTRIBUTE_ACCESSORS(UBaseStatSet, FireResistance)

	/* General Stats */
	UPROPERTY(Category = "Base Stats", EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData MovementSpeed;
	ATTRIBUTE_ACCESSORS(UBaseStatSet, MovementSpeed)

	// Calcuate the current percentage of displayed stats
	float GetCurrentHealthPercentage();
	float GetCurrentEnergyPercentage();
	float GetCurrentEtherPercentage();

	virtual void PreAttributeChange(const FGameplayAttribute& Attribute, float& NewValue) override;
	virtual void PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data) override;

protected:
	virtual AUBaseCharacter* GetSourceCharacter(const FGameplayEffectModCallbackData& Data);

	virtual AUBaseCharacter* GetTargetCharacter(const FGameplayEffectModCallbackData& Data);

	virtual const FHitResult* GetHitResult(const FGameplayEffectModCallbackData& Data);
};