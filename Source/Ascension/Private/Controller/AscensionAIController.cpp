#include "Controller/AscensionAIController.h"
#include "World/AscensionGameInstance.h"
#include "Math/Vector.h"
#include "Perception/AISense_Sight.h"
#include "NavigationSystem.h"
#include "Ability/CharacterAbilitySystemComponent.h"
#include "Stats/BaseStatSet.h"
#include "AI/Goal/PatrolGoal.h"
#include "Kismet/KismetMathLibrary.h"
#include "Ability/Actor/BlastProjectile.h"
#include "NavigationPath.h"

AAscensionAIController::AAscensionAIController()
{
}

TArray<FAtom> AAscensionAIController::GetCurrentWorldState()
{
	// TODO: Would like to "age off" the world state every few seconds. Currently this causes the game to completely freeze for ~5 seconds
	/*FTimespan Age = FDateTime::Now().operator-(WorldStateUpdateTime);
	if (Age.GetSeconds() > 2)
	{
		UpdateWorldState();
		WorldStateUpdateTime = FDateTime::Now();
	}*/
	UpdateWorldState();
	return CurrentWorldState;
}

TMap<EAIWorldStateAttribute, bool> AAscensionAIController::GetCurrentWorldStateMap()
{
	UpdateWorldState();
	return CurrentWorldStateMap;
}

void AAscensionAIController::UpdateWorldState()
{
	// TODO: create a generate current world state/update current world state to handle
		// updating the TArray of world state booleans.
	// TODO: find target: AUBaseCharacter* Target = ;
		// Populate this from a list of basecharacters stored in the gameInstance?
		// GetList() -> Loop through and get the closest -> set as target
	CurrentWorldState.Empty();
	CurrentWorldStateMap.Empty();
	if (Self)
	{
		FAtom IsAlive, EnemyIsAlive, NeedHealing, HasTarget, IsInRange, TargetEnemyIsHealthier, EnemiesInRange;
		IsAlive.name = GET_STRING_FROM_ENUM("EAIWorldStateAttribute", EAIWorldStateAttribute::IsAlive);
		IsAlive.value = Self->IsAlive();
		AddToWorldState(EAIWorldStateAttribute::IsAlive, IsAlive);

		EnemyIsAlive.name = GET_STRING_FROM_ENUM("EAIWorldStateAttribute", EAIWorldStateAttribute::EnemyIsAlive);
		EnemyIsAlive.value = true;
		AddToWorldState(EAIWorldStateAttribute::EnemyIsAlive, EnemyIsAlive);

		UBaseStatSet* StatSet = Self->BaseStatSet;
		if (StatSet)
		{
			NeedHealing.name = GET_STRING_FROM_ENUM("EAIWorldStateAttribute", EAIWorldStateAttribute::NeedHealing);
			NeedHealing.value = StatSet->GetCurrentHealthPercentage() < 0.4; // Below 40%
			AddToWorldState(EAIWorldStateAttribute::NeedHealing, NeedHealing);
		}

		// Check for characters within the MaxRange(1000)
		TArray<TEnumAsByte<EObjectTypeQuery>> ObjectTypeQuery;
		TArray<AActor*> OverlapActors;
		// TODO: Witin MaxRange, get from game constants data table
		UKismetSystemLibrary::SphereOverlapActors(this, Self->GetActorLocation(), 1000.0, ObjectTypeQuery, AUBaseCharacter::StaticClass(), 
			TArray<AActor*>(), OverlapActors);
		EnemiesInRange.name = GET_STRING_FROM_ENUM("EAIWorldStateAttribute", EAIWorldStateAttribute::EnemiesInRange);
		EnemiesInRange.value = OverlapActors.Num() > 1; // Offset to 1 because the overlap always finds self
		AddToWorldState(EAIWorldStateAttribute::EnemiesInRange, EnemiesInRange);

		HasTarget.name = GET_STRING_FROM_ENUM("EAIWorldStateAttribute", EAIWorldStateAttribute::HasTarget);
		HasTarget.value = CurrentTarget != nullptr;
		AddToWorldState(EAIWorldStateAttribute::HasTarget, HasTarget);

		if (CurrentTarget)
		{
			// Distance is uninterrupted and less than X (spell range?)
			float Distance = FVector::Distance(Self->GetActorLocation(), CurrentTarget->GetActorLocation());
			IsInRange.name = GET_STRING_FROM_ENUM("EAIWorldStateAttribute", EAIWorldStateAttribute::IsInRange);
			IsInRange.value = (Distance <= 500.f);//TODO: Calculate distance between self and target
			AddToWorldState(EAIWorldStateAttribute::IsInRange, IsInRange);

			UBaseStatSet* TargetStatSet = CurrentTarget->BaseStatSet;
			if (TargetStatSet && StatSet)
			{
				TargetEnemyIsHealthier.name = GET_STRING_FROM_ENUM("EAIWorldStateAttribute", EAIWorldStateAttribute::TargetEnemyIsHealthier);
				TargetEnemyIsHealthier.value = (StatSet->GetCurrentHealthPercentage() < TargetStatSet->GetCurrentHealthPercentage());
				AddToWorldState(EAIWorldStateAttribute::TargetEnemyIsHealthier, TargetEnemyIsHealthier);
			}
		}

		UCharacterAbilitySystemComponent* AbilitySystem = Self->AbilitySystem;
		if (AbilitySystem)
		{
			// Declaration for ability FAtoms
			FAtom AbilityOneAvailable, AbilityTwoAvailable, AbilityThreeAvailable, AbilityFourAvailable, AbilityFiveAvailable, HasAttackAvailable, RecoverAvailable;
			AbilityOneAvailable.name = GET_STRING_FROM_ENUM("EAIWorldStateAttribute", EAIWorldStateAttribute::AbilityOneAvailable);
			AbilityOneAvailable.value = AbilitySystem->IsAbilityAtIndexAvailable(0);
			AddToWorldState(EAIWorldStateAttribute::AbilityOneAvailable, AbilityOneAvailable);

			AbilityTwoAvailable.name = GET_STRING_FROM_ENUM("EAIWorldStateAttribute", EAIWorldStateAttribute::AbilityTwoAvailable);
			AbilityTwoAvailable.value = AbilitySystem->IsAbilityAtIndexAvailable(1);
			AddToWorldState(EAIWorldStateAttribute::AbilityTwoAvailable, AbilityTwoAvailable);

			AbilityThreeAvailable.name = GET_STRING_FROM_ENUM("EAIWorldStateAttribute", EAIWorldStateAttribute::AbilityThreeAvailable);
			AbilityThreeAvailable.value = AbilitySystem->IsAbilityAtIndexAvailable(2);
			AddToWorldState(EAIWorldStateAttribute::AbilityThreeAvailable, AbilityThreeAvailable);

			AbilityFourAvailable.name = GET_STRING_FROM_ENUM("EAIWorldStateAttribute", EAIWorldStateAttribute::AbilityFourAvailable);
			AbilityFourAvailable.value = AbilitySystem->IsAbilityAtIndexAvailable(3);
			AddToWorldState(EAIWorldStateAttribute::AbilityFourAvailable, AbilityFourAvailable);

			AbilityFiveAvailable.name = GET_STRING_FROM_ENUM("EAIWorldStateAttribute", EAIWorldStateAttribute::AbilityFiveAvailable);
			AbilityFiveAvailable.value = AbilitySystem->IsAbilityAtIndexAvailable(4);
			AddToWorldState(EAIWorldStateAttribute::AbilityFiveAvailable, AbilityFiveAvailable);

			HasAttackAvailable.name = GET_STRING_FROM_ENUM("EAIWorldStateAttribute", EAIWorldStateAttribute::HasAttackAvailable);
			HasAttackAvailable.value = (AbilitySystem->HasAbilityDescriptor(0, EAbilityDescriptor::Attack) && AbilityOneAvailable.value) ||
				(AbilitySystem->HasAbilityDescriptor(1, EAbilityDescriptor::Attack) && AbilityTwoAvailable.value) ||
				(AbilitySystem->HasAbilityDescriptor(2, EAbilityDescriptor::Attack) && AbilityThreeAvailable.value) ||
				(AbilitySystem->HasAbilityDescriptor(3, EAbilityDescriptor::Attack) && AbilityFourAvailable.value) ||
				(AbilitySystem->HasAbilityDescriptor(4, EAbilityDescriptor::Attack) && AbilityFiveAvailable.value);
			AddToWorldState(EAIWorldStateAttribute::HasAttackAvailable, HasAttackAvailable);

			RecoverAvailable.name = GET_STRING_FROM_ENUM("EAIWorldStateAttribute", EAIWorldStateAttribute::RecoverAvailable);
			RecoverAvailable.value = AbilitySystem->IsAbilityAtIndexAvailable(5);
			AddToWorldState(EAIWorldStateAttribute::RecoverAvailable, RecoverAvailable);
		}
	}
	else
	{
		UE_LOG(LogAscension, Error, TEXT("Could not find Self Character."));
	}
}

void AAscensionAIController::AddToWorldState(EAIWorldStateAttribute Attribute, FAtom AttributeAtom)
{
	CurrentWorldState.Add(AttributeAtom);
	CurrentWorldStateMap.Add(Attribute, AttributeAtom.value);
}

AUBaseCharacter* AAscensionAIController::GetCurrentTarget()
{
	return CurrentTarget;
}

FVector AAscensionAIController::GetLastKnownStimulusLocation()
{
	return LastKnownStimulusLocation;
}

bool AAscensionAIController::GetRandomPointInRadius(const FVector& Origin, float Radius, FVector& Result)
{
	UNavigationSystemV1* NavigationSystem = FNavigationSystem::GetCurrent<UNavigationSystemV1>(this);//GetWorld());
	if (NavigationSystem)
	{
		return NavigationSystem->K2_GetRandomReachablePointInRadius(this, Origin, Result, Radius);
	}
	return false;
}

bool AAscensionAIController::CanReachLocation(FVector PathStart, FVector PathEnd)
{
	UNavigationSystemV1* NavigationSystem = FNavigationSystem::GetCurrent<UNavigationSystemV1>(this);
	if (NavigationSystem)
	{
		UNavigationPath* NavPath = NavigationSystem->FindPathToLocationSynchronously(GetWorld(), PathStart, PathEnd, NULL);
		return NavPath ? !NavPath->IsPartial() : false;
	}
	return false;
}

UBaseAIGoal* AAscensionAIController::GetCurrentGoal()
{
	return CurrentGoal;
}

UBaseAIGoal* AAscensionAIController::FindBestGoal()
{
	UBaseAIGoal* SelectedGoal = nullptr;
	float SelectedGoalUtility = -1;

	for (int32 i = 0; i < GoalsArray.Num(); i++)
	{
		float Utility = GoalsArray[i]->CalculateUtility(CurrentWorldStateMap);
		if (SelectedGoal == nullptr)
		{
			SelectedGoal = GoalsArray[i];
			SelectedGoalUtility = Utility;
		}
		else if (Utility > SelectedGoalUtility)
		{
			SelectedGoal = GoalsArray[i];
			SelectedGoalUtility = Utility;
		}
	}

	return SelectedGoal;
}

void AAscensionAIController::SelectNewGoal()
{
	UBaseAIGoal* NewGoal = FindBestGoal();
	if (NewGoal)
	{
		// There was not a goal currently being pursued or a better goal was found
		if (NewGoal)
		{
			CurrentGoal = NewGoal;
		}
		else
		{
			UE_LOG(LogAscension, Error, TEXT("New Goal was NULL."));
		}

		// Update the current goal
		setCurrentWorld(GetCurrentWorldState());
		setGoal(CurrentGoal->GetDesiredWorldState());
	}
}

AUBaseCharacter* AAscensionAIController::FindBestTarget(TArray<AActor*> PerceivedActors)
{
	// TODO: better algorithm for choosing targets (health, proximity etc.)
	if (PerceivedActors.Num() > 0)
	{
		return Cast<AUBaseCharacter>(PerceivedActors[0]);
	}
	return nullptr;
}

void AAscensionAIController::OnPerceptionUpdated(const TArray<AActor*>& UpdatedActors)
{
	if (PerceptionComponent)
	{
		for (int32 i = 0; i < UpdatedActors.Num(); i++)
		{
			if (UpdatedActors[i]->IsA(AUBaseCharacter::StaticClass()))
			{
				FActorPerceptionBlueprintInfo PerceptionInfo;
				if (PerceptionComponent->GetActorsPerception(UpdatedActors[i], PerceptionInfo))
				{
					FAIStimulus Stimulus = PerceptionInfo.LastSensedStimuli[0];
					if (!Stimulus.WasSuccessfullySensed())
					{
						// TODO: Lost track of this enemy, store all positions in a map?
						LastKnownStimulusLocation = Stimulus.StimulusLocation;
					}
				}
			}
			else if (UpdatedActors[i]->IsA(ABlastProjectile::StaticClass()))
			{
				// The AI sees a blast projectile, we need to determine if its going to hit us, if so, try to dodge
				ABlastProjectile* BlastProjectile = Cast<ABlastProjectile>(UpdatedActors[i]);
				if (BlastProjectile)
				{
					BlastProjectile->ProjectileMovementComponent;

					// TODO: find a way to do this more naturally, maybe with a flag
					// Update the AI to dodge
					// TODO: if projectile will hit me!!!
					if (BlastProjectile->GetInstigator() != Self)
					{
						// Update World State
						UpdateWorldState();

						// Set "NeedToDodge"
						FAtom NeedToDodge;
						NeedToDodge.name = GET_STRING_FROM_ENUM("EAIWorldStateAttribute", EAIWorldStateAttribute::NeedToDodge);
						NeedToDodge.value = true;
						AddToWorldState(EAIWorldStateAttribute::NeedToDodge, NeedToDodge);

						// Set the world state
						setCurrentWorld(CurrentWorldState);

						// Set the goal
						setGoal(FindBestGoal()->GetDesiredWorldState());

						// ExecuteGoap
						executeGOAP();
					}
				}

				// TODO: when hit by beam, make sure to move out of the way (onHitEvent)
					// or OnHit, turn towards stimulus, see actor, dodge???
			}
		}

		// TODO: We only want to interupt certain actions/goals under specific circumstances
		if (!CurrentGoal || CurrentGoal->IsA(UPatrolGoal::StaticClass()) || IsPlanComplete())
		{
			// Choose a new target
			TArray<AActor*> PerceivedActors;
			PerceptionComponent->GetCurrentlyPerceivedActors(UAISense_Sight::StaticClass(), PerceivedActors);
			AUBaseCharacter* NewTarget = FindBestTarget(PerceivedActors);

			// Choose a new goal
			SelectNewGoal();

			// Execute the GOAP Plan for the goal selected above
			executeGOAP();
		}
	}
}

void AAscensionAIController::InitializeGoals(FAIConfigTableRow Data)
{
	TArray<TSubclassOf<UBaseAIGoal>> Goals = Data.GoalsList;

	// Add each entry in the DataTable as a new goal
	for (int32 i = 0; i < Goals.Num(); i++)
	{
		GoalsArray.Add(NewObject<UBaseAIGoal>(this, Goals[i]));
	}
}


void AAscensionAIController::PrintCurrentAndDesiredWorldStates()
{
	UE_LOG(LogAscension, Warning, TEXT("Current State"));
	PrintState(getCurrentWorldStateAtoms());
	UE_LOG(LogAscension, Warning, TEXT("Desired State"));
	PrintState(getDesiredWorldStateAtoms());
}

void AAscensionAIController::PrintState(TArray<FAtom> State)
{
	UE_LOG(LogAscension, Warning, TEXT("State size: %d"), State.Num());
	for (int32 i = 0; i < State.Num(); i++)
	{
		UE_LOG(LogAscension, Warning, TEXT("Name: %s, Value: %d"), *State[i].name, State[i].value);
	}
}

void AAscensionAIController::OnPlanCompleted()
{
	// Update the current target
	if (PerceptionComponent)
	{
		// Look at all of the currently perceived actors and check if there is a better target
		TArray<AActor*> PerceivedActors;
		PerceptionComponent->GetCurrentlyPerceivedActors(UAISense_Sight::StaticClass(), PerceivedActors);
		AUBaseCharacter* NewTarget = FindBestTarget(PerceivedActors);
		if (NewTarget)
		{
			if (CurrentTarget != NewTarget)
			{
				// The previous target left our perception, pick a new one from currently peceived actors
				CurrentTarget = NewTarget;
			}
		}
	}

	// Generate new GOAP and execute it
	setCurrentWorld(GetCurrentWorldState());
	SelectNewGoal();
	executeGOAP();
}

void AAscensionAIController::OnHitByAbilityActor(FVector ActorOrigin)
{
	// TODO: If not already facing the stimulus location, interrupt all actions and turn towards it
	if (Self)
	{
		FRotator Rotator = UKismetMathLibrary::FindLookAtRotation(Self->GetActorLocation(), ActorOrigin);
		Self->SetActorRotation(Rotator);
	}
}

void AAscensionAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AAscensionAIController::BeginPlay()
{
	// Set the Self reference
	Self = Cast<AUAICharacter>(GetPawn());

	// Set the perception component from the character
	if (Self)
	{
		SetPerceptionComponent(*Self->PerceptionComponent);
	}

	// Setup the AI perception events
	if (PerceptionComponent)
	{
		PerceptionComponent->OnPerceptionUpdated.AddDynamic(this, &AAscensionAIController::OnPerceptionUpdated);
	}
	else
	{
		UE_LOG(LogAscension, Error, TEXT("No perception component found for AI Controller."));
	}

	// Initialize the actions and goals from DataTables
	UWorld* World = GetWorld();
	FAIConfigTableRow* Data = GetRowData<FAIConfigTableRow>(World, EDataTableName::AIConfig, UAIConfigDataTable::GetRowName());
	if (Data)
	{
		InitializeGoals(*Data);
		actions = Data->ActionsList;
	}
	
	// Add the event listener for being hit by an ability
	OnHitByAbilityActorEvent.AddUObject(this, &AAscensionAIController::OnHitByAbilityActor);

	UpdateWorldState();

	setCurrentWorld(GetCurrentWorldState());
	setGoal(FindBestGoal()->GetDesiredWorldState());

	Super::BeginPlay();
}