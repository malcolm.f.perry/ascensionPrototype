#include "Controller/AscensionPlayerController.h"
#include "Ascension/Ascension.h"

void AUAscensionPlayerController::BeginPlay()
{
	Super::BeginPlay();
	SetInputMode(FInputModeGameAndUI());
}