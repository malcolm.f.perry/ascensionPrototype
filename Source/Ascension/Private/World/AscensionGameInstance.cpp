#include "World/AscensionGameInstance.h"

UAscensionGameInstance::UAscensionGameInstance(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	CharacterInfo = CreateDefaultSubobject<UCharacterInfo>(TEXT("CharacterInfo"));
}

TMap<EAffinity, int32> UAscensionGameInstance::GetAffinities() const
{
	return CharacterInfo ? CharacterInfo->GetAffinities() : TMap<EAffinity, int32> {};
}

bool UAscensionGameInstance::AddAffinity(EAffinity Affinity, int32 Value)
{
	return CharacterInfo ? CharacterInfo->AddAffinity(Affinity, Value) : false;
}

int32 UAscensionGameInstance::RemoveAffinity(EAffinity Affinity)
{
	return CharacterInfo ? CharacterInfo->RemoveAffinity(Affinity) : false;
}

TArray<UAbilityAttributes*> UAscensionGameInstance::GetAbilityAttributesArray() const
{
	return CharacterInfo ? CharacterInfo->GetAttributesList() : TArray<UAbilityAttributes*> {};
}

bool UAscensionGameInstance::AddAbilityAttributes(int32 Index, UAbilityAttributes* NewAttributes)
{
	return CharacterInfo ? CharacterInfo->AddAttributes(Index, NewAttributes) : false;
}

bool UAscensionGameInstance::RemoveAbilityAttributes(int32 Index)
{
	return CharacterInfo ? CharacterInfo->RemoveAttributes(Index) : false;
}

UDataTableManager * UAscensionGameInstance::GetDataTableManager() const
{
	return DataTableManager.GetDefaultObject();
}

TArray<AUBaseCharacter*> UAscensionGameInstance::GetGameMembers()
{
	return GameMembers;
}

void UAscensionGameInstance::AddGameMember(AUBaseCharacter* NewCharacter)
{
	GameMembers.Add(NewCharacter);
}

void UAscensionGameInstance::OnStart()
{
	Super::OnStart();
}