#include "DataTable/DataTableManager.h"

UDataTableManager::UDataTableManager(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{

}

UDataTable * UDataTableManager::GetDataTable(EDataTableName DataTableName) const
{
	auto Table = DataTablesMap.Find(DataTableName);
	return Table ? *Table : nullptr;
}