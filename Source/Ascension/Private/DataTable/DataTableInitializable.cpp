#include "DataTable/DataTableInitializable.h"
#include "World/AscensionGameInstance.h"
#include "Engine/World.h"

UDataTableManager * IDataTableInitializable::GetDataTableManager(UWorld* World)
{
	if (World)
	{
		UAscensionGameInstance* GameInstance = Cast<UAscensionGameInstance>(World->GetGameInstance());
		if (GameInstance)
		{
			return GameInstance->GetDataTableManager();
		}
		else
		{
			UE_LOG(LogAscension, Error, TEXT("Could not find a reference to the GameInstance."));
			return nullptr;
		}
	}
	else
	{
		UE_LOG(LogAscension, Error, TEXT("Could not get the GameInstance, World reference was NULL."));
		return nullptr;
	}
}

// Asynchronously load an Asset from a path, no callback
void IDataTableInitializable::LoadAsynchronous(FSoftObjectPath Path)
{
	FStreamableManager& StreamableManager = UAssetManager::GetStreamableManager();
	StreamableManager.RequestAsyncLoad(Path, [] {});
}

// Asynchronously load an Asset from a path
void IDataTableInitializable::LoadAsynchronous(FSoftObjectPath Path, FStreamableDelegate Delegate)
{
	FStreamableManager& StreamableManager = UAssetManager::GetStreamableManager();
	StreamableManager.RequestAsyncLoad(Path, Delegate);
}

// Asynchronously load an array of Asset paths, no callback
void IDataTableInitializable::LoadAsynchronous(TArray<FSoftObjectPath> Paths)
{
	FStreamableManager& StreamableManager = UAssetManager::GetStreamableManager();
	StreamableManager.RequestAsyncLoad(Paths, [] {});
}

// Asynchronously load an array of Asset paths
void IDataTableInitializable::LoadAsynchronous(TArray<FSoftObjectPath> Paths, FStreamableDelegate Delegate)
{
	FStreamableManager& StreamableManager = UAssetManager::GetStreamableManager();
	StreamableManager.RequestAsyncLoad(Paths, Delegate);
}

//void IDataTableInitializable::OnGameInstanceStart()
//{
//}