#include "Ability/CharacterAbilitySystemComponent.h"
#include "Abilities/GameplayAbility.h"
#include "DataTable/GameConstantsDataTable.h"
#include "Ability/RecoverAbility.h"
#include "Ability/SprintAbility.h"
#include "Ability/RollAbility.h"
#include "Ability/JumpAbility.h"

UCharacterAbilitySystemComponent::UCharacterAbilitySystemComponent(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	// TODO: Figure out how to move the below into a global event system that can be called after datatable initialization
	// Get the max number of abilities from the constants table
	//FGameConstantsTableRow* Row = GetRowData<FGameConstantsTableRow>(GetWorld(), EDataTableName::GameConstants,
	//	UGameConstantsDataTable::GetRowName());
	//int32 MaxNumAbilities = Row ? Row->MaxNumAbilities : 0;

	// Initialize the ability slots
	for (int32 i = 0; i < 5; i++)
	{
		ActiveAbilities.Add(nullptr);
		ActiveAbilityHandles.Add(FGameplayAbilitySpecHandle());
		ActiveAbilityAttributes.Add(nullptr);
	}
}

UAbilityAttributes* UCharacterAbilitySystemComponent::GetAttributesForAbility(int32 Index) const
{
	return ActiveAbilityAttributes.IsValidIndex(Index) ? ActiveAbilityAttributes[Index] : nullptr;
}

bool UCharacterAbilitySystemComponent::HasAbilityDescriptor(int32 Index, EAbilityDescriptor Descriptor)
{
	if (ActiveAbilityHandles.IsValidIndex(Index))
	{
		FGameplayAbilitySpec* Spec = FindAbilitySpecFromHandle(ActiveAbilityHandles[Index]);
		if (Spec)
		{
			UBaseGameplayAbility* Ability = Cast<UBaseGameplayAbility>(Spec->Ability);
			if (Ability)
			{
				return Ability->GetAbilityDescriptor() == Descriptor;
			}
		}
	}
	return false;
}

bool UCharacterAbilitySystemComponent::HasAbilityAtIndex(int32 Index)
{
	return ActiveAbilityHandles.IsValidIndex(Index);
}

bool UCharacterAbilitySystemComponent::IsAbilityAtIndex(int32 Index, FGameplayAbilitySpecHandle Handle)
{
	return (ActiveAbilityHandles.IsValidIndex(Index) && ActiveAbilityHandles[Index].operator==(Handle));
}

bool UCharacterAbilitySystemComponent::IsAbilityAtIndexAvailable(int32 Index)
{
	if (ActiveAbilityHandles.IsValidIndex(Index))
	{
		FGameplayAbilitySpec* Spec = FindAbilitySpecFromHandle(ActiveAbilityHandles[Index]);
		if (Spec)
		{
			UBaseGameplayAbility* Ability = Cast<UBaseGameplayAbility>(Spec->GetPrimaryInstance());
			if (Ability)
			{
				FGameplayAbilityActorInfo Info = Ability->GetActorInfo();
				return Ability->CanActivateAbility(ActiveAbilityHandles[Index], &Info);
			}
		}
	}
	return false;
}

bool UCharacterAbilitySystemComponent::ActivateAbilityAtIndex(int32 Index)
{
	if (ActiveAbilityHandles.IsValidIndex(Index))
	{
		return TryActivateAbility(ActiveAbilityHandles[Index]);
	}
	return false;
}

bool UCharacterAbilitySystemComponent::ActivateRecoverAbility()
{
	if (RecoverAbilityHandle.IsValid())
	{
		return TryActivateAbility(RecoverAbilityHandle);
	}
	return false;
}

void UCharacterAbilitySystemComponent::EndRecoverAbility()
{
	return CancelAbilityHandle(RecoverAbilityHandle);
}

bool UCharacterAbilitySystemComponent::ActivateSprintAbility()
{
	if (SprintAbilityHandle.IsValid())
	{
		return TryActivateAbility(SprintAbilityHandle);
	}
	return false;
}

void UCharacterAbilitySystemComponent::EndSprintAbility()
{
	return CancelAbilityHandle(SprintAbilityHandle);
}

bool UCharacterAbilitySystemComponent::ActivateRollAbility()
{
	if (RollAbilityHandle.IsValid())
	{
		return TryActivateAbility(RollAbilityHandle);
	}
	return false;
}

bool UCharacterAbilitySystemComponent::ActivateJumpAbility()
{
	if (JumpAbilityHandle.IsValid())
	{
		return TryActivateAbility(JumpAbilityHandle);
	}
	return false;
}

void UCharacterAbilitySystemComponent::EndJumpAbility()
{
	return CancelAbilityHandle(JumpAbilityHandle);
}

bool UCharacterAbilitySystemComponent::AddAbilityToIndex(int32 Index, UAbilityAttributes* Attributes)
{
	TSubclassOf<UGameplayAbility> Ability = Attributes ? Attributes->GetAbilityType() : nullptr;
	if (Index < 5 && Ability && ActiveAbilities.IsValidIndex(Index) && GetOwner()->HasAuthority())
	{
		UBaseGameplayAbility* GameplayAbility = Cast<UBaseGameplayAbility>((Ability->GetDefaultObject()));
		if (GameplayAbility)
		{
			ActiveAbilityAttributes[Index] = Attributes;
			ActiveAbilityHandles[Index] = GiveAbility(FGameplayAbilitySpec(GameplayAbility, 1, Index));
			ActiveAbilities[Index] = Ability;

			return true;
		}
	}

	return false;
}

bool UCharacterAbilitySystemComponent::RemoveAbilityFromIndex(int32 Index)
{
	if (ActiveAbilityHandles.IsValidIndex(Index) && ActiveAbilityHandles[Index].IsValid())
	{
		if (GetOwner()->HasAuthority())
		{
			ClearAbility(ActiveAbilityHandles[Index]);
			ActiveAbilityHandles[Index] = FGameplayAbilitySpecHandle();
			ActiveAbilities[Index] = nullptr;
			ActiveAbilityAttributes[Index] = nullptr;
			return true;
		}
	}
	return false;
}

void UCharacterAbilitySystemComponent::AbilityLocalInputPressed(int32 InputID)
{
	Super::AbilityLocalInputPressed(InputID);
}

void UCharacterAbilitySystemComponent::AbilityLocalInputReleased(int32 InputID)
{
	Super::AbilityLocalInputReleased(InputID);

	FGameplayAbilitySpecHandle Handle = ActiveAbilityHandles[InputID];
	if (Handle.IsValid() && ShouldCancelIfReleased(ActiveAbilities[InputID]))
	{
		CancelAbilityHandle(Handle);
	}
}

void UCharacterAbilitySystemComponent::CancelEnergyAbilities()
{
	// TODO: Make this more dynamic by querying tags
	EndSprintAbility();
}

void UCharacterAbilitySystemComponent::CancelEtherAbilities()
{
	// TODO: Make this more dynamic by querying tags?
	for (int32 i = 0; i < 5; i++)
	{
		CancelAbilityHandle(ActiveAbilityHandles[i]);
	}
}

FGameplayAbilitySpecHandle UCharacterAbilitySystemComponent::GiveBaseGameplayAbility(TSubclassOf<UBaseGameplayAbility> AbilityClass,
	int32 Level, int32 Id)
{
	// Initialize the ability
	UBaseGameplayAbility* GameplayAbility = AbilityClass ? Cast<UBaseGameplayAbility>(AbilityClass->GetDefaultObject()) : nullptr;
	if (GameplayAbility)
	{
		return GiveAbility(FGameplayAbilitySpec(GameplayAbility, Level, Id));
	}
	return FGameplayAbilitySpecHandle();
}

void UCharacterAbilitySystemComponent::BeginPlay()
{
	Super::BeginPlay();

	// TODO: Put the index of these static abilities into a data table
	RecoverAbilityHandle = GiveBaseGameplayAbility(URecoverAbility::StaticClass(), 1, 5);
	ActiveAbilityHandles.Add(RecoverAbilityHandle);

	SprintAbilityHandle = GiveBaseGameplayAbility(USprintAbility::StaticClass(), 1, 6);
	ActiveAbilityHandles.Add(SprintAbilityHandle);

	RollAbilityHandle = GiveBaseGameplayAbility(URollAbility::StaticClass(), 1, 7);
	ActiveAbilityHandles.Add(RollAbilityHandle);

	JumpAbilityHandle = GiveBaseGameplayAbility(UJumpAbility::StaticClass(), 1, 8);
	ActiveAbilityHandles.Add(JumpAbilityHandle);
}

bool UCharacterAbilitySystemComponent::ShouldCancelIfReleased(TSubclassOf<class UBaseGameplayAbility> Ability)
{
	if (Ability)
	{
		UBaseGameplayAbility* UBGAbility = Cast<UBaseGameplayAbility>(Ability->GetDefaultObject());
		if (UBGAbility)
		{
			return UBGAbility->ShouldCancelIfInputReleased();
		}
	}

	return false;
}