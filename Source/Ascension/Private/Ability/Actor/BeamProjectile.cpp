#include "Ability/Actor/BeamProjectile.h"
#include "DataTable/BeamAbilityDataTable.h"
#include "Character/BaseCharacter.h"

// Sets default values
ABeamProjectile::ABeamProjectile()
{
	// Initialize the root component
	DefaultSceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("DefaultSceneComponent"));
	RootComponent = DefaultSceneComponent;

	// Initialize the static mesh
	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComponent"));
	StaticMeshComponent->CanCharacterStepUpOn = ECB_No;
	StaticMeshComponent->BodyInstance.SetCollisionProfileName(TEXT("BeamProjectile"));
	StaticMeshComponent->AttachToComponent(DefaultSceneComponent, FAttachmentTransformRules(EAttachmentRule::KeepRelative, false));

	// Initialize the particle system component
	ParticleSystemComponent = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("ParticleSystemComponent"));
	ParticleSystemComponent->AttachToComponent(StaticMeshComponent, FAttachmentTransformRules(EAttachmentRule::KeepRelative, false));
}

void ABeamProjectile::ExecuteAbility(FActorAttributes NewActorAttributes, const FVector& Direction)
{
	Super::ExecuteAbility(NewActorAttributes, Direction);

	// TODO: Use these attributes to initialize values for the projectile: Range, Speed, Delay, Size, Affinity
	StaticMeshComponent->SetRelativeScale3D(FVector(1.f, 1.f, 20.f));
	StaticMeshComponent->AddRelativeRotation(FRotator(-90.f, 0.f, 0.f));
	StaticMeshComponent->SetVisibility(false);
	StaticMeshComponent->SetHiddenInGame(true);

	ParticleSystemComponent->SetRelativeScale3D(FVector(1.f, 1.f, 2.f));
	ParticleSystemComponent->AddRelativeLocation(FVector(0.f, 0.f, 20.f));

	// Add the projectile to the root socket so it stays relative to character movement
	AUBaseCharacter* Character = Cast<AUBaseCharacter>(GetInstigator());
	USkeletalMeshComponent* CharacterMesh = Character ? Character->GetMesh() : nullptr;
	if (CharacterMesh)
	{
		DefaultSceneComponent->AttachToComponent(CharacterMesh, FAttachmentTransformRules::KeepWorldTransform, FName("root"));
	}
	else
	{
		UE_LOG(LogAscension, Error, TEXT("Error finding character mesh for the BeamProjectile."));
	}

	// TODO: Change this init to get these UPROPERTYs from the blueprint config instead of the datatable
	// Get the data row and initialize values
	FString RowName = GET_STRING_FROM_ENUM("EAffinity", NewActorAttributes.Affinity);
	FBeamAbilityTableRow* Data = GetRowData<FBeamAbilityTableRow>(GetWorld(), EDataTableName::BeamAbility, RowName);
	if (Data)
	{
		// Initialize the ParticleSystem and OnHitFx
		ParticleSystem = Data->ActorParticles;
		StaticMesh = Data->AbilityShape;

		// If we need to load assets, load them asynchronously then finish execution
		TArray<FSoftObjectPath> Paths;
		if (!ParticleSystem.Get())
		{
			Paths.Add(ParticleSystem.ToSoftObjectPath());
		}

		if (!StaticMesh.Get())
		{
			Paths.Add(StaticMesh.ToSoftObjectPath());
		}

		if (Paths.Num() > 0)
		{
			LoadAsynchronous(Paths, FStreamableDelegate::CreateUObject(this, &ABeamProjectile::FinishExecute));
		}
		else
		{
			FinishExecute();
		}
	}
}

void ABeamProjectile::FinishExecute()
{
	Super::FinishExecute();

	if (ParticleSystem.Get())
	{
		ParticleSystemComponent->SetTemplate(ParticleSystem.Get());
	}

	if (StaticMesh.Get())
	{
		StaticMeshComponent->SetStaticMesh(StaticMesh.Get());
	}
}

void ABeamProjectile::BeginPlay()
{
	Super::BeginPlay();

	// Register overlap events for this projectile
	StaticMeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ABeamProjectile::OnOverlap);
	StaticMeshComponent->OnComponentEndOverlap.AddDynamic(this, &ABeamProjectile::OnEndOverlap);
}

void ABeamProjectile::OnOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	Super::OnOverlap(OverlappedComponent, OtherActor, OtherComponent, OtherBodyIndex, bFromSweep, SweepResult);

	// If the other actor is now ourself, add it to the map of actors
	AActor* AbilityInstigator = GetInstigator();
	if (OtherActor != AbilityInstigator)
	{
		OverlappedActors.Add(OtherActor->GetUniqueID(), OtherActor);
	}
}

void ABeamProjectile::OnEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* Other, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	// Remove the actor from map
	OverlappedActors.Remove(Other->GetUniqueID());
}

void ABeamProjectile::DoHit(AActor* OtherActor, const FHitResult& HitResult, FGameplayTag EventTag)
{
	Super::DoHit(OtherActor, HitResult, EventTag);

	AActor* AbilityInstigator = GetInstigator();
	if (OtherActor != AbilityInstigator)
	{
		// TODO: Add hit particles?
	}
}

void ABeamProjectile::Tick(float DeltaTime)
{
	DeltaSeconds += DeltaTime;

	// Apply damage to each overlapped actor each second
	if (DeltaSeconds > 1.f)
	{
		for (auto& Elem : OverlappedActors)
		{
			DoHit(Elem.Value, FHitResult(), FGameplayTag());
		}

		DeltaSeconds -= 1;
	}
}