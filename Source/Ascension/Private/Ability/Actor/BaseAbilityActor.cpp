#include "Ability/Actor/BaseAbilityActor.h"
#include "Character/BaseCharacter.h"
#include "Ability/CharacterAbilitySystemComponent.h"
#include "AbilitySystemBlueprintLibrary.h"
#include "Controller/AscensionAIController.h"
#include "Perception/AISense_Sight.h"
#include "Ability/BaseGameplayAbility.h"

// Sets default values
ABaseAbilityActor::ABaseAbilityActor()
{
	// Initialize Actor settings
	SetCanBeDamaged(false);

 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Register the ability actor as "see-able" for the AI Perception system
	StimuliSourceComponent = CreateDefaultSubobject<UAIPerceptionStimuliSourceComponent>(TEXT("StimuliSourceComponent"));
	StimuliSourceComponent->bAutoRegister = true;
	StimuliSourceComponent->RegisterForSense(UAISense_Sight::StaticClass());
}

void ABaseAbilityActor::FinishExecute()
{
	// Do final configuration and activation
}

// Called when the game starts or when spawned
void ABaseAbilityActor::BeginPlay()
{
	Super::BeginPlay();
	SetLifeSpan(10.0f); //TODO: Can i use this as a way of controlling Range?
	OriginLocation = GetActorLocation(); // Set the origin location

	// Get attributes from the ability and call execute
	AUBaseCharacter* InstigatorCharacter = Cast<AUBaseCharacter>(GetInstigator());
	UCharacterAbilitySystemComponent* SourceAbilitySystem = InstigatorCharacter ? InstigatorCharacter->AbilitySystem : nullptr;
	if (SourceAbilitySystem && SourceAbilitySystem->CurrentActiveAbility)
	{
		UBaseGameplayAbility* Ability = SourceAbilitySystem->CurrentActiveAbility;
		if (DamageEffect && SourceAbilitySystem->CurrentActiveAbility)
		{
			DamageEffectSpecHandle = SourceAbilitySystem->CurrentActiveAbility->MakeOutgoingGameplayEffectSpec(DamageEffect, 1.0f);
		}
		ExecuteAbility(Ability->GetActorAttributes(), FVector(1.0, 0.0, 0.0));
	}
}

void ABaseAbilityActor::OnHit(UPrimitiveComponent * HitComponent, AActor * OtherActor, UPrimitiveComponent * OtherComponent, FVector NormalImpulse, const FHitResult & Hit)
{
	DoHit(OtherActor, Hit, FGameplayTag::RequestGameplayTag(TEXT("GameplayEvent.AbilityActorHit")));
}

void ABaseAbilityActor::OnOverlap(UPrimitiveComponent * OverlappedComponent, AActor * OtherActor, UPrimitiveComponent * OtherComponent, int32 OtherBodyIndex, 
	bool bFromSweep, const FHitResult & SweepResult)
{
	DoHit(OtherActor, SweepResult, FGameplayTag::RequestGameplayTag(TEXT("GameplayEvent.AbilityActorOverlap")));
}

void ABaseAbilityActor::DoHit(AActor * OtherActor, const FHitResult & HitResult, FGameplayTag EventTag)
{
	AActor* AbilityInstigator = GetInstigator();
	if (OtherActor != AbilityInstigator)
	{
		// Apply Damage Effect
		AUBaseCharacter* HitCharacter = Cast<AUBaseCharacter>(OtherActor);
		if (HitCharacter != nullptr)
		{
			UCharacterAbilitySystemComponent* TargetAbilitySystem = HitCharacter->AbilitySystem;
			AUBaseCharacter* InstigatorCharacter = Cast<AUBaseCharacter>(AbilityInstigator);
			if (InstigatorCharacter && TargetAbilitySystem)
			{
				// Let the source AbilitySystem apply the damage to the target
				UCharacterAbilitySystemComponent* SourceAbilitySystem = InstigatorCharacter->AbilitySystem;
				if (SourceAbilitySystem)
				{
					if (DamageEffectSpecHandle.IsValid())
					{
						SourceAbilitySystem->ApplyGameplayEffectSpecToTarget(*DamageEffectSpecHandle.Data.Get(), TargetAbilitySystem);
					}

					// Broadcast the hit event to the ai controller if present
					AAscensionAIController* AIController = Cast<AAscensionAIController>(HitCharacter->GetController());
					if (AIController)
					{
						AIController->OnHitByAbilityActorEvent.Broadcast(OriginLocation);
					}
				}
			}
			else {
				// Have the target apply the damage to itself
				TargetAbilitySystem->ApplyGameplayEffectSpecToSelf(*DamageEffectSpecHandle.Data.Get());
			}
		}
	}
}

void ABaseAbilityActor::DestroyAndSendEvent(AActor* OtherActor, AActor* AbilityInstigator, FGameplayTag EventTag)
{
	// Send GameplayEvent to spawning ability and destroy actor
	FGameplayEventData Data;
	Data.Target = OtherActor;
	UAbilitySystemBlueprintLibrary::SendGameplayEventToActor(AbilityInstigator, EventTag, Data);
	UAbilitySystemBlueprintLibrary::SendGameplayEventToActor(AbilityInstigator,
		FGameplayTag::RequestGameplayTag(FName("GameplayEvent.AbilityActorDestroyed")), FGameplayEventData());
	Destroy();
}

// Called every frame
void ABaseAbilityActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ABaseAbilityActor::LifeSpanExpired()
{
	// Send ActorDestroyed event to the owning ability
	AActor* AbilityInstigator = GetInstigator();
	if (AbilityInstigator != nullptr)
	{
		UAbilitySystemBlueprintLibrary::SendGameplayEventToActor(AbilityInstigator,
			FGameplayTag::RequestGameplayTag(FName("GameplayEvent.AbilityActorDestroyed")), FGameplayEventData());
	}

	Super::LifeSpanExpired();
}

void ABaseAbilityActor::ExecuteAbility(FActorAttributes NewActorAttributes, const FVector & Direction)
{
	ActorAttributes = NewActorAttributes;
}