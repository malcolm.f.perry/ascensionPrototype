#include "Ability/Actor/BlastProjectile.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "DataTable/BlastAbilityDataTable.h"
#include "DataTable/GameConstantsDataTable.h"

// Sets default values
ABlastProjectile::ABlastProjectile()
{
	// Initialize the sphere component
	CollisionComponent = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent"));
	CollisionComponent->CanCharacterStepUpOn = ECB_No;
	CollisionComponent->BodyInstance.SetCollisionProfileName(TEXT("BlastProjectile"));
	RootComponent = CollisionComponent;

	// Initialize the projectile movement component
	ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovementComponent"));
	ProjectileMovementComponent->SetUpdatedComponent(CollisionComponent);
	ProjectileMovementComponent->bRotationFollowsVelocity = true;
	ProjectileMovementComponent->ProjectileGravityScale = 0;

	// Initialize the particle system component
	ParticleSystemComponent = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("ParticleSystemComponent"));
	ParticleSystemComponent->AttachToComponent(CollisionComponent, FAttachmentTransformRules(EAttachmentRule::KeepRelative, false));
}

void ABlastProjectile::ExecuteAbility(FActorAttributes NewActorAttributes, const FVector& Direction)
{
	Super::ExecuteAbility(NewActorAttributes, Direction);

	// TODO: Use these attributes to initialize values for the projectile: Range, Speed, Delay, Size, Affinity
	CollisionComponent->SetSphereRadius(NewActorAttributes.Size);
	ProjectileMovementComponent->SetVelocityInLocalSpace(Direction * NewActorAttributes.Speed);

	// TODO: Change this init to get these UPROPERTYs from the blueprint config instead of the datatable
	// Get the data row and initialize values
	FString RowName = GET_STRING_FROM_ENUM("EAffinity", NewActorAttributes.Affinity);
	FBlastAbilityTableRow* Data = GetRowData<FBlastAbilityTableRow>(GetWorld(), EDataTableName::BlastAbility, RowName);
	if (Data)
	{
		// Initialize the ParticleSystem and OnHitFx
		ParticleSystemComponent->AddLocalRotation(FQuat(Data->ActorParticlesRotation));
		ParticleSystem = Data->ActorParticles;
		OnHitFX = Data->OnHitParticles;

		TArray<FSoftObjectPath> Paths;
		if (!ParticleSystem.Get())
		{
			Paths.Add(ParticleSystem.ToSoftObjectPath());
		}

		if (!OnHitFX.Get())
		{
			Paths.Add(OnHitFX.ToSoftObjectPath());
		}

		// If we need to load assets, load them asynchronously then finish execution
		if (Paths.Num() > 0)
		{
			LoadAsynchronous(Paths, FStreamableDelegate::CreateUObject(this, &ABlastProjectile::FinishExecute));
		}
		else
		{
			FinishExecute();
		}
	}
}

void ABlastProjectile::FinishExecute()
{
	Super::FinishExecute();
	UParticleSystem* Particles = ParticleSystem.Get();
	if (Particles)
	{
		ParticleSystemComponent->SetTemplate(Particles);
	}
	ProjectileMovementComponent->Activate();
}

void ABlastProjectile::BeginPlay()
{
	Super::BeginPlay();

	// Get values from the constants table
	FGameConstantsTableRow* Row = GetRowData<FGameConstantsTableRow>(GetWorld(), EDataTableName::GameConstants,
		UGameConstantsDataTable::GetRowName());
	int32 MinSpeed = Row ? Row->FindMinValue(EAbilityAttributeName::Speed) : 1000;
	int32 MaxSpeed = Row ? Row->FindMaxValue(EAbilityAttributeName::Speed) : 1000;

	// Set the min/max speed for the projectile
	ProjectileMovementComponent->InitialSpeed = MinSpeed;
	ProjectileMovementComponent->MaxSpeed = MaxSpeed;

	// Register hit events for this projectile
	CollisionComponent->OnComponentHit.AddDynamic(this, &ABlastProjectile::OnHit);

	// Register overlap events for this projectile
	CollisionComponent->OnComponentBeginOverlap.AddDynamic(this, &ABlastProjectile::OnOverlap);
}

void ABlastProjectile::OnHit(UPrimitiveComponent * HitComponent, AActor * OtherActor, UPrimitiveComponent * OtherComponent, FVector NormalImpulse, const FHitResult & Hit)
{
	Super::OnHit(HitComponent, OtherActor, OtherComponent, NormalImpulse, Hit);
}

void ABlastProjectile::OnOverlap(UPrimitiveComponent * OverlappedComponent, AActor * OtherActor, UPrimitiveComponent * OtherComponent, int32 OtherBodyIndex, 
	bool bFromSweep, const FHitResult & SweepResult)
{
	Super::OnOverlap(OverlappedComponent, OtherActor, OtherComponent, OtherBodyIndex, bFromSweep, SweepResult);
}

void ABlastProjectile::DoHit(AActor* OtherActor, const FHitResult & HitResult, FGameplayTag EventTag)
{
	Super::DoHit(OtherActor, HitResult, EventTag);

	AActor* AbilityInstigator = GetInstigator();
	if (OtherActor != AbilityInstigator)
	{
		ProjectileMovementComponent->StopMovementImmediately();

		// Spawn particle emitter
		if (OnHitFX.Get())
		{
			UGameplayStatics::SpawnEmitterAtLocation(this, OnHitFX.Get(), HitResult.Location);
		}
		DestroyAndSendEvent(OtherActor, AbilityInstigator, EventTag);
	}
}