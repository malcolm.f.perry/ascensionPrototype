#include "Ability/BlastAbility.h"
#include "DataTable/BlastAbilityDataTable.h"

UBlastAbility::UBlastAbility(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

FVector UBlastAbility::GetSpawnTransformLocation(const AUBaseCharacter* Character)
{
	// Spawn the actor at the point between the left and right hands
	const USkeletalMeshComponent* Mesh = Character->GetMesh();
	FVector LeftHand = Mesh->GetSocketLocation("hand_l");
	FVector RightHand = Mesh->GetSocketLocation("hand_r");
	return LeftHand.operator+(RightHand).operator/(2);
}

FRotator UBlastAbility::GetSpawnTransformRotation(const AUBaseCharacter* Character)
{
	return Character->GetActorRotation();
}

void UBlastAbility::OnActorHit(FGameplayEventData GameplayEventData)
{
	Super::OnActorHit(GameplayEventData);
}

void UBlastAbility::OnActorOverlap(FGameplayEventData GameplayEventData)
{
	Super::OnActorOverlap(GameplayEventData);
}

void UBlastAbility::OnActorDestroyed(FGameplayEventData GameplayEventData)
{
	Super::OnActorDestroyed(GameplayEventData);
}

void UBlastAbility::OnAvatarSet(const FGameplayAbilityActorInfo * ActorInfo, const FGameplayAbilitySpec & Spec)
{
	Super::OnAvatarSet(ActorInfo, Spec);

	// Initialize properties from the data table
	FString RowName = GET_STRING_FROM_ENUM("EAffinity", GetAffinity());
	FBlastAbilityTableRow* Data = GetRowData<FBlastAbilityTableRow>(GetWorld(), EDataTableName::BlastAbility, RowName);
	if (Data)
	{
		AnimMontage = Data->Montage;
		AnimMontageStartSection = Data->MontageStartSection;
		MontagePlayRate = Data->MontagePlayRate;
		AbilityActor = Data->AbilityActor;
		SpawnActorDelay = Data->SpawnActorDelay;

		// Make sure the AnimMontage is loaded
		if (!AnimMontage.Get())
		{
			LoadAsynchronous(AnimMontage.ToSoftObjectPath());
		}
	}
}