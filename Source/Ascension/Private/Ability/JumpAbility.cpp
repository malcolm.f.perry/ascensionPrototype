#include "Ability/JumpAbility.h"
#include "Ability/Effect/EnergyCostEffect.h"
#include "Character/BaseCharacter.h"
#include "DataTable/JumpAbilityDataTable.h"

UJumpAbility::UJumpAbility(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	// Create the Cost Effects
	CostEffect = CreateDefaultSubobject<UEnergyCostEffect>(TEXT("EnergyCostEffect"));

	// Initialize the attributes
	Attributes = CreateDefaultSubobject<UAbilityAttributes>(TEXT("AbilityAttributes"));

	// TODO: Can this be initialized from a dataTable?
	// Add the blocked ability descriptor tag
	AbilityTags.AddTag(FGameplayTag::RequestGameplayTag(FName(*FString("GameplayTag.BlockedAbility"))));
	ActivationOwnedTags.AddTag(FGameplayTag::RequestGameplayTag(FName(*FString("GameplayTag.BlockedAbility"))));
}

void UJumpAbility::ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
	const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData)
{
	// Get the character reference
	AUBaseCharacter* CharacterActor = Cast<AUBaseCharacter>(ActorInfo->AvatarActor);
	if (CommitAbility(Handle, ActorInfo, ActivationInfo) && CharacterActor)
	{
		// Call Character.h jump
		CharacterActor->Jump();
	}
}

void UJumpAbility::EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
	const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateCancelAbility, bool bWasCancelled)
{
	Super::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateCancelAbility, bWasCancelled);

	// Get the character reference
	AUBaseCharacter* CharacterActor = Cast<AUBaseCharacter>(ActorInfo->AvatarActor);
	if (CharacterActor)
	{
		// Call Character.h stop jumping
		CharacterActor->StopJumping();
	}
}

void UJumpAbility::OnAvatarSet(const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilitySpec& Spec)
{
	// Initialize properties from the data table
	FJumpAbilityTableRow* Data = GetRowData<FJumpAbilityTableRow>(GetWorld(), EDataTableName::JumpAbility,
		UJumpAbilityDataTable::GetRowName());
	if (Data)
	{
		// Call parent to initialize the attributes
		if (Attributes)
		{
			Attributes->SetCost(Data->Cost);
			Attributes->SetAbilityDescriptor(EAbilityDescriptor::Movement);
			Initialize(Attributes);
		}
	}
}