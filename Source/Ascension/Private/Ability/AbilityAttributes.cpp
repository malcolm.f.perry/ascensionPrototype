#include "Ability/AbilityAttributes.h"
#include "DataTable/GameConstantsDataTable.h"

UAbilityAttributes::UAbilityAttributes(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

bool UAbilityAttributes::Validate(EAbilityAttributeName AttributeName, int32 Value)
{
	FString RowName = UGameConstantsDataTable::GetRowName();
	FGameConstantsTableRow* Data = GetRowData<FGameConstantsTableRow>(GetWorld(), EDataTableName::GameConstants, RowName);
	if (Data)
	{
		return Data->ValueInRange(AttributeName, Value);
	}
	else
	{
		return true;
	}
}

TSubclassOf<UBaseGameplayAbility> UAbilityAttributes::GetAbilityType() const
{
	return AbilityType;
}

bool UAbilityAttributes::SetAbilityType(TSubclassOf<UBaseGameplayAbility> NewAbilityType)
{
	AbilityType = NewAbilityType;
	return true;
}

int32 UAbilityAttributes::GetPower() const
{
	return Power;
}

bool UAbilityAttributes::SetPower(int32 NewPower)
{
	if (Validate(EAbilityAttributeName::Power, NewPower))
	{
		Power = NewPower;
		return true;
	}
	else {
		UE_LOG(LogAscension, Error, TEXT("Could not set Power, invalid value: %f"), NewPower);
		return false;
	}
}

int32 UAbilityAttributes::GetPenetration() const
{
	return Penetration;
}

bool UAbilityAttributes::SetPenetration(int32 NewPenetration)
{
	if (Validate(EAbilityAttributeName::Penetration, NewPenetration))
	{
		Penetration = NewPenetration;
		return true;
	}
	else {
		UE_LOG(LogAscension, Error, TEXT("Could not set Penetration, invalid value: %f"), NewPenetration);
		return false;
	}
}

int32 UAbilityAttributes::GetRange() const
{
	return Range;
}

bool UAbilityAttributes::SetRange(int32 NewRange)
{
	if (Validate(EAbilityAttributeName::Range, NewRange))
	{
		Range = NewRange;
		return true;
	}
	else {
		UE_LOG(LogAscension, Error, TEXT("Could not set Range, invalid value: %f"), NewRange);
		return false;
	}
}

int32 UAbilityAttributes::GetSpeed() const
{
	return Speed;
}

bool UAbilityAttributes::SetSpeed(int32 NewSpeed)
{
	if (Validate(EAbilityAttributeName::Speed, NewSpeed))
	{
		Speed = NewSpeed;
		return true;
	}
	else {
		UE_LOG(LogAscension, Error, TEXT("Could not set Speed, invalid value: %f"), NewSpeed);
		return false;
	}
}

int32 UAbilityAttributes::GetSize() const
{
	return Size;
}

bool UAbilityAttributes::SetSize(int32 NewSize)
{
	if (Validate(EAbilityAttributeName::Size, NewSize))
	{
		Size = NewSize;
		return true;
	}
	else {
		UE_LOG(LogAscension, Error, TEXT("Could not set Size, invalid value: %f"), NewSize);
		return false;
	}
}

int32 UAbilityAttributes::GetCastTime() const
{
	return CastTime;
}

bool UAbilityAttributes::SetCastTime(int32 NewCastTime)
{
	if (Validate(EAbilityAttributeName::CastTime, NewCastTime))
	{
		CastTime = NewCastTime;
		return true;
	}
	else {
		UE_LOG(LogAscension, Error, TEXT("Could not set CastTime, invalid value: %f"), NewCastTime);
		return false;
	}
}

int32 UAbilityAttributes::GetDuration() const
{
	return Duration;
}

bool UAbilityAttributes::SetDuration(int32 NewDuration)
{
	if (Validate(EAbilityAttributeName::Duration, NewDuration))
	{
		Duration = NewDuration;
		return true;
	}
	else {
		UE_LOG(LogAscension, Error, TEXT("Could not set Duration, invalid value: %f"), NewDuration);
		return false;
	}
}

int32 UAbilityAttributes::GetDelay() const
{
	return Delay;
}

bool UAbilityAttributes::SetDelay(int32 NewDelay)
{
	if (Validate(EAbilityAttributeName::Delay, NewDelay))
	{
		Delay = NewDelay;
		return true;
	}
	else {
		UE_LOG(LogAscension, Error, TEXT("Could not set Delay, invalid value: %f"), NewDelay);
		return false;
	}
}

int32 UAbilityAttributes::GetCooldown() const
{
	return Cooldown;
}

bool UAbilityAttributes::SetCooldown(int32 NewCooldown)
{
	if (Validate(EAbilityAttributeName::Cooldown, NewCooldown))
	{
		Cooldown = NewCooldown;
		return true;
	}
	else {
		UE_LOG(LogAscension, Error, TEXT("Could not set Cooldown, invalid value: %f"), NewCooldown);
		return false;
	}
}

int32 UAbilityAttributes::GetCost() const
{
	return Cost;
}

bool UAbilityAttributes::SetCost(int32 NewCost)
{
	if (Validate(EAbilityAttributeName::Cost, NewCost))
	{
		Cost = NewCost;
		return true;
	}
	else {
		UE_LOG(LogAscension, Error, TEXT("Could not set Cost, invalid value: %f"), NewCost);
		return false;
	}
}

EAffinity UAbilityAttributes::GetAffinity() const
{
	return Affinity;
}

bool UAbilityAttributes::SetAffinity(EAffinity NewAffinity)
{
	Affinity = NewAffinity;
	return true;
}

EAbilityDescriptor UAbilityAttributes::GetAbilityDescriptor() const
{
	return AbilityDescriptor;
}

bool UAbilityAttributes::SetAbilityDescriptor(EAbilityDescriptor NewAbilityDescriptor)
{
	AbilityDescriptor = NewAbilityDescriptor;
	return true;
}
