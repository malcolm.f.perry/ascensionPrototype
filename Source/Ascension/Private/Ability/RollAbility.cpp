#include "Ability/RollAbility.h"
#include "Ability/Effect/EnergyCostEffect.h"
#include "Character/BaseCharacter.h"
#include "Character/PlayerCharacter.h"
#include "DataTable/RollAbilityDataTable.h"

URollAbility::URollAbility(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	// Create the Cost Effects
	CostEffect = CreateDefaultSubobject<UEnergyCostEffect>(TEXT("EnergyCostEffect"));

	// Initialize the attributes
	Attributes = CreateDefaultSubobject<UAbilityAttributes>(TEXT("AbilityAttributes"));
}

void URollAbility::ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, 
	const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData)
{
	// Get the character reference
	AUBaseCharacter* CharacterActor = Cast<AUBaseCharacter>(ActorInfo->AvatarActor);
	UCharacterAbilitySystemComponent* AbilitySystemComponent = CharacterActor ? CharacterActor->AbilitySystem : nullptr;

	if (CommitAbility(Handle, ActorInfo, ActivationInfo) && AnimMontage && CharacterActor && AbilitySystemComponent)
	{
		if (CharacterActor->IsA(AUPlayerCharacter::StaticClass()))
		{
			AUPlayerCharacter* PlayerCharacter = Cast<AUPlayerCharacter>(CharacterActor);
			// Stop inputs from disrupting the animation
			PlayerCharacter->DisablePlayerInput();
		}

		// Determine duration, (montage length) / (montage play rate + 1)
		// This makes it so the player regains control just before the animation completes
		float AnimDuration = AbilitySystemComponent->PlayMontage(this, ActivationInfo, AnimMontage.Get(),
			MontagePlayRate, AnimMontageStartSection) / (MontagePlayRate + 1);

		// Wait for the montage to play,the re-enable input
		GetWorld()->GetTimerManager().SetTimer(AnimTimerHandle, this, &URollAbility::EndRollAbility, AnimDuration, false);
	}
}

void URollAbility::EndRollAbility()
{
	AUPlayerCharacter* PlayerCharacter = Cast<AUPlayerCharacter>(CurrentActorInfo->AvatarActor);
	if (PlayerCharacter)
	{
		PlayerCharacter->EnablePlayerInput();
	}
	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, false);
}

void URollAbility::OnAvatarSet(const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilitySpec& Spec)
{
	// Initialize properties from the data table
	FRollAbilityTableRow* Data = GetRowData<FRollAbilityTableRow>(GetWorld(), EDataTableName::RollAbility,
		URollAbilityDataTable::GetRowName());
	if (Data)
	{
		// Call parent to initialize the attributes
		if (Attributes)
		{
			Attributes->SetCost(Data->Cost);
			Attributes->SetAbilityDescriptor(EAbilityDescriptor::Movement);
			Initialize(Attributes);
		}

		AnimMontage = Data->Montage;
		AnimMontageStartSection = Data->MontageStartSection;
		MontagePlayRate = Data->MontagePlayRate;

		// Make sure the AnimMontage is loaded
		TArray<FSoftObjectPath> Paths;
		if (!AnimMontage.Get())
		{
			LoadAsynchronous(AnimMontage.ToSoftObjectPath());
		}
	}
}