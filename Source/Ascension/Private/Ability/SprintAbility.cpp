#include "Ability/SprintAbility.h"
#include "DataTable/SprintAbilityDataTable.h"

USprintAbility::USprintAbility(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	// Disable the default cost effect
	bHasCostEffect = false;

	// Create the Sprinting Effects
	SprintCostEffect = CreateDefaultSubobject<UPeriodicEnergyCostEffect>(TEXT("SprintCostEffect"));
	SprintMovementSpeedEffect = CreateDefaultSubobject<USprintMovementSpeedEffect>(TEXT("SprintMovementSpeedEffect"));

	// Initialize the attributes
	Attributes = CreateDefaultSubobject<UAbilityAttributes>(TEXT("AbilityAttributes"));

	// TODO: Can this be initialized from a dataTable?
	// Add the channeled ability descriptor tag
	AbilityTags.AddTag(FGameplayTag::RequestGameplayTag(FName(*FString("GameplayTag.ChanneledAbility"))));
}

void USprintAbility::ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
	const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData)
{
	// Make the cost more of a curve? smoother? cost 1, 5, 10 ...?
	// TODO: if energy is > 0???
	if (CommitAbility(Handle, ActorInfo, ActivationInfo) && SprintCostEffect && SprintMovementSpeedEffect)
	{
		SprintCostEffectHandle = ApplyGameplayEffectToOwner(Handle, ActorInfo, ActivationInfo, SprintCostEffect, GetAbilityLevel());
		SprintMovementSpeedEffectHandle = ApplyGameplayEffectToOwner(Handle, ActorInfo, ActivationInfo, SprintMovementSpeedEffect, 
			GetAbilityLevel());
	}
	else
	{
		EndAbility(Handle, ActorInfo, ActivationInfo, true, false);
	}
}

void USprintAbility::EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
	const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateCancelAbility, bool bWasCancelled)
{
	Super::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateCancelAbility, bWasCancelled);

	UAbilitySystemComponent* AbilitySystem = GetAbilitySystemComponentFromActorInfo();
	if (AbilitySystem)
	{
		// End the effects
		AbilitySystem->RemoveActiveGameplayEffect(SprintCostEffectHandle);
		AbilitySystem->RemoveActiveGameplayEffect(SprintMovementSpeedEffectHandle);
	}
}

void USprintAbility::OnAvatarSet(const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilitySpec& Spec)
{
	// Initialize properties from the data table
	FSprintAbilityTableRow* Data = GetRowData<FSprintAbilityTableRow>(GetWorld(), EDataTableName::SprintAbility,
		USprintAbilityDataTable::GetRowName());
	if (Data)
	{
		// TODO: Investigate how to use this in place of the constructor call
		// Set this ability as a channeled ability
		//AbilityTags.AppendTags(Data->AbilityTags);

		// Call parent to initialize the attributes
		if (Attributes)
		{
			Attributes->SetCost(Data->Cost);
			Attributes->SetAbilityDescriptor(EAbilityDescriptor::Movement);
			Initialize(Attributes);
		}
	}
}