#include "Ability/BaseGameplayAbility.h"
#include "Ability/CharacterAbilitySystemComponent.h"
#include "Abilities/Tasks/AbilityTask_WaitGameplayEvent.h"
#include "Stats/BaseStatSet.h"
#include "Ability/AbilityAttributes.h"
#include "Ability/Effect/EtherCostEffect.h"
#include "Macros/Macros.h"
#include "Engine/World.h"

UBaseGameplayAbility::UBaseGameplayAbility(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	// Each Actor with an ability will create 1 instance of it to use.
	InstancingPolicy = EGameplayAbilityInstancingPolicy::InstancedPerActor;

	// TODO: Can this be initialized from a dataTable?
	// Add the blocked ability descriptor tag
	ActivationBlockedTags.AddTag(FGameplayTag::RequestGameplayTag(FName(*FString("GameplayTag.BlockedAbility"))));
	ActivationBlockedTags.AddTag(FGameplayTag::RequestGameplayTag(FName(*FString("GameplayTag.ChanneledAbility"))));
	SetShouldBlockOtherAbilities(true);

	// Create the Gameplay effects
	CostEffect = CreateDefaultSubobject<UEtherCostEffect>(TEXT("EtherCostEffect"));
	CooldownEffect = CreateDefaultSubobject<UCooldownEffect>(TEXT("CooldownEffect"));
	DamageEffect = CreateDefaultSubobject<UDamageEffect>(TEXT("DamageEffect"));
}

bool UBaseGameplayAbility::Initialize(UAbilityAttributes* NewAttributes)
{
	if (bWasInitialized)
	{
		UE_LOG(LogAscension, Error, TEXT("This Ability is already initialized."));
		return false;
	}
	else {
		// Initialize this abilities attributes from the AbilityAttributes object
		bWasInitialized = true;
		if (NewAttributes)
		{
			Power = NewAttributes->GetPower();
			Penetration = NewAttributes->GetPenetration();
			Range = NewAttributes->GetRange();
			Speed = NewAttributes->GetSpeed();
			Size = NewAttributes->GetSize();
			CastTime = NewAttributes->GetCastTime();
			Duration = NewAttributes->GetDuration();
			Delay = NewAttributes->GetDelay();
			Cooldown = NewAttributes->GetCooldown();
			Cost = NewAttributes->GetCost();
			Affinity = NewAttributes->GetAffinity();
			AbilityDescriptor = NewAttributes->GetAbilityDescriptor();

			// Initialize an ActorAttributes struct used to pass attributes to a spawned actor
			ActorAttributes = FActorAttributes::FActorAttributes(Range, Speed, Size, Delay, Affinity);

			// After initialization we can clean up this object
			NewAttributes->MarkPendingKill();
		}
		return bWasInitialized;
	}
}

int32 UBaseGameplayAbility::GetPower() const
{
	return Power;
}

int32 UBaseGameplayAbility::GetPenetration() const
{
	return Penetration;
}

int32 UBaseGameplayAbility::GetRange() const
{
	return Range;
}

int32 UBaseGameplayAbility::GetSpeed() const
{
	return Speed;
}

int32 UBaseGameplayAbility::GetSize() const
{
	return Size;
}

int32 UBaseGameplayAbility::GetCastTime() const
{
	return CastTime;
}

int32 UBaseGameplayAbility::GetDuration() const
{
	return Duration;
}

int32 UBaseGameplayAbility::GetDelay() const
{
	return Delay;
}

int32 UBaseGameplayAbility::GetCooldown() const
{
	return Cooldown;
}

int32 UBaseGameplayAbility::GetCost() const
{
	return Cost;
}

EAffinity UBaseGameplayAbility::GetAffinity() const
{
	return Affinity;
}

EAbilityDescriptor UBaseGameplayAbility::GetAbilityDescriptor() const
{
	return AbilityDescriptor;
}

FActorAttributes UBaseGameplayAbility::GetActorAttributes()
{
	return ActorAttributes;
}

void UBaseGameplayAbility::SpawnActor()
{
	//Determing Location/Rotation of the character
	FVector Vector = GetSpawnTransformLocation(CharacterRef);
	FRotator Rotator = GetSpawnTransformRotation(CharacterRef);

	// Spawn the Actor
	if (AbilityActor != nullptr) {
		FActorSpawnParameters SpawnParams = FActorSpawnParameters();
		SpawnParams.Owner = GetOwningActorFromActorInfo();
		SpawnParams.Instigator = Cast<AUBaseCharacter>(CurrentActorInfo->AvatarActor);
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		SpawnedAbilityActor = GetWorld()->SpawnActor<ABaseAbilityActor>(AbilityActor, Vector, Rotator, SpawnParams);
	}
}

UGameplayEffect * UBaseGameplayAbility::GetCostGameplayEffect() const
{
	if (bHasCostEffect)
	{
		return CostEffect;
	}
	else
	{
		return nullptr;
	}
}

UGameplayEffect * UBaseGameplayAbility::GetCooldownGameplayEffect() const
{
	return CooldownEffect;
}

UGameplayEffect * UBaseGameplayAbility::GetDamageGameplayEffect() const
{
	return DamageEffect;
}

bool UBaseGameplayAbility::ShouldCancelIfInputReleased()
{
	return bShouldCancelIfInputReleased;
}

const FGameplayTagContainer * UBaseGameplayAbility::GetCooldownTags() const
{
	FGameplayTagContainer* CustomCooldownTags = const_cast<FGameplayTagContainer*>(&CooldownTagContainer);
	const FGameplayTagContainer* ParentCooldownTags = Super::GetCooldownTags();
	if (ParentCooldownTags) {
		CustomCooldownTags->AppendTags(*ParentCooldownTags);
	}
	return CustomCooldownTags;
}

void UBaseGameplayAbility::ApplyCooldown(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo * ActorInfo, 
	const FGameplayAbilityActivationInfo ActivationInfo) const
{
	// Add our custom cooldown tag to the effect and apply it
	if (CooldownEffect) {
		FGameplayEffectSpecHandle SpecHandle = MakeOutgoingGameplayEffectSpec(CooldownEffect->GetClass(), GetAbilityLevel());
		SpecHandle.Data.Get()->DynamicGrantedTags.AppendTags(CooldownTagContainer);
		ApplyGameplayEffectSpecToOwner(Handle, ActorInfo, ActivationInfo, SpecHandle);
	}
}

FVector UBaseGameplayAbility::GetSpawnTransformLocation(const AUBaseCharacter* Character)
{
	UE_LOG(LogAscension, Error, TEXT("Something has gone wrong, calling GetSpawnTransformLocation in super."));
	return FVector();
}

FRotator UBaseGameplayAbility::GetSpawnTransformRotation(const AUBaseCharacter* Character)
{
	UE_LOG(LogAscension, Error, TEXT("Something has gone wrong, calling GetSpawnTransformRotation in super."));
	return FRotator();
}

void UBaseGameplayAbility::PreActivate(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo * ActorInfo, 
	const FGameplayAbilityActivationInfo ActivationInfo, FOnGameplayAbilityEnded::FDelegate * OnGameplayAbilityEndedDelegate)
{
	Super::PreActivate(Handle, ActorInfo, ActivationInfo, OnGameplayAbilityEndedDelegate);
}

void UBaseGameplayAbility::ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
	const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData)
{
	CharacterRef = CharacterRef ? CharacterRef : Cast<AUBaseCharacter>(ActorInfo->AvatarActor);
	if (CharacterRef != nullptr)
	{
		UCharacterAbilitySystemComponent* AbilitySystemComponent = CharacterRef->AbilitySystem;
		UAnimMontage* Montage = AnimMontage.Get();
		if (AbilitySystemComponent != nullptr && Montage != nullptr && CommitAbility(Handle, ActorInfo, ActivationInfo))
		{
			// Set the active ability
			AbilitySystemComponent->CurrentActiveAbility = this;

			// TODO: move these events somewhere else? only happen once?
			// Setup the ability actor hit event
			UAbilityTask_WaitGameplayEvent* WaitForHit = UAbilityTask_WaitGameplayEvent::WaitGameplayEvent(this,
				FGameplayTag::RequestGameplayTag(TEXT("GameplayEvent.AbilityActorHit")), CharacterRef, false, true);
			WaitForHit->EventReceived.AddDynamic(this, &UBaseGameplayAbility::OnActorHit);
			WaitForHit->ReadyForActivation();

			// Setup the ability actor overlap event
			UAbilityTask_WaitGameplayEvent* WaitForOverlap = UAbilityTask_WaitGameplayEvent::WaitGameplayEvent(this,
				FGameplayTag::RequestGameplayTag(TEXT("GameplayEvent.AbilityActorOverlap")), CharacterRef, false, true);
			WaitForOverlap->EventReceived.AddDynamic(this, &UBaseGameplayAbility::OnActorOverlap);
			WaitForOverlap->ReadyForActivation();

			// Setup the ability actor destroyed event
			UAbilityTask_WaitGameplayEvent* WaitForDestroy = UAbilityTask_WaitGameplayEvent::WaitGameplayEvent(this,
				FGameplayTag::RequestGameplayTag(TEXT("GameplayEvent.AbilityActorDestroyed")), CharacterRef, false, true);
			WaitForDestroy->EventReceived.AddDynamic(this, &UBaseGameplayAbility::OnActorDestroyed);
			WaitForDestroy->ReadyForActivation();

			// Play the configured montage
			AbilitySystemComponent->PlayMontage(this, ActivationInfo, Montage, MontagePlayRate, AnimMontageStartSection);
			
			//Wait for the montage to play and spawn the actor that represents the ability
			GetWorld()->GetTimerManager().SetTimer(AnimTimerHandle, this, &UBaseGameplayAbility::SpawnActor, SpawnActorDelay, false);
		}
		else {
			UE_LOG(LogAscension, Error, TEXT("Error committing ability, missing components."));
			EndAbility(Handle, ActorInfo, ActivationInfo, true, false);
		}
	} 
	else {
		UE_LOG(LogAscension, Error, TEXT("Error casting to UBaseCharacter while trying to activate ability."));
		EndAbility(Handle, ActorInfo, ActivationInfo, true, false);
	}
}

void UBaseGameplayAbility::EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, 
	const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled)
{
	Super::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateEndAbility, bWasCancelled);
}

void UBaseGameplayAbility::OnActorHit(FGameplayEventData GameplayEventData)
{
	Macros::PrintDebugMessage("Apply Ability Effects!!!");
}

void UBaseGameplayAbility::OnActorOverlap(FGameplayEventData GameplayEventData)
{
	Macros::PrintDebugMessage("Actor Overlap!!!");
}

void UBaseGameplayAbility::OnActorDestroyed(FGameplayEventData GameplayEventData)
{
	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, false);
}

void UBaseGameplayAbility::OnAvatarSet(const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilitySpec& Spec)
{
	// Initiate passives or do other "BeginPlay" type of logic here.
	Super::OnAvatarSet(ActorInfo, Spec);

	// Get the AbilitySystem from the Character and initialize the attributes
	CharacterRef = CharacterRef ? CharacterRef : Cast<AUBaseCharacter>(ActorInfo->AvatarActor);
	if (CharacterRef)
	{
		UCharacterAbilitySystemComponent* AbilitySystemComponent = CharacterRef->AbilitySystem;
		if (AbilitySystemComponent)
		{
			int32 Id = Spec.InputID;
			// TODO; move the cooldown tag base name into a dataTable
			FString TagName = FString("GameplayTag.AbilityCooldown").Append(FString::FromInt(Id));

			// Add the cooldown tag for this ability to the tag container
			CooldownTagContainer.AddTag(FGameplayTag::RequestGameplayTag(FName(*TagName)));

			// Retrieve the attributes for this ability from the AbilitySystemComponent
			UAbilityAttributes* NewAttributes = AbilitySystemComponent->GetAttributesForAbility(Id);
			if (!bWasInitialized && NewAttributes)
			{
				Initialize(NewAttributes);
			}
		}
	}
}