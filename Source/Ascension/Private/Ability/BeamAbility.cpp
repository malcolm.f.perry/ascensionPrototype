#include "Ability/BeamAbility.h"
#include "DataTable/BeamAbilityDataTable.h"

UBeamAbility::UBeamAbility(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	bHasCostEffect = false;
	bShouldCancelIfInputReleased = true;

	EtherCostEffect = CreateDefaultSubobject<UPeriodicEtherCostEffect>(TEXT("PeriodicEtherCostEffect"));

	// TODO: Can this be initialized from a dataTable?
	// Add the channeled ability descriptor tag
	AbilityTags.AddTag(FGameplayTag::RequestGameplayTag(FName(*FString("GameplayTag.ChanneledAbility"))));
}

FVector UBeamAbility::GetSpawnTransformLocation(const AUBaseCharacter* Character)
{
	// Spawn the actor at the point between the left and right hands
	const USkeletalMeshComponent* Mesh = Character->GetMesh();
	FVector LeftHand = Mesh->GetSocketLocation("hand_l");
	FVector RightHand = Mesh->GetSocketLocation("hand_r");
	return LeftHand.operator+(RightHand).operator/(2);
}

FRotator UBeamAbility::GetSpawnTransformRotation(const AUBaseCharacter* Character)
{
	return Character->GetActorRotation();
}

void UBeamAbility::ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, 
	const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData)
{
	Super::ActivateAbility(Handle, ActorInfo, ActivationInfo, TriggerEventData);

	if (EtherCostEffect)
	{
		EtherCostEffectHandle = ApplyGameplayEffectToOwner(Handle, ActorInfo, ActivationInfo, EtherCostEffect, GetAbilityLevel());
	}
	else
	{
		EndAbility(Handle, ActorInfo, ActivationInfo, true, false);
	}
}

void UBeamAbility::OnActorHit(FGameplayEventData GameplayEventData)
{
	Super::OnActorHit(GameplayEventData);
}

void UBeamAbility::OnActorOverlap(FGameplayEventData GameplayEventData)
{
	Super::OnActorOverlap(GameplayEventData);
}

void UBeamAbility::OnActorDestroyed(FGameplayEventData GameplayEventData)
{
	Super::OnActorDestroyed(GameplayEventData);
}

void UBeamAbility::EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
	const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateCancelAbility, bool bWasCancelled)
{
	Super::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateCancelAbility, bWasCancelled);

	// Remove the projectile actor
	if (SpawnedAbilityActor)
	{
		SpawnedAbilityActor->Destroy();
	}

	UAbilitySystemComponent* AbilitySystem = GetAbilitySystemComponentFromActorInfo();
	if (AbilitySystem)
	{
		// End the cost effect
		AbilitySystem->RemoveActiveGameplayEffect(EtherCostEffectHandle);

		// End the montage
		UAnimMontage* Montage = AnimMontage.Get();
		if (Montage)
		{
			AbilitySystem->StopMontageIfCurrent(*Montage, 0.0f);
		}
	}
}

void UBeamAbility::OnAvatarSet(const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilitySpec& Spec)
{
	Super::OnAvatarSet(ActorInfo, Spec);

	// Initialize properties from the data table
	FString RowName = GET_STRING_FROM_ENUM("EAffinity", GetAffinity());
	FBeamAbilityTableRow* Data = GetRowData<FBeamAbilityTableRow>(GetWorld(), EDataTableName::BeamAbility, RowName);
	if (Data)
	{
		AnimMontage = Data->Montage;
		AnimMontageStartSection = Data->MontageStartSection;
		MontagePlayRate = Data->MontagePlayRate;
		AbilityActor = Data->AbilityActor;
		SpawnActorDelay = Data->SpawnActorDelay;

		// Make sure the AnimMontage is loaded
		if (!AnimMontage.Get())
		{
			LoadAsynchronous(AnimMontage.ToSoftObjectPath());
		}
	}
}