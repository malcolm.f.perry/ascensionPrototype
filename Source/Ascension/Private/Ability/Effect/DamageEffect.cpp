#include "Ability/Effect/DamageEffect.h"
#include "Stats/BaseStatSet.h"
#include "Ability/Effect/Calculation/DamageEffectCalculation.h"

UDamageEffect::UDamageEffect(const FObjectInitializer& ObjectInitializer)
{
	ChanceToApplyToTarget = 1.0f;
	DurationPolicy = EGameplayEffectDurationType::Instant;

	AddCalculationFloatModifierInfo(UBaseStatSet::GetHealthAttribute(), UDamageEffectCalculation::StaticClass());
}