#include "Ability/Effect/BaseGameplayEffect.h"

void UBaseGameplayEffect::AddModifierInfo(FGameplayAttribute Attribute, 
	FGameplayEffectModifierMagnitude Magnitude, EGameplayModOp::Type Operation = EGameplayModOp::Type())
{
	FGameplayModifierInfo ModifierInfo;
	ModifierInfo.Attribute = Attribute;
	ModifierInfo.ModifierMagnitude = Magnitude;
	ModifierInfo.ModifierOp = Operation;
	Modifiers.Add(ModifierInfo);
}

void UBaseGameplayEffect::AddAdditiveModifierInfo(FGameplayAttribute Attribute, FGameplayEffectModifierMagnitude Magnitude)
{
	AddModifierInfo(Attribute, Magnitude, EGameplayModOp::Additive);
}

void UBaseGameplayEffect::AddMultiplicativeModifierInfo(FGameplayAttribute Attribute, FGameplayEffectModifierMagnitude Magnitude)
{
	AddModifierInfo(Attribute, Magnitude, EGameplayModOp::Multiplicitive);
}

void UBaseGameplayEffect::AddCalculationFloatModifierInfo(FGameplayAttribute Attribute,
	TSubclassOf<UGameplayModMagnitudeCalculation> CalculationClass)
{
	// Reference the class used to obtain the Magnitude value
	FCustomCalculationBasedFloat CalculationFloat;
	CalculationFloat.CalculationClassMagnitude = CalculationClass;

	AddModifierInfo(Attribute, FGameplayEffectModifierMagnitude(CalculationFloat));
}