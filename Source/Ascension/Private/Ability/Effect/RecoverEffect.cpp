#include "Ability/Effect/RecoverEffect.h"
#include "Stats/BaseStatSet.h"

URecoverEffect::URecoverEffect(const FObjectInitializer& ObjectInitializer)
{
	ChanceToApplyToTarget = 1.0f;
	DurationPolicy = EGameplayEffectDurationType::Infinite;

	// Regen Modifier Info
	AddMultiplicativeModifierInfo(UBaseStatSet::GetHealthRegenRateAttribute(), FGameplayEffectModifierMagnitude(20));
	AddMultiplicativeModifierInfo(UBaseStatSet::GetEnergyRegenRateAttribute(), FGameplayEffectModifierMagnitude(20));
	AddMultiplicativeModifierInfo(UBaseStatSet::GetEtherRegenRateAttribute(), FGameplayEffectModifierMagnitude(20));
}