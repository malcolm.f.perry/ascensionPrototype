#include "Ability/Effect/EtherCostEffect.h"
#include "Stats/BaseStatSet.h"
#include "Ability/Effect/Calculation/CostEffectCalculation.h"

UEtherCostEffect::UEtherCostEffect(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	ChanceToApplyToTarget = 1.0f;
	DurationPolicy = EGameplayEffectDurationType::Instant;

	AddCalculationFloatModifierInfo(UBaseStatSet::GetEtherAttribute(), UCostEffectCalculation::StaticClass());
}