#include "Ability/Effect/Calculation/EtherRegenCalculation.h"
#include "Stats/BaseStatSet.h"

UEtherRegenCalculation::UEtherRegenCalculation(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	EtherRegenRateDef.AttributeToCapture = UBaseStatSet::GetEtherRegenRateAttribute();
	EtherRegenRateDef.AttributeSource = EGameplayEffectAttributeCaptureSource::Target;
	EtherRegenRateDef.bSnapshot = false;

	RelevantAttributesToCapture.Add(EtherRegenRateDef);
}

float UEtherRegenCalculation::CalculateBaseMagnitude_Implementation(const FGameplayEffectSpec & Spec) const
{
	// Target tags to allow for buffs/debuffs
	const FGameplayTagContainer* TargetTags = Spec.CapturedTargetTags.GetAggregatedTags();

	FAggregatorEvaluateParameters Parameters;
	Parameters.TargetTags = TargetTags;

	float RegenRate = 1.0f;
	GetCapturedAttributeMagnitude(EtherRegenRateDef, Spec, Parameters, RegenRate);

	return RegenRate;
}