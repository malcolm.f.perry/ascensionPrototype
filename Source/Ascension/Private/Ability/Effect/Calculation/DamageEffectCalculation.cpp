#include "Ability/Effect/Calculation/DamageEffectCalculation.h"
#include "Ability/BaseGameplayAbility.h"

UDamageEffectCalculation::UDamageEffectCalculation(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{

}

float UDamageEffectCalculation::CalculateBaseMagnitude_Implementation(const FGameplayEffectSpec & Spec) const
{
	const UBaseGameplayAbility* Ability = Cast<UBaseGameplayAbility>(Spec.GetContext().GetAbilityInstance_NotReplicated());

	if (Ability) {
		// TODO: this needs to use the power -> damage function that we may create in a curvetable with
		// possibly some random modifiers for thresholds of damage. why dont we also log damage over the
		// players head so we can see a running tally of +/- health(other stats).
		return -1.0 * abs(Ability->GetPower() * 10);
	}
	else {
		return 0.0f;
	}
}