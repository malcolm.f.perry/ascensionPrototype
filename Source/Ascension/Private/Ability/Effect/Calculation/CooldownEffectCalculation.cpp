#include "Ability/Effect/Calculation/CooldownEffectCalculation.h"
#include "Ability/BaseGameplayAbility.h"

UCooldownEffectCalculation::UCooldownEffectCalculation(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{

}

float UCooldownEffectCalculation::CalculateBaseMagnitude_Implementation(const FGameplayEffectSpec & Spec) const
{
	const UBaseGameplayAbility* Ability = Cast<UBaseGameplayAbility>(Spec.GetContext().GetAbilityInstance_NotReplicated());

	if (Ability) {
		return Ability->GetCooldown();
	}
	else {
		return 0.0f;
	}
}