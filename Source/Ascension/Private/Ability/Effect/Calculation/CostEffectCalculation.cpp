#include "Ability/Effect/Calculation/CostEffectCalculation.h"
#include "Ability/BaseGameplayAbility.h"

UCostEffectCalculation::UCostEffectCalculation(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

float UCostEffectCalculation::CalculateBaseMagnitude_Implementation(const FGameplayEffectSpec & Spec) const
{
	const UBaseGameplayAbility* Ability = Cast<UBaseGameplayAbility>(Spec.GetContext().GetAbilityInstance_NotReplicated());

	if (Ability) {
		return -1.0 * abs(float(Ability->GetCost()));
	}
	else {
		return 0.0f;
	}
}