#include "Ability/Effect/Calculation/EnergyRegenCalculation.h"
#include "Stats/BaseStatSet.h"

UEnergyRegenCalculation::UEnergyRegenCalculation(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	EnergyRegenRateDef.AttributeToCapture = UBaseStatSet::GetEnergyRegenRateAttribute();
	EnergyRegenRateDef.AttributeSource = EGameplayEffectAttributeCaptureSource::Target;
	EnergyRegenRateDef.bSnapshot = false;

	RelevantAttributesToCapture.Add(EnergyRegenRateDef);
}

float UEnergyRegenCalculation::CalculateBaseMagnitude_Implementation(const FGameplayEffectSpec & Spec) const
{
	// Target tags to allow for buffs/debuffs
	const FGameplayTagContainer* TargetTags = Spec.CapturedTargetTags.GetAggregatedTags();

	FAggregatorEvaluateParameters Parameters;
	Parameters.TargetTags = TargetTags;

	float RegenRate = 1.0f;
	GetCapturedAttributeMagnitude(EnergyRegenRateDef, Spec, Parameters, RegenRate);

	return RegenRate;
}