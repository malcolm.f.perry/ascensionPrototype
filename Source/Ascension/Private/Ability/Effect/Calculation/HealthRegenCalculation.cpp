#include "Ability/Effect/Calculation/HealthRegenCalculation.h"
#include "Stats/BaseStatSet.h"

UHealthRegenCalculation::UHealthRegenCalculation(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	HealthRegenRateDef.AttributeToCapture = UBaseStatSet::GetHealthRegenRateAttribute();
	HealthRegenRateDef.AttributeSource = EGameplayEffectAttributeCaptureSource::Target;
	HealthRegenRateDef.bSnapshot = false;

	RelevantAttributesToCapture.Add(HealthRegenRateDef);
}

float UHealthRegenCalculation::CalculateBaseMagnitude_Implementation(const FGameplayEffectSpec & Spec) const
{
	// Target tags to allow for buffs/debuffs
	const FGameplayTagContainer* TargetTags = Spec.CapturedTargetTags.GetAggregatedTags();

	FAggregatorEvaluateParameters Parameters;
	Parameters.TargetTags = TargetTags;

	float RegenRate = 1.0f;
	GetCapturedAttributeMagnitude(HealthRegenRateDef, Spec, Parameters, RegenRate);

	return RegenRate;
}