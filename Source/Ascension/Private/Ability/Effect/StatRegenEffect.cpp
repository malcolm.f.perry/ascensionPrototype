#include "Ability/Effect/StatRegenEffect.h"
#include "Ability/Effect/Calculation/HealthRegenCalculation.h"
#include "Ability/Effect/Calculation/EnergyRegenCalculation.h"
#include "Ability/Effect/Calculation/EtherRegenCalculation.h"
#include "Stats/BaseStatSet.h"

UStatRegenEffect::UStatRegenEffect(const FObjectInitializer& ObjectInitializer)
{
	ChanceToApplyToTarget = 1.0f;
	DurationPolicy = EGameplayEffectDurationType::Infinite;
	
	Period = 1; // In seconds
	bExecutePeriodicEffectOnApplication = true;

	AddCalculationFloatModifierInfo(UBaseStatSet::GetHealthAttribute(), UHealthRegenCalculation::StaticClass());
	AddCalculationFloatModifierInfo(UBaseStatSet::GetEnergyAttribute(), UEnergyRegenCalculation::StaticClass());
	AddCalculationFloatModifierInfo(UBaseStatSet::GetEtherAttribute(), UEtherRegenCalculation::StaticClass());
}