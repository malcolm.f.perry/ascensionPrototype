#include "Ability/Effect/SprintMovementSpeedEffect.h"
#include "Stats/BaseStatSet.h"

USprintMovementSpeedEffect::USprintMovementSpeedEffect(const FObjectInitializer& ObjectInitializer)
{
	ChanceToApplyToTarget = 1.0f;
	DurationPolicy = EGameplayEffectDurationType::Infinite;

	AddMultiplicativeModifierInfo(UBaseStatSet::GetMovementSpeedAttribute(), FGameplayEffectModifierMagnitude(1.5));
}