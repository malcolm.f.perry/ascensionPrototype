#include "Ability/Effect/PeriodicEtherCostEffect.h"
#include "Stats/BaseStatSet.h"
#include "Ability/Effect/Calculation/CostEffectCalculation.h"

UPeriodicEtherCostEffect::UPeriodicEtherCostEffect(const FObjectInitializer& ObjectInitializer)
{
	ChanceToApplyToTarget = 1.0f;
	DurationPolicy = EGameplayEffectDurationType::Infinite;

	Period = 1; // In seconds
	bExecutePeriodicEffectOnApplication = true;

	// Energy cost modifier
	AddCalculationFloatModifierInfo(UBaseStatSet::GetEtherAttribute(), UCostEffectCalculation::StaticClass());
}