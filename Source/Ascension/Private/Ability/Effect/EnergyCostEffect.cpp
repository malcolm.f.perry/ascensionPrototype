#include "Ability/Effect/EnergyCostEffect.h"
#include "Stats/BaseStatSet.h"
#include "Ability/Effect/Calculation/CostEffectCalculation.h"

UEnergyCostEffect::UEnergyCostEffect(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	ChanceToApplyToTarget = 1.0f;
	DurationPolicy = EGameplayEffectDurationType::Instant;

	AddCalculationFloatModifierInfo(UBaseStatSet::GetEnergyAttribute(), UCostEffectCalculation::StaticClass());
}