#include "Ability/Effect/PeriodicEnergyCostEffect.h"
#include "Stats/BaseStatSet.h"
#include "Ability/Effect/Calculation/CostEffectCalculation.h"

UPeriodicEnergyCostEffect::UPeriodicEnergyCostEffect(const FObjectInitializer& ObjectInitializer)
{
	ChanceToApplyToTarget = 1.0f;
	DurationPolicy = EGameplayEffectDurationType::Infinite;

	Period = 1; // In seconds
	bExecutePeriodicEffectOnApplication = true;

	// Energy cost modifier
	AddCalculationFloatModifierInfo(UBaseStatSet::GetEnergyAttribute(), UCostEffectCalculation::StaticClass());
}