#include "Ability/Effect/CooldownEffect.h"
#include "Ability/Effect/Calculation/CooldownEffectCalculation.h"

UCooldownEffect::UCooldownEffect(const FObjectInitializer& ObjectInitializer)
{
	ChanceToApplyToTarget = 1.0f;
	DurationPolicy = EGameplayEffectDurationType::HasDuration;

	FCustomCalculationBasedFloat CalculationFloat;
	CalculationFloat.CalculationClassMagnitude = UCooldownEffectCalculation::StaticClass();
	DurationMagnitude = CalculationFloat;
}