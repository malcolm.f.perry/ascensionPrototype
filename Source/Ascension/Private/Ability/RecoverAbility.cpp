#include "Ability/RecoverAbility.h"
#include "DataTable/RecoverAbilityDataTable.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Abilities/Tasks/AbilityTask_WaitDelay.h"

URecoverAbility::URecoverAbility(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	bHasCostEffect = false;

	// Add the recover effect
	RecoverEffect = CreateDefaultSubobject<URecoverEffect>(TEXT("RecoverEffect"));

	// Initialize the cooldown attribute
	Attributes = CreateDefaultSubobject<UAbilityAttributes>(TEXT("AbilityAttributes"));

	// TODO: Can this be initialized from a dataTable?
	// Add the channeled ability descriptor tag
	AbilityTags.AddTag(FGameplayTag::RequestGameplayTag(FName(*FString("GameplayTag.ChanneledAbility"))));

	UE_LOG(LogAscension, Warning, TEXT("Num: %d"), ActivationBlockedTags.Num());
}

FVector URecoverAbility::GetSpawnTransformLocation(const AUBaseCharacter* Character)
{
	// Spawn the actor at the root location (center between mesh's feet)
	const USkeletalMeshComponent* Mesh = Character->GetMesh();
	return Mesh->GetSocketLocation("root");
}

FRotator URecoverAbility::GetSpawnTransformRotation(const AUBaseCharacter* Character)
{
	return Character->GetActorRotation();
}

void URecoverAbility::ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo * ActorInfo, 
	const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData * TriggerEventData)
{
	if (CommitAbility(Handle, ActorInfo, ActivationInfo) && RecoverEffect)
	{
		UAbilitySystemComponent* AbilitySystem = GetAbilitySystemComponentFromActorInfo();
		UAnimMontage* Montage = AnimMontage.Get();
		AUBaseCharacter* Character = Cast<AUBaseCharacter>(ActorInfo->AvatarActor);
		if (AbilitySystem && Montage && Character && RecoverParticles.Get())
		{
			// Start the montage
			AbilitySystem->PlayMontage(this, ActivationInfo, Montage, MontagePlayRate, AnimMontageStartSection);

			// Spawn and activate the particle system
			ParticleSystemComponent = UGameplayStatics::SpawnEmitterAtLocation(this, RecoverParticles.Get(), 
				GetSpawnTransformLocation(Character));
		}
		RecoverEffectHandle = ApplyGameplayEffectToOwner(Handle, ActorInfo, ActivationInfo, RecoverEffect, GetAbilityLevel());

		// Only allow abilty to last for the Duration
		UAbilityTask_WaitDelay* WaitForDuration = UAbilityTask_WaitDelay::WaitDelay(this, GetDuration());
		WaitForDuration->OnFinish.AddDynamic(this, &URecoverAbility::EndRecover);
		WaitForDuration->ReadyForActivation();
	}
	else
	{
		EndAbility(Handle, ActorInfo, ActivationInfo, true, false);
	}
}

void URecoverAbility::EndRecover()
{
	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, false);
}

void URecoverAbility::EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
	const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateCancelAbility, bool bWasCancelled)
{
	Super::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateCancelAbility, bWasCancelled);

	// Deactivate and remove the particle system
	if (ParticleSystemComponent)
	{
		ParticleSystemComponent->Deactivate();
		ParticleSystemComponent->MarkPendingKill();
	}

	UAbilitySystemComponent* AbilitySystem = GetAbilitySystemComponentFromActorInfo();
	if (AbilitySystem)
	{
		// End the montage
		UAnimMontage* Montage = AnimMontage.Get();
		if (Montage)
		{
			AbilitySystem->StopMontageIfCurrent(*Montage, 0.0f);
		}

		// End the effect
		AbilitySystem->RemoveActiveGameplayEffect(RecoverEffectHandle);
	}
}

void URecoverAbility::OnAvatarSet(const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilitySpec& Spec)
{
	Super::OnAvatarSet(ActorInfo, Spec);

	// Initialize properties from the data table
	FRecoverAbilityTableRow* Data = GetRowData<FRecoverAbilityTableRow>(GetWorld(), EDataTableName::RecoverAbility,
		URecoverAbilityDataTable::GetRowName());
	if (Data)
	{
		// TODO: Investigate how to use this in place of the constructor call
		// Set this ability as a channeled ability
		//AbilityTags.AppendTags(Data->AbilityTags);

		// Call parent to initialize the attributes
		if (Attributes)
		{
			Attributes->SetCooldown(Data->Cooldown);
			Attributes->SetCost(Data->Cost);
			Attributes->SetDuration(Data->Duration);
			Attributes->SetAbilityDescriptor(EAbilityDescriptor::Recovery);
			Initialize(Attributes);
		}

		RecoverParticles = Data->ActorParticles;
		AnimMontage = Data->Montage;
		AnimMontageStartSection = Data->MontageStartSection;
		MontagePlayRate = Data->MontagePlayRate;
		AbilityActor = Data->AbilityActor;

		// Make sure the AnimMontage is loaded
		TArray<FSoftObjectPath> Paths;
		if (!AnimMontage.Get())
		{
			Paths.Add(AnimMontage.ToSoftObjectPath());
		}

		// Ensure the particle system is loaded
		if (!RecoverParticles.Get())
		{
			Paths.Add(RecoverParticles.ToSoftObjectPath());
		}

		// If we need to load assets, load them asynchronously then finish execution
		if (Paths.Num() > 0)
		{
			LoadAsynchronous(Paths);
		}
	}
}