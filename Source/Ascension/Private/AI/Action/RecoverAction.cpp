#include "AI/Action/RecoverAction.h"
#include "Character/AICharacter.h"
#include "Ability/CharacterAbilitySystemComponent.h"

URecoverAction::URecoverAction()
{
	Index = 5;
}

void URecoverAction::doAction_Implementation(APawn* Pawn)
{
	Super::doAction_Implementation(Pawn);
}