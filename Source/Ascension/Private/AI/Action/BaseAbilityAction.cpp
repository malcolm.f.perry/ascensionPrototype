#include "AI/Action/BaseAbilityAction.h"
#include "Character/AICharacter.h"
#include "Controller/AscensionAIController.h"
#include "Kismet/KismetMathLibrary.h"

UBaseAbilityAction::UBaseAbilityAction()
{
}

void UBaseAbilityAction::doAction_Implementation(APawn* Pawn)
{
	AUAICharacter* Self = Cast<AUAICharacter>(Pawn);
	AbilitySystem = Self ? Self->AbilitySystem : nullptr;
	Controller = Cast<AAscensionAIController>(Self->GetController());

	if (Controller)
	{
		if (AbilitySystem && AbilitySystem->HasAbilityAtIndex(Index))
		{
			// Add the end ability listener
			if (!AbilitySystem->OnAbilityEnded.IsBoundToObject(this))
			{
				AbilitySystem->OnAbilityEnded.AddUObject(this, &UBaseAbilityAction::AbilityEnded);
			}

			// Rotate to face target
			AUBaseCharacter* Target = Controller->GetCurrentTarget();
			if (Target && Self)
			{
				FRotator Rotator = UKismetMathLibrary::FindLookAtRotation(Self->GetActorLocation(), Target->GetActorLocation());
				Self->SetActorRotation(Rotator);
			}

			// Activate the ability
			if (!AbilitySystem->ActivateAbilityAtIndex(Index))
			{
				// If activation was unsuccessful, complete this action as a failure
				Controller->OnActionCompletedEvent.Broadcast(this, false);
			}
		}
	}
	else
	{
		UE_LOG(LogAscension, Error, TEXT("Missing AI controller reference for action: %s"), *this->GetFName().ToString());
	}
}

void UBaseAbilityAction::AbilityEnded(const FAbilityEndedData& AbilityEndedData)
{
	if (Controller && AbilitySystem && AbilitySystem->IsAbilityAtIndex(Index, AbilityEndedData.AbilitySpecHandle))
	{
		// Complete this action
		Controller->OnActionCompletedEvent.Broadcast(this, !AbilityEndedData.bWasCancelled);
	}
}