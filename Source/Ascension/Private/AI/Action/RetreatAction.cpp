#include "AI/Action/RetreatAction.h"
#include "Character/BaseCharacter.h"

URetreatAction::URetreatAction()
{
}

void URetreatAction::doAction_Implementation(APawn* Pawn)
{
	Controller = Cast<AAscensionAIController>(Pawn->GetController());
	Self = Cast<AUAICharacter>(Pawn);
	if (Controller)
	{
		Controller->ReceiveMoveCompleted.AddUniqueDynamic(this, &URetreatAction::MoveCompleted);

		// TODO: Find a point outside of max spell distance or that is "hidden" from Target and move there?
		FVector FleeFromPoint;
		AUBaseCharacter* Target = Controller->GetCurrentTarget();
		if (Target)
		{
			FleeFromPoint = Target->GetActorLocation();
		}
		else
		{
			// Use last stimulus location
			FleeFromPoint = Controller->GetLastKnownStimulusLocation();
		}

		// TODO: replace 1000 with getMaxSpellRange -> from DT
		FVector TargetLocation;
		if (Controller->GetRandomPointInRadius(FleeFromPoint, 1000.0, TargetLocation))
		{
			Controller->MoveToLocation(TargetLocation, 50.0);
		}
		else
		{
			UE_LOG(LogAscension, Error, TEXT("Could not find path to retreat location: %s"), *FleeFromPoint.ToString());
			Controller->OnActionCompletedEvent.Broadcast(this, false);
		}
	}
}

void URetreatAction::MoveCompleted(FAIRequestID RequestID, EPathFollowingResult::Type Result)
{
	// Turn to see if you were followed
	if (Self)
	{
		FRotator CurrentRotation = Self->GetActorRotation();
		Self->SetActorRotation(FRotator(CurrentRotation.Pitch, -CurrentRotation.Yaw, CurrentRotation.Roll));
	}

	Super::MoveCompleted(RequestID, Result);
}