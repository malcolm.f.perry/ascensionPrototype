#include "AI/Action/IdleAction.h"
#include "Character/AICharacter.h"
#include "Controller/AscensionAIController.h"

UIdleAction::UIdleAction()
{
}

void UIdleAction::doAction_Implementation(APawn* Pawn)
{
	AUAICharacter* Self = Cast<AUAICharacter>(Pawn);
	Controller = Cast<AAscensionAIController>(Self->GetController());

	if (Controller)
	{
		// Wait for the specified idle delay period
		UWorld* World = Controller->GetWorld();
		if (World)
		{
			World->GetTimerManager().SetTimer(IdleDelayTimerHandle, this, &UIdleAction::IdleDelayCallback, IdleDelay, false);
		}
		else
		{
			UE_LOG(LogAscension, Error, TEXT("Failed to find world reference in the IdleAction."));
			Controller->OnActionCompletedEvent.Broadcast(this, false);
		}
	}
	else
	{
		UE_LOG(LogAscension, Error, TEXT("Missing AI controller reference for action: %s"), *this->GetFName().ToString());
	}
}

void UIdleAction::IdleDelayCallback()
{
	if (Controller)
	{
		// Complete this action
		Controller->OnActionCompletedEvent.Broadcast(this, true);
	}
}