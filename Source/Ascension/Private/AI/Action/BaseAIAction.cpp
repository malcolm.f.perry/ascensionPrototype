#include "AI/Action/BaseAIAction.h"

UBaseAIAction::UBaseAIAction()
{
}

void UBaseAIAction::doAction_Implementation(APawn* Pawn)
{
	UE_LOG(LogAscension, Warning, TEXT("No ExecuteAction implemented in BaseAIAction."));
	Super::doAction_Implementation(Pawn);
}