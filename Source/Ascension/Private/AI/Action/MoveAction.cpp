#include "AI/Action/MoveAction.h"
#include "Character/BaseCharacter.h"
#include "Kismet/KismetMathLibrary.h"
#include "AI/Goal/PatrolGoal.h"

UMoveAction::UMoveAction()
{
}

void UMoveAction::doAction_Implementation(APawn* Pawn)
{
	Controller = Cast<AAscensionAIController>(Pawn->GetController());
	if (Controller)
	{
		Controller->ReceiveMoveCompleted.AddUniqueDynamic(this, &UMoveAction::MoveCompleted);

		AUAICharacter* Self = Cast<AUAICharacter>(Pawn);
		AUBaseCharacter* Target = Controller->GetCurrentTarget();
		bool bDoMove = true;
		
		if (Self)
		{
			FVector TargetLocation;
			float AcceptanceBuffer = 400.0;

			UBaseAIGoal* CurrentGoal = Controller->GetCurrentGoal();
			if (CurrentGoal && CurrentGoal->IsA(UPatrolGoal::StaticClass()))
			{
				AcceptanceBuffer = 50;
				float Radius = 50.0; // Radius of area to get random point from
				float DistanceToMove = 500.0;

				if (!TryMovingForward(Self->GetActorLocation(), DistanceToMove, Radius, TargetLocation))
				{
					// The forward location is unreachable, try diagonals
					if (!TryMovingDiagonally(Self->GetActorLocation(), DistanceToMove, Radius, TargetLocation))
					{
						// The diagonals are unreachable, try the sides
						if (!TryMovingLeftRight(Self->GetActorLocation(), DistanceToMove, Radius, TargetLocation))
						{
							// The sides are unreachable, try rear diagonals
							if (!TryMovingRearDiagonally(Self->GetActorLocation(), DistanceToMove, Radius, TargetLocation))
							{
								// The rear diagonals are unreachable, try going straight back
								if (!TryMovingBackward(Self->GetActorLocation(), DistanceToMove, Radius, TargetLocation))
								{
									// All movement directions unreachable, log error and complete action
									UE_LOG(LogAscension, Error, TEXT("Could not find a point to move to in a radius of: %f"), DistanceToMove);
									bDoMove = false;
									Controller->OnActionCompletedEvent.Broadcast(this, false);
								}
							}
						}
					}
				}
			}
			else if (Target)
			{
				// Move in range of the target
				TargetLocation = Target->GetActorLocation();
				AcceptanceBuffer = 400.0;

				// Rotate to face target location
				FRotator Rotator = UKismetMathLibrary::FindLookAtRotation(Self->GetActorLocation(), TargetLocation);
				Self->SetActorRotation(Rotator);
			}
			else
			{
				// Move to the last place we saw the target
				TargetLocation = Controller->GetLastKnownStimulusLocation();
			}

			// Send the movement request
			if (bDoMove)
			{
				// TODO: acceptance radius should match cast distance minus some threshold (cast speed * movement speed + buffer)?
				//Controller->MoveToActor(Target, 100.0f);
				Controller->MoveToLocation(TargetLocation, AcceptanceBuffer);
			}
		}
		else
		{
			UE_LOG(LogAscension, Error, TEXT("Self NULL"));
			Controller->OnActionCompletedEvent.Broadcast(this, false);
		}
	}
	else
	{
		UE_LOG(LogAscension, Error, TEXT("Missing AI controller reference for action: %s"), *this->GetFName().ToString());
	}
}

bool UMoveAction::TryMovingForward(const FVector& ActorLocation, float DistanceToMove, float Radius, FVector& TargetLocation)
{
	FVector ForwardLocation = ActorLocation.operator+(FVector(DistanceToMove, 0.0, 0.0));
	return Controller->GetRandomPointInRadius(ForwardLocation, Radius, TargetLocation);
}

bool UMoveAction::TryMovingBackward(const FVector& ActorLocation, float DistanceToMove, float Radius, FVector& TargetLocation)
{
	FVector ForwardLocation = ActorLocation.operator-(FVector(DistanceToMove, 0.0, 0.0));
	return Controller->GetRandomPointInRadius(ForwardLocation, Radius, TargetLocation);
}

bool UMoveAction::TryMovingDiagonally(const FVector& ActorLocation, float DistanceToMove, float Radius, FVector& TargetLocation)
{
	TArray<FVector> Locations;
	Locations.Add(ActorLocation.operator+(FVector(DistanceToMove, DistanceToMove, 0.0)));
	Locations.Add(ActorLocation.operator+(FVector(DistanceToMove, -DistanceToMove, 0.0)));

	return SelectFirstValidLocation(Locations, Radius, TargetLocation);
}

bool UMoveAction::TryMovingLeftRight(const FVector& ActorLocation, float DistanceToMove, float Radius, FVector& TargetLocation)
{
	TArray<FVector> Locations;
	Locations.Add(ActorLocation.operator+(FVector(0.0, DistanceToMove, 0.0)));
	Locations.Add(ActorLocation.operator+(FVector(0.0, -DistanceToMove, 0.0)));

	return SelectFirstValidLocation(Locations, Radius, TargetLocation);
}

bool UMoveAction::TryMovingRearDiagonally(const FVector& ActorLocation, float DistanceToMove, float Radius, FVector& TargetLocation)
{
	TArray<FVector> Locations;
	Locations.Add(ActorLocation.operator+(FVector(-DistanceToMove, DistanceToMove, 0.0)));
	Locations.Add(ActorLocation.operator+(FVector(-DistanceToMove, -DistanceToMove, 0.0)));

	return SelectFirstValidLocation(Locations, Radius, TargetLocation);
}

bool UMoveAction::SelectFirstValidLocation(TArray<FVector> Locations, float Radius, FVector& TargetLocation)
{
	for (int32 Index = 0; Index < Locations.Num(); Index++)
	{
		if (Controller->GetRandomPointInRadius(Locations[Index], Radius, TargetLocation))
		{
			return true;
		}
	}
	return false;
}

void UMoveAction::MoveCompleted(FAIRequestID RequestID, EPathFollowingResult::Type Result)
{
	// Complete this action
	if (Controller)
	{
		if (Result == EPathFollowingResult::Type::Success)
		{
			Controller->OnActionCompletedEvent.Broadcast(this, true);
		}
	}
}