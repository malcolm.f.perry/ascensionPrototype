#include "AI/Action/DodgeAction.h"
#include "Stats/BaseStatSet.h"
#include "Kismet/KismetMathLibrary.h"

UDodgeAction::UDodgeAction()
{
	Index = 7;
}

void UDodgeAction::doAction_Implementation(APawn* Pawn)
{
	Controller = Cast<AAscensionAIController>(Pawn->GetController());
	if (Controller)
	{
		Self = Cast<AUAICharacter>(Pawn);
		AbilitySystem = Self ? Self->AbilitySystem : nullptr;

		// Add the end action listeners
		//Controller->ReceiveMoveCompleted.AddUniqueDynamic(this, &UMoveAction::MoveCompleted);
		if (!AbilitySystem->OnAbilityEnded.IsBoundToObject(this))
		{
			AbilitySystem->OnAbilityEnded.AddUObject(this, &UDodgeAction::AbilityEnded);
		}

		if (Self)
		{
			// TODO: Get this value (energy required) from the roll datatable
			bool bHasEnergy = Self->BaseStatSet ? Self->BaseStatSet->GetEnergy() > 10 : false;
			FVector CurrentLocation = Self->GetActorLocation();
			OriginalActorRotation = Self->GetActorRotation();
			
			// Determine which direction to face
			float MoveDistance = 100.0;
			FVector LeftLocation = CurrentLocation.operator+(FVector(0.0, MoveDistance, 0.0));
			FVector RightLocation = CurrentLocation.operator+(FVector(0.0, -MoveDistance, 0.0));
			if (Controller->CanReachLocation(CurrentLocation, LeftLocation))
			{

				UE_LOG(LogAscension, Warning, TEXT("Roll Left"));

				FRotator Rotator = UKismetMathLibrary::FindLookAtRotation(CurrentLocation, LeftLocation);
				Self->SetActorRotation(Rotator);
				DoDodge(bHasEnergy);
			}
			else if (Controller->CanReachLocation(CurrentLocation, RightLocation))
			{

				UE_LOG(LogAscension, Warning, TEXT("Roll Right"));

				FRotator Rotator = UKismetMathLibrary::FindLookAtRotation(CurrentLocation, RightLocation);
				Self->SetActorRotation(Rotator);
				DoDodge(bHasEnergy);
			}
			else
			{

				UE_LOG(LogAscension, Warning, TEXT("Cant go anywhere...."));

				// TODO: Run to random location far away??? but for now, just get hit and end the ability
				Controller->OnActionCompletedEvent.Broadcast(this, true);
			}
		}
	}
}

void UDodgeAction::AbilityEnded(const FAbilityEndedData& AbilityEndedData)
{
	// Turn to old location
	if (Self)
	{
		Self->SetActorRotation(OriginalActorRotation);
	}

	Super::AbilityEnded(AbilityEndedData);
}

void UDodgeAction::DoDodge(bool HasEnergy)
{
	// Do the actual dodging
	if (HasEnergy)
	{
		// Execute a roll action
		if (!AbilitySystem->ActivateAbilityAtIndex(Index))
		{
			UE_LOG(LogAscension, Error, TEXT("Failed to activate the roll ability."));

			// If activation was unsuccessful, complete this action as a failure
			Controller->OnActionCompletedEvent.Broadcast(this, false);
		}
	}
	else
	{
		// Move normally
		// TODO: for now just end
		Controller->OnActionCompletedEvent.Broadcast(this, true);
	}
}
