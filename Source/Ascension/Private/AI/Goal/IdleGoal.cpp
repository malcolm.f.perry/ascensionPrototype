#include "AI/Goal/IdleGoal.h"

UIdleGoal::UIdleGoal(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

float UIdleGoal::CalculateUtility(TMap<EAIWorldStateAttribute, bool> CurrentWorldState)
{
	// TODO: Determine a real utility function
	return 0.5;
}