#include "AI/Goal/DodgeGoal.h"

UDodgeGoal::UDodgeGoal(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

float UDodgeGoal::CalculateUtility(TMap<EAIWorldStateAttribute, bool> CurrentWorldState)
{
	// TODO: Determine a real utility function
	bool* NeedToDodge = CurrentWorldState.Find(EAIWorldStateAttribute::NeedToDodge);
	bool* IsAlive = CurrentWorldState.Find(EAIWorldStateAttribute::IsAlive);

	return ((IsAlive && *IsAlive) && (NeedToDodge && *NeedToDodge)) ? 2.0 : 0.0;
}