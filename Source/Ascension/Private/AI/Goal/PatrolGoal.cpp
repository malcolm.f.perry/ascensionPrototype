#include "AI/Goal/PatrolGoal.h"

UPatrolGoal::UPatrolGoal(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

float UPatrolGoal::CalculateUtility(TMap<EAIWorldStateAttribute, bool> CurrentWorldState)
{
	// TODO: Determine a real utility function
	bool* HasTarget = CurrentWorldState.Find(EAIWorldStateAttribute::HasTarget);
	bool* IsAlive = CurrentWorldState.Find(EAIWorldStateAttribute::IsAlive);
	return ((IsAlive && *IsAlive) && (HasTarget && *HasTarget)) ? 0.0 : 1.0;
}