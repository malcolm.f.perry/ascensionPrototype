#include "AI/Goal/RecoverGoal.h"

URecoverGoal::URecoverGoal(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

float URecoverGoal::CalculateUtility(TMap<EAIWorldStateAttribute, bool> CurrentWorldState)
{
	// TODO: Determine a real utility function
	// TODO: Add a worldState for "hidden"(out of LOS) or factor into the "enemies in range"
	// Recover if im low and (far or hidden from the enemy)
	bool* RecoverAvailable = CurrentWorldState.Find(EAIWorldStateAttribute::RecoverAvailable);
	bool* NeedHealing = CurrentWorldState.Find(EAIWorldStateAttribute::NeedHealing);
	bool* EnemiesInRange = CurrentWorldState.Find(EAIWorldStateAttribute::EnemiesInRange);
	bool* IsAlive = CurrentWorldState.Find(EAIWorldStateAttribute::IsAlive);

	return ((IsAlive && *IsAlive) && (RecoverAvailable && *RecoverAvailable) && (NeedHealing && *NeedHealing) && !(EnemiesInRange && *EnemiesInRange)) ? 1.5 : 0.0;
}