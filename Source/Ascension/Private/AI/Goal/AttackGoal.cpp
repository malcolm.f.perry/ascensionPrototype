#include "AI/Goal/AttackGoal.h"

UAttackGoal::UAttackGoal(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

float UAttackGoal::CalculateUtility(TMap<EAIWorldStateAttribute, bool> CurrentWorldState)
{
	// TODO: Determine a real utility function
	bool* HasAttackAvailable = CurrentWorldState.Find(EAIWorldStateAttribute::HasAttackAvailable);
	bool* HasTarget = CurrentWorldState.Find(EAIWorldStateAttribute::HasTarget);
	bool* IsAlive = CurrentWorldState.Find(EAIWorldStateAttribute::IsAlive);

	return ((IsAlive && *IsAlive) && (HasTarget && *HasTarget) && (HasAttackAvailable && *HasAttackAvailable)) ? 1.0 : 0.0;
}