#include "AI/Goal/BaseAIGoal.h"

UBaseAIGoal::UBaseAIGoal(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

float UBaseAIGoal::CalculateUtility(TMap<EAIWorldStateAttribute, bool> CurrentWorldState)
{
	return 0.0;
}

TArray<FAtom> UBaseAIGoal::GetDesiredWorldState()
{
	return DesiredWorldState;
}