#include "AI/Goal/RetreatGoal.h"

URetreatGoal::URetreatGoal(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

float URetreatGoal::CalculateUtility(TMap<EAIWorldStateAttribute, bool> CurrentWorldState)
{
	// TODO: Determine a real utility function
	// TODO: if im low and more than 3 enemies(outnumbered) -> run
	// Retreat if I need healing and the target enemy has more health
	bool* NeedHealing = CurrentWorldState.Find(EAIWorldStateAttribute::NeedHealing);
	bool* TargetEnemyIsHealthier = CurrentWorldState.Find(EAIWorldStateAttribute::TargetEnemyIsHealthier);
	bool* IsAlive = CurrentWorldState.Find(EAIWorldStateAttribute::IsAlive);

	return ((IsAlive && *IsAlive) && (NeedHealing && *NeedHealing) && (TargetEnemyIsHealthier && *TargetEnemyIsHealthier)) ? 1.25 : 0.0;
}