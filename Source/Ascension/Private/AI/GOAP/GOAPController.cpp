/**
	GOAP NPC: Goal-Oriented Action Planning for Non-Player Characters.

	Publishers/Authors:
		-	Diego Romero-Hombrebueno Santos.
		-	Mario Sanchez Blanco.
		-	Jose Manuel Sierra Ramos.

	Published on 2020.
 */
#include "AI/GOAP/GOAPController.h"

AGOAPController::AGOAPController() {}

void AGOAPController::BeginPlay()
{
	// Bind completion events
	OnActionCompletedEvent.AddUObject(this, &AGOAPController::OnActionCompleted);
	OnPlanCompletedEvent.AddUObject(this, &AGOAPController::OnPlanCompleted);

	// Loads actions.
	for (auto i = 0; i < actions.Num(); ++i)
	{
		if (actions[i] != NULL) {
			FString aux = actions[i].GetDefaultObject()->GetName();
			auxActions.Push(actions[i].GetDefaultObject());
		}
	}

	// Loads Current World.
	for (FAtom atom : currentWorld)
		wsCurrentWorld.addAtom(atom.name, atom.value);

	// Loads Desired World.
	for (FAtom atom : desiredWorld)
		wsDesiredWorld.addAtom(atom.name, atom.value);

	// Loads actions' preconditions and effects.
	for (UGOAPAction* a : auxActions)
		a->create_P_E();

	// Loads Planner.
	planner = new GOAPPlanner(&wsCurrentWorld, &wsDesiredWorld, auxActions);
	planner->setMaxDepth(maxDepth);

	// Error messages.
	if (auxActions.Num() == 0)
		UE_LOG(LogTemp, Warning, TEXT("Actions not found in GOAPController."));

	if (wsCurrentWorld.isEmpty())
		UE_LOG(LogTemp, Warning, TEXT("Undefined Current World in GOAPController."));

	if (wsDesiredWorld.isEmpty())
		UE_LOG(LogTemp, Warning, TEXT("Undefined Desired World in GOAPController."));

	Super::BeginPlay();
}

void AGOAPController::OnPossess(APawn* pawn)
{
	Super::OnPossess(pawn);
}

void AGOAPController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
}

bool AGOAPController::executeGOAP()
{
	if (generatePlan())
	{
		if (plan.Num() > 0)
		{
			// Get the next action to perform.
			UGOAPAction* aux = plan[plan.Num() - 1];

			// Performs an action and when it's done its effects are applied, changing the current world state.
			// When complete, actions fire the OnActionCompletedEvent to advance the plan
			aux->doAction(GetPawn());

			return true;
		}
	}

	return false;
}

bool AGOAPController::generatePlan()
{
	if (auxActions.Num() > 0 && !wsCurrentWorld.isEmpty() && !wsDesiredWorld.isEmpty())
	{
		// Creates the cheapest plan of actions.
		plan = planner->generatePlan(GetPawn());

		return true;
	}

	return false;
}

bool AGOAPController::IsPlanComplete()
{
	return (plan.Num() == 0);
}

TArray<UGOAPAction*> AGOAPController::getPlan()
{
	return plan;
}

void AGOAPController::setGoal(const TArray<FAtom>& newGoal)
{
	wsDesiredWorld.cleanAtoms();
	updateGoal(newGoal);
}

void AGOAPController::updateGoal(const TArray<FAtom>& atoms)
{
	for (FAtom atom : atoms)
		wsDesiredWorld.addAtom(atom.name, atom.value);
}

void AGOAPController::setCurrentWorld(const TArray<FAtom>& newCurrentWorld)
{
	wsCurrentWorld.cleanAtoms();
	updateCurrentWorld(newCurrentWorld);
}

void AGOAPController::updateCurrentWorld(const TArray<FAtom>& atoms)
{
	for (FAtom atom : atoms)
		wsCurrentWorld.addAtom(atom.name, atom.value);
}

TArray<FAtom> AGOAPController::getCurrentWorldStateAtoms()
{
	TArray<FAtom> worldStateAtoms;
	for (auto atoms : wsCurrentWorld.getAtoms())
	{
		worldStateAtoms.Add({ atoms.first, atoms.second });
	}

	return worldStateAtoms;
}

TArray<FAtom> AGOAPController::getDesiredWorldStateAtoms()
{
	TArray<FAtom> worldStateAtoms;
	for (auto atoms : wsDesiredWorld.getAtoms())
	{
		worldStateAtoms.Add({ atoms.first, atoms.second });
	}

	return worldStateAtoms;
}

void AGOAPController::OnActionCompleted(UGOAPAction* Action, bool bSuccessful)
{
	// Update state and call next action if this action was successful
	if (bSuccessful && plan.Num() > 0)
	{
		// Update world state
		wsCurrentWorld.joinWorldState(Action->getEffects());

		// Remove the completed action
		int32 CurrentActionIndex = plan.Num() - 1;
		plan.RemoveAt(CurrentActionIndex);

		// Call the next action, if available
		if (plan.Num() > 0)
		{
			plan[CurrentActionIndex - 1]->doAction(GetPawn());
		}
		else
		{
			// The last action was called, end the current plan
			OnPlanCompletedEvent.Broadcast();
		}
	}
	else
	{
		// The action wasn't successful, so clear out the plan and start over
		plan.Empty();
		OnPlanCompletedEvent.Broadcast();
	}
}

void AGOAPController::OnPlanCompleted()
{
}