#include "AI/GOAP/GOAPAction.h"
#include "AI/GOAP/GOAPController.h"

UGOAPAction::UGOAPAction() {}

TArray<AActor*> UGOAPAction::getTargetsList(APawn* p)
{
	TArray<AActor*> actorsFound;
	UGameplayStatics::GetAllActorsOfClass(p->GetWorld(), targetsType, actorsFound);
	return actorsFound;
}

bool UGOAPAction::checkProceduralPrecondition_Implementation(APawn* p)
{
	return true;
}

void UGOAPAction::doAction_Implementation(APawn* Pawn)
{	
	// Implement the action and once done, let the controller know to move to the next action
	CompleteAction(Pawn, true);
}

void UGOAPAction::CompleteAction_Implementation(APawn* p, bool bSuccessResult)
{
	// Let the pawn's controller know that this action is complete, and the next can begin
	AGOAPController* Controller = Cast<AGOAPController>(p->GetController());
	if (Controller)
	{
		Controller->OnActionCompletedEvent.Broadcast(this, bSuccessResult);
	}
}

void UGOAPAction::create_P_E()
{
	for (FAtom itP : preconditions)
	{
		wsPreconditions.addAtom(itP.name, itP.value);
	}
	for (FAtom itE : effects)
	{
		wsEffects.addAtom(itE.name, itE.value);
	}
	if (targetsType == NULL)
		UE_LOG(LogTemp, Warning, TEXT("Targets' type of '%s' action are not defined."), *name);
}

bool UGOAPAction::operator==(UGOAPAction& a)
{
	return this->cost == a.getCost() && target == a.getTarget() && wsPreconditions == a.getPreconditions() && wsEffects == a.getEffects();
}

bool UGOAPAction::operator!=(UGOAPAction& a)
{
	return !(*this == a);
}

// GETS

FString UGOAPAction::getName()
{
	return this->name;
}

float UGOAPAction::getCost()
{
	return this->cost;
}

AActor* UGOAPAction::getTarget()
{
	return target;
}

GOAPWorldState UGOAPAction::getPreconditions()
{
	return wsPreconditions;
}

GOAPWorldState UGOAPAction::getEffects()
{
	return wsEffects;
}

// SETS

void UGOAPAction::setName(FString n)
{
	this->name = n;
}

void UGOAPAction::setCost(float c)
{
	cost = c;
}

void UGOAPAction::setTarget(AActor* t)
{
	target = t;
}

void UGOAPAction::setPreconditions(GOAPWorldState pre)
{
	wsPreconditions = pre;
}

void UGOAPAction::setEffects(GOAPWorldState ef)
{
	wsEffects = ef;
}
