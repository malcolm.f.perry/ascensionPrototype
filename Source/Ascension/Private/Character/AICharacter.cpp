#include "Character/AICharacter.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Stats/BaseStatSet.h"
#include "Ability/AbilityAttributes.h"
#include "Ability/CharacterAbilitySystemComponent.h"
#include "Ability/BlastAbility.h"
#include "Ability/BeamAbility.h"

AUAICharacter::AUAICharacter()
{
	// Set the AI Nav propeties
	UCharacterMovementComponent* MovementComponent = GetCharacterMovement();
	FNavAgentProperties NavProperties = MovementComponent->NavAgentProps;

	// Create the perception component
	PerceptionComponent = CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("PerceptionComponent"));
}

FVector AUAICharacter::GetMovementVector()
{
	return GetVelocity();
}

void AUAICharacter::GetActorEyesViewPoint(FVector& OutLocation, FRotator& OutRotation) const
{
	// Set from the actor location
	FVector AILocation = GetActorLocation();
	OutLocation.X = AILocation.X;
	OutLocation.Y = AILocation.Y;
	OutLocation.Z = AILocation.Z;

	// Set from the actor rotation
	FRotator AIRotator = GetActorRotation();
	OutRotation.Pitch = AIRotator.Pitch;
	OutRotation.Yaw = AIRotator.Yaw;
	OutRotation.Roll = AIRotator.Roll;
}

void AUAICharacter::AddAbility(int32 Index, TSubclassOf<UBaseGameplayAbility> AbilityType, int32 Power, int32 Penetration, int32 Range, int32 Speed, 
	int32 Size, int32 CastTime, int32 Duration, int32 Delay, int32 Cooldown, int32 Cost, EAffinity Affinity)
{
	// Initialize this abilities attributes
	UAbilityAttributes* AbilityAttributes = NewObject<UAbilityAttributes>(this);
	AbilityAttributes->SetAbilityType(AbilityType);
	AbilityAttributes->SetPower(Power);
	AbilityAttributes->SetPenetration(Penetration);
	AbilityAttributes->SetRange(Range);
	AbilityAttributes->SetSpeed(Speed);
	AbilityAttributes->SetSize(Size);
	AbilityAttributes->SetCastTime(CastTime);
	AbilityAttributes->SetDuration(Duration);
	AbilityAttributes->SetDelay(Delay);
	AbilityAttributes->SetCooldown(Cooldown);
	AbilityAttributes->SetCost(Cost);
	AbilityAttributes->SetAffinity(Affinity);

	// Add the ability to the ability system
	if (!AbilitySystem->AddAbilityToIndex(Index, AbilityAttributes))
	{
		// Failed to add the ability
		UE_LOG(LogAscension, Error, TEXT("Add AI ability at index: %d failed"), Index);
	}
}

void AUAICharacter::BeginPlay()
{
	Super::BeginPlay();

	if (HasAuthority() && AbilitySystem && BaseStatSet)
	{
		// Initialize the ability system 
		AbilitySystem->InitAbilityActorInfo(this, this);

		// Add the Character's Affinities
		BaseStatSet->SetAffinities(AffinitiesMap);
		
		// TODO: Figure out how to use the beam ability, which is channeled, so the ai doesnt use all its energy on that?
			// OnMissWithBeamEvent -> Cancel the beam ability
		// TODO: Expose abilities' configuration on the AI Character blueprint -> configure all 5 abilities separately
		// Add the AI's abilities
		AddAbility(0, UBlastAbility::StaticClass(), 1, 1, 100, 1500, 60, 5, 0, 0, 5, 2, EAffinity::Fire);
		AddAbility(1, UBlastAbility::StaticClass(), 1, 1, 100, 1500, 60, 5, 0, 0, 5, 2, EAffinity::Water);
		AddAbility(2, UBlastAbility::StaticClass(), 1, 1, 100, 1500, 60, 5, 0, 0, 5, 2, EAffinity::Earth);
		AddAbility(3, UBlastAbility::StaticClass(), 1, 1, 100, 1500, 60, 5, 0, 0, 5, 2, EAffinity::Air);
		//AddAbility(4, UBlastAbility::StaticClass(), 1, 1, 100, 1500, 60, 5, 0, 0, 1, 5, EAffinity::Fire);
	}
}