#include "Character/PlayerCharacter.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "Ability/CharacterAbilitySystemComponent.h"
#include "World/AscensionGameInstance.h"
#include "Abilities/GameplayAbility.h"
#include "Controller/AscensionPlayerController.h"
#include "DataTable/GameConstantsDataTable.h"
#include "Stats/BaseStatSet.h"

AUPlayerCharacter::AUPlayerCharacter()
{
	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Locks camera behind character, allows controller(mouse) to control direction
	UCharacterMovementComponent* MovementComponent = GetCharacterMovement();
	MovementComponent->bOrientRotationToMovement = false;
	MovementComponent->bUseControllerDesiredRotation = true;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	
	// Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm
}

//////////////////////////////////////////////////////////////////////////
// Input
void AUPlayerCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);

	// Basic movement
	PlayerInputComponent->BindAxis("MoveForwardBackward", this, &AUPlayerCharacter::MoveForwardBackward);
	PlayerInputComponent->BindAxis("MoveLeftRight", this, &AUPlayerCharacter::MoveLeftRight);

	// Jumping
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AUPlayerCharacter::PlayerJump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &AUPlayerCharacter::PlayerStopJumping);

	// Rolling
	PlayerInputComponent->BindAction("Roll", IE_Pressed, this, &AUPlayerCharacter::Roll);

	// Sprinting
	PlayerInputComponent->BindAction("Sprint", IE_Pressed, this, &AUPlayerCharacter::StartSprint);
	PlayerInputComponent->BindAction("Sprint", IE_Released, this, &AUPlayerCharacter::StopSprint);

	// Recover ability
	PlayerInputComponent->BindAction("Recover", IE_Pressed, this, &AUBaseCharacter::StartRecover);
	PlayerInputComponent->BindAction("Recover", IE_Released, this, &AUBaseCharacter::StopRecover);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AUPlayerCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AUPlayerCharacter::LookUpAtRate);

	// Initialize AbilitySystem Inputs from the AbilityInputEnum
	AbilitySystem->BindAbilityActivationToInputComponent(PlayerInputComponent, 
		FGameplayAbilityInputBinds("Confirm Input", "Cancel Input", "AbilityInputEnum"));
}

void AUPlayerCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AUPlayerCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AUPlayerCharacter::PlayerJump()
{
	if (AbilitySystem)
	{
		// Handle any pre-action updates
		PreActionExecution();

		// Activate the roll ability
		AbilitySystem->ActivateJumpAbility();
	}
}

void AUPlayerCharacter::PlayerStopJumping()
{
	if (AbilitySystem)
	{
		// Activate the roll ability
		AbilitySystem->EndJumpAbility();
	}
}

void AUPlayerCharacter::Roll()
{
	// Cannot roll while jumping or falling
	FVector VelocityVector = GetVelocity();
	if (AbilitySystem && VelocityVector.Z == 0)
	{
		// Handle any pre-action updates
		PreActionExecution();

		// Activate the roll ability
		AbilitySystem->ActivateRollAbility();
	}
}

void AUPlayerCharacter::StartSprint()
{
	// Handle any pre-action updates
	PreActionExecution();

	// Moving will now trigger sprinting
	bShouldSprint = true;
}

void AUPlayerCharacter::StopSprint()
{
	UCharacterMovementComponent* MovementComponent = GetCharacterMovement();
	if (bShouldSprint && bIsSprinting && AbilitySystem && MovementComponent)
	{
		bShouldSprint = false;
		bIsSprinting = false;

		// Cancel the Sprint Ability
		if (AbilitySystem)
		{
			// Start recovery
			AbilitySystem->EndSprintAbility();
		}

		// Update the MaxWalkSpeed
		MovementComponent->MaxWalkSpeed = BaseStatSet->GetMovementSpeed();
	}
}

void AUPlayerCharacter::MoveForwardBackward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// Handle any pre-action updates
		if (!bShouldSprint && !bIsSprinting)
		{
			PreActionExecution();
		}

		// If we are moving forward and sprinting, add the sprinting effects
		UCharacterMovementComponent* MovementComponent = GetCharacterMovement();
		if (BaseStatSet->GetEnergy() > 0)
		{
			if (Value > 0 && bShouldSprint && !bIsSprinting && MovementComponent)
			{
				bIsSprinting = true;

				// Add the Sprint Effect
				if (AbilitySystem)
				{
					AbilitySystem->ActivateSprintAbility();
				}

				// Update the MaxWalkSpeed
				MovementComponent->MaxWalkSpeed = BaseStatSet->GetMovementSpeed();
			}
		} 
		else
		{
			// End sprinting if energy is low
			StopSprint();
		}

		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void AUPlayerCharacter::MoveLeftRight(float Value)
{
	// If moving forward/backward, then left/right movement will control the turn rate
	if (InputComponent->GetAxisValue(TEXT("MoveForwardBackward")) != 0)
	{
		TurnAtRate(Value);
	}
	else if ((Controller != NULL) && (Value != 0.0f))
	{
		// Handle any pre-action updates
		PreActionExecution();

		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

FVector AUPlayerCharacter::GetMovementVector()
{
	if (InputComponent)
	{
		return FVector(InputComponent->GetAxisValue(TEXT("MoveForwardBackward")),
			InputComponent->GetAxisValue(TEXT("MoveLeftRight")), 0);
	}

	return FVector(ForceInit);
}

void AUPlayerCharacter::TogglePlayerInput(bool Enable)
{
	AUAscensionPlayerController* PlayerController = Cast<AUAscensionPlayerController>(GetController());
	if (PlayerController)
	{
		if (Enable)
		{
			EnableInput(PlayerController);
		}
		else
		{
			DisableInput(PlayerController);
		}
	}
}

void AUPlayerCharacter::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);

	AbilitySystem->RefreshAbilityActorInfo();
}

void AUPlayerCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	UAscensionGameInstance* GameInstance = Cast<UAscensionGameInstance>(GetGameInstance());
	if (HasAuthority() && AbilitySystem && GameInstance && BaseStatSet)
	{
		// Initialize the ability system 
		AbilitySystem->InitAbilityActorInfo(this, this);

		// Add the Character's Affinities
		BaseStatSet->SetAffinities(GameInstance->GetAffinities());

		// Add the Character's Abilities
		if (GameInstance->GetAbilityAttributesArray().Num() > 0)
		{
			int32 Index = 0;
			for (UAbilityAttributes* AbilityAttributes : GameInstance->GetAbilityAttributesArray())
			{
				if (AbilityAttributes) {
					AbilitySystem->AddAbilityToIndex(Index, AbilityAttributes);
				}
				Index++;
			}
		}
	}

	// Initialize from GameConstants
	FGameConstantsTableRow* Data = GetRowData<FGameConstantsTableRow>(GetWorld(), EDataTableName::GameConstants,
		UGameConstantsDataTable::GetRowName());
	if (Data)
	{
		//Offset the camera from the character
		CameraBoom->SocketOffset = Data->CameraOffset;
	}
}

void AUPlayerCharacter::DisablePlayerInput()
{
	TogglePlayerInput(false);
}

void AUPlayerCharacter::EnablePlayerInput()
{
	TogglePlayerInput(true);
}