#include "Character/CharacterInfo.h"
#include "DataTable/GameConstantsDataTable.h"

UCharacterInfo::UCharacterInfo(const FObjectInitializer& ObjectInitializer)
{
	// TODO: Figure out how to move the below into a global event system that can be called after datatable initialization
	// Get the max number of abilities from the constants table
	//FGameConstantsTableRow* Row = GetRowData<FGameConstantsTableRow>(GetWorld(), EDataTableName::GameConstants, 
	//	UGameConstantsDataTable::GetRowName());
	//int32 MaxNumAbilities = Row ? Row->MaxNumAbilities : 0;

	// Initialize ability slots
	for (int32 i = 0; i < 5; i++)
	{
		AttributesList.Add(nullptr);
	}
}

bool UCharacterInfo::AddAffinity(EAffinity Affinity, int32 Value)
{
	// Affinities are percentages(0 to 100)
	if (Value > 0 && Value <= 100)
	{
		Affinities.Add(Affinity, Value);
		return true;
	}
	return false;
}

int32 UCharacterInfo::RemoveAffinity(EAffinity Affinity)
{
	return Affinities.Remove(Affinity);
}

TMap<EAffinity, int32> UCharacterInfo::GetAffinities() const
{
	return Affinities;
}

bool UCharacterInfo::AddAttributes(int32 Index, UAbilityAttributes * Attributes)
{
	if (AttributesList.IsValidIndex(Index))
	{
		AttributesList[Index] = Attributes;
		return true;
	}
	return false;
}

bool UCharacterInfo::RemoveAttributes(int32 Index)
{
	if (AttributesList.IsValidIndex(Index))
	{
		AttributesList.RemoveAt(Index, 1, true);
		return true;
	}
	return false;
}

TArray<UAbilityAttributes*> UCharacterInfo::GetAttributesList() const
{
	return AttributesList;
}