#include "Character/BaseCharacter.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "Ability/CharacterAbilitySystemComponent.h"
#include "Stats/BaseStatSet.h"
#include "Abilities/GameplayAbilityTypes.h"
#include "DataTable/GameConstantsDataTable.h"
#include "Perception/AISense_Sight.h"

AUBaseCharacter::AUBaseCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Configure character movement
	UCharacterMovementComponent* MovementComponent = GetCharacterMovement();
	MovementComponent->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	MovementComponent->JumpZVelocity = 600.f;
	MovementComponent->AirControl = 0.2f;

	// Initialize Stats for the character
	BaseStatSet = CreateDefaultSubobject<UBaseStatSet>(TEXT("BaseStatSet"));
	UpdateStatPercentages();
	
	// Initialize the Ability System
	AbilitySystem = CreateDefaultSubobject<UCharacterAbilitySystemComponent>(TEXT("AbilitySystem"));

	// Create the AI Perception Stimuli Component
	PerceptionStimuliComponent = CreateDefaultSubobject<UAIPerceptionStimuliSourceComponent>(TEXT("PerceptionStimuli"));
}

UBaseStatSet* AUBaseCharacter::GetBaseStatSet()
{
	return BaseStatSet;
}

UAbilitySystemComponent* AUBaseCharacter::GetAbilitySystemComponent() const
{
	return Cast<UAbilitySystemComponent>(AbilitySystem);
}

void AUBaseCharacter::SetAbilitySystemComponent(UCharacterAbilitySystemComponent* NewAbilitySystem)
{
	AbilitySystem = NewAbilitySystem;
}

void AUBaseCharacter::UpdateStatPercentages()
{
	CurrentHealthPercentage = BaseStatSet->GetCurrentHealthPercentage();
	CurrentEnergyPercentage = BaseStatSet->GetCurrentEnergyPercentage();
	CurrentEtherPercentage = BaseStatSet->GetCurrentEtherPercentage();
}

bool AUBaseCharacter::IsAlive() const
{
	return BaseStatSet->GetHealth() > 0.0f;
}

FVector AUBaseCharacter::GetMovementVector()
{
	return FVector(ForceInit);
}

void AUBaseCharacter::PreActionExecution()
{
	if (AbilitySystem)
	{
		// Abilities with this tag will be interupted
		AbilitySystem->CancelAbilities(&InterruptTagContainer);
	}
}

void AUBaseCharacter::StartRecover()
{
	// Execute the Recover ability
	if (AbilitySystem)
	{
		// Start recovery
		AbilitySystem->ActivateRecoverAbility();
	}
}

void AUBaseCharacter::StopRecover()
{
	// Cancel recovery
	if (AbilitySystem)
	{
		// End recovery
		AbilitySystem->EndRecoverAbility();
	}
}

void AUBaseCharacter::OnCharacterHit()
{
	// Play the hit montage
	if (HitMontage.Get())
	{
		PlayAnimMontage(HitMontage.Get(), HitMontagePlayRate, HitMontageStartSection);
	}
}

void AUBaseCharacter::OnCharacterDeath()
{
	// Disable any movement
	GetCharacterMovement()->Deactivate();

	// After a 10 seconds, destroy the actor
	GetWorld()->GetTimerManager().SetTimer(DeathTimerHandle, this, &AUBaseCharacter::FinishDeath, 10, false);
}

void AUBaseCharacter::HealthChanged(const FOnAttributeChangeData & Data)
{
	UpdateStatPercentages();
	if (!IsAlive())
	{
		OnCharacterDeath();
	}
}

void AUBaseCharacter::EnergyChanged(const FOnAttributeChangeData & Data)
{
	UpdateStatPercentages();
}

void AUBaseCharacter::EtherChanged(const FOnAttributeChangeData & Data)
{
	UpdateStatPercentages();
}

void AUBaseCharacter::FinishDeath()
{
	Destroy();
}

void AUBaseCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (AbilitySystem)
	{
		if (BaseStatSet->GetEnergy() <= 0.0f)
		{
			AbilitySystem->CancelEnergyAbilities();
		}

		if (BaseStatSet->GetEther() <= 0.0f)
		{
			AbilitySystem->CancelEtherAbilities();
		}
	}
}

void AUBaseCharacter::BeginPlay()
{
	Super::BeginPlay();

	if (AbilitySystem)
	{
		// Add handlers for displaying stat changes
		HealthChangedHandle = AbilitySystem->GetGameplayAttributeValueChangeDelegate(BaseStatSet->GetHealthAttribute()).AddUObject(this, &AUBaseCharacter::HealthChanged);
		EnergyChangedHandle = AbilitySystem->GetGameplayAttributeValueChangeDelegate(BaseStatSet->GetEnergyAttribute()).AddUObject(this, &AUBaseCharacter::EnergyChanged);
		EtherChangedHandle = AbilitySystem->GetGameplayAttributeValueChangeDelegate(BaseStatSet->GetEtherAttribute()).AddUObject(this, &AUBaseCharacter::EtherChanged);

		// Add the permanent regen effect
		StatRegenEffect = NewObject<UStatRegenEffect>(this);
		AbilitySystem->ApplyGameplayEffectToSelf(StatRegenEffect, 1.0f, AbilitySystem->MakeEffectContext());
	}

	// Setup the montages from the constants data table
	FGameConstantsTableRow* Data = GetRowData<FGameConstantsTableRow>(GetWorld(), EDataTableName::GameConstants, 
		UGameConstantsDataTable::GetRowName());
	if (Data)
	{
		HitMontage = Data->OnHitMontage;
		HitMontageStartSection = Data->HitMontageStartSection;
		HitMontagePlayRate = Data->HitMontagePlayRate;
		InterruptTagContainer = Data->InterruptTagContainer;
		
		// If we need to load assets, load them asynchronously
		if (!HitMontage.Get())
		{
			LoadAsynchronous(HitMontage.ToSoftObjectPath());
		}
	}
}