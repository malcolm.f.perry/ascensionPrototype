#include "Stats/BaseStatSet.h"
#include "GameplayEffectExtension.h"
#include "Macros/Macros.h"

UBaseStatSet::UBaseStatSet()
{
	// Initialize Combat Stats
	InitMight(10.f);
	InitFinesse(10.f);

	// Initialize Energy Stats
	InitHealth(100.f);
	InitEnergy(100.f);
	InitEther(100.f);

	// Initialize Max Stats
	InitMaxHealth(100.f);
	InitMaxEnergy(100.f);
	InitMaxEther(100.f);

	// Initialize Combat Stats Regeneration Rate
	InitMightRegenRate(2.f);
	InitFinesseRegenRate(2.f);

	// Initialize Energy Stats Regeneration Rate
	InitHealthRegenRate(0.25f);
	InitEnergyRegenRate(0.25f);
	InitEtherRegenRate(0.25f);

	// Initialize Resistances
	InitEvasion(1.f);
	InitBludgeoningResistance(1.f);
	InitPiercingResistance(1.f);
	InitSlashingResistance(1.f);
	InitFireResistance(1.f);

	// Initialize General Stats
	InitMovementSpeed(600.f);
}

int32 UBaseStatSet::GetAffinityValue(EAffinity Affinity) const
{
	auto Value = Affinities.Find(Affinity);
	return Value ? *Value : -1;
}

bool UBaseStatSet::SetAffinities(TMap<EAffinity, int32> NewAffinities)
{
	// TODO: is this the best way to assure we initialize only once
	if (Affinities.Num() == 0)
	{
		Affinities = NewAffinities;
		return true;
	}
	return false;
}

float UBaseStatSet::GetCurrentHealthPercentage()
{
	return GetHealth() / GetMaxHealth();
}

float UBaseStatSet::GetCurrentEnergyPercentage()
{
	return GetEnergy() / GetMaxEnergy();
}

float UBaseStatSet::GetCurrentEtherPercentage()
{
	return GetEther() / GetMaxEther();
}

void UBaseStatSet::PreAttributeChange(const FGameplayAttribute & Attribute, float & NewValue)
{
	Super::PreAttributeChange(Attribute, NewValue);
}

void UBaseStatSet::PostGameplayEffectExecute(const FGameplayEffectModCallbackData & Data)
{
	Super::PostGameplayEffectExecute(Data);

	// Clamp Stats to keep them between 0 and Max
	if (Data.EvaluatedData.Attribute == GetHealthAttribute())
	{
		SetHealth(FMath::Clamp(GetHealth(), 0.0f, GetMaxHealth()));

		// TODO: investigate a better way of detecting an 'attack' and doing OnHit
		// Magnitude is negative so health went down, do the OnHit
		if (Data.EvaluatedData.Magnitude < 0)
		{
			AUBaseCharacter* TargetCharacter = GetTargetCharacter(Data);
			AUBaseCharacter* SourceCharacter = GetSourceCharacter(Data);
			if (SourceCharacter && TargetCharacter && TargetCharacter->IsAlive())
			{
				TargetCharacter->OnCharacterHit();
			}
		}
	}
	else if (Data.EvaluatedData.Attribute == GetEnergyAttribute())
	{
		SetEnergy(FMath::Clamp(GetEnergy(), 0.0f, GetMaxEnergy()));
	}
	else if (Data.EvaluatedData.Attribute == GetEtherAttribute())
	{
		SetEther(FMath::Clamp(GetEther(), 0.0f, GetMaxEther()));
	}
}

AUBaseCharacter * UBaseStatSet::GetSourceCharacter(const FGameplayEffectModCallbackData & Data)
{
	AUBaseCharacter* SourceCharacter = nullptr;
	FGameplayEffectContextHandle Context = Data.EffectSpec.GetContext();
	UAbilitySystemComponent* Source = Context.GetOriginalInstigatorAbilitySystemComponent();
	if (Source->AbilityActorInfo.IsValid() && Source->AbilityActorInfo->AvatarActor.IsValid())
	{
		SourceCharacter = Cast<AUBaseCharacter>(Source->AbilityActorInfo->AvatarActor);
	}
	return SourceCharacter;
}

AUBaseCharacter * UBaseStatSet::GetTargetCharacter(const FGameplayEffectModCallbackData & Data)
{
	AUBaseCharacter* TargetCharacter = nullptr;
	if (Data.Target.AbilityActorInfo.IsValid() && Data.Target.AbilityActorInfo->AvatarActor.IsValid())
	{
		TargetCharacter = Cast<AUBaseCharacter>(Data.Target.AbilityActorInfo->AvatarActor);
	}
	return TargetCharacter;
}

const FHitResult * UBaseStatSet::GetHitResult(const FGameplayEffectModCallbackData & Data)
{
	return Data.EffectSpec.GetContext().GetHitResult();
}