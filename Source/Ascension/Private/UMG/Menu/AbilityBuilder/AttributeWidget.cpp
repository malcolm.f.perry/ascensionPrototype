#include "UMG/Menu/AbilityBuilder/AttributeWidget.h"

UAttributeWidget::UAttributeWidget(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

void UAttributeWidget::UpdateCost()
{
	float Magnitude = bCostIsInverted ? (float)MaxValue - (float)Value : Value;
	Cost = ((Magnitude / (float)MaxValue) * 50.0) + 1.0;
}

void UAttributeWidget::UpdateMinValue(FGameConstantsTableRow Data)
{
	TMap<EAbilityAttributeName, int32> MinValueMap = Data.AttributeMinValueMap;
	MinValue = MinValueMap.FindChecked(EnumName);
	OnMinValueChanged();
}

void UAttributeWidget::UpdateMaxValue(FGameConstantsTableRow Data)
{
	TMap<EAbilityAttributeName, int32> MaxValueMap = Data.AttributeMaxValueMap;
	MaxValue = MaxValueMap.FindChecked(EnumName);
	OnMaxValueChanged();
}

void UAttributeWidget::Init(EAbilityAttributeName NewName)
{
	EnumName = NewName;
	Name = GET_STRING_FROM_ENUM("EAbilityAttributeName", NewName);
	bCostIsInverted = GetGameConstantsData().AttributeInvertedCostMap.FindChecked(EnumName);
	Reset();
}

int32 UAttributeWidget::GetCost()
{
	return Cost;
}

void UAttributeWidget::Update(int32 NewValue)
{
	if (NewValue >= MinValue && NewValue <= MaxValue)
	{
		SetValue(NewValue);
		OnAttributeWidgetValueChanged.Broadcast(EnumName, Value);
	}
}

void UAttributeWidget::SetValue(int32 NewValue)
{
	Value = NewValue;
	OnValueReset();
	UpdateCost();
}

void UAttributeWidget::Reset()
{
	FGameConstantsTableRow Data = GetGameConstantsData();
	UpdateMinValue(Data);
	UpdateMaxValue(Data);

	if (bCostIsInverted)
	{
		Update(MaxValue);
	}
	else
	{
		Update(MinValue);
	}

	OnValueReset();
}