#include "UMG/Menu/AbilityBuilder/AbilityBuilderMenu.h"
#include "World/AscensionGameInstance.h"

UAbilityBuilderMenu::UAbilityBuilderMenu(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

int32 UAbilityBuilderMenu::GetCurrentAttributesCost()
{
	// Add up attributes
	int32 Cost = 0;
	TArray<EAbilityAttributeName> Keys;
	AttributeWidgetsMap.GetKeys(Keys);
	for (int32 idx = 0; idx < Keys.Num(); idx++)
	{
		// TODO: cost should only look at displayed/utilized attributes
		Cost += AttributeWidgetsMap.FindChecked(Keys[idx])->GetCost();
	}
	return Cost;
}

int32 UAbilityBuilderMenu::GetCurrentAbilityCost()
{
	int32 Cost = BaseAbilityCost;

	// TODO: Add up affinities
	Cost += BaseAffinityCost;

	// Add the attribute's cost
	Cost += GetCurrentAttributesCost();

	return Cost;
}

int32 UAbilityBuilderMenu::GetTotalAbilitiesCost()
{
	int32 Total = 0;
	for (int32 Index = 0; Index < AbilityAttributesCostArray.Num(); Index++)
	{
		Total += AbilityAttributesCostArray[Index];
	}
	return Total;
}

void UAbilityBuilderMenu::RecalculateRemainingPoints()
{
	RemainingAbilityPoints = TotalAbilityPoints - GetTotalAbilitiesCost();
}

bool UAbilityBuilderMenu::CanAddAbility()
{
	if (AbilityAttributesArray.Num() < MaxNumberOfAbilities)
	{
		return true;
	}
	return false;
}

void UAbilityBuilderMenu::UpdateActiveAbility(int32 CurrentIndex, int32 NewIndex)
{
	if (CurrentIndex >= 0)
	{
		// Deactivate the old selection, activate the new 
		AbilityWidgetsArray[CurrentIndex]->ShowSelected(false);
		AbilityWidgetsArray[NewIndex]->ShowSelected(true);
	}
	else
	{
		// The first ability added
		AbilityWidgetsArray[NewIndex]->ShowSelected(true);
	}
}

bool UAbilityBuilderMenu::AddAbilityWidget(UAbilityWidget* AbilityWidget)
{
	if (CanAddAbility())
	{
		// Add the new widget, listener and attributes
		CurrentAbilityIndex++;
		AbilityWidget->Init(CurrentAbilityIndex);
		AbilityWidgetsArray.Add(AbilityWidget);
		AbilityWidget->OnAbilityWidgetClicked.AddUObject(this, &UAbilityBuilderMenu::SwitchAbility);
		AddAbilityAttributes();

		// Reset the sub widgets
		Reset();

		// Update the cost
		AbilityAttributesCostArray.Add(GetCurrentAbilityCost());
		
		// Update the builder display
		Refresh();

		// Update which ability is active
		UpdateActiveAbility(CurrentAbilityIndex - 1, CurrentAbilityIndex);

		return true;
	}
	return false;
}

void UAbilityBuilderMenu::RemoveAbilityWidget(int32 Index)
{
	// TODO: add remove ability
}

void UAbilityBuilderMenu::AddAbilityAttributes()
{
	AbilityAttributesArray.Add(NewObject<UAbilityAttributes>(this));
}

void UAbilityBuilderMenu::RemoveAbilityAttributes(int32 Index)
{
	// TODO: add remove ability
}

void UAbilityBuilderMenu::ResetAffinities()
{
	// Reset affinities
	TArray<EAffinity> Keys;
	AffinityWidgetsMap.GetKeys(Keys);
	for (int32 idx = 0; idx < Keys.Num(); idx++)
	{
		AffinityWidgetsMap.FindChecked(Keys[idx])->Reset();
	}
}

void UAbilityBuilderMenu::ResetAttributes()
{
	// Reset the attributes
	TArray<EAbilityAttributeName> Keys;
	AttributeWidgetsMap.GetKeys(Keys);
	for (int32 idx = 0; idx < Keys.Num(); idx++)
	{
		AttributeWidgetsMap.FindChecked(Keys[idx])->Reset();
	}
}

void UAbilityBuilderMenu::Reset()
{
	ResetAffinities();

	// Reset ability type
	AbilityTypeWidget->Reset();

	UpdateAbilityType(AbilityTypeWidget->GetValue());

	ResetAttributes();
}

bool UAbilityBuilderMenu::HasAllComponents()
{
	for (int32 Index = 0; Index < AbilityAttributesArray.Num(); Index++)
	{
		UAbilityAttributes* Attributes = AbilityAttributesArray[Index];
		if (Attributes->GetAbilityType() == nullptr || Attributes->GetAffinity() == EAffinity::None)
		{
			return false;
		}
	}
	return true;
}

bool UAbilityBuilderMenu::Validate()
{
	if (HasAllComponents() && GetTotalAbilitiesCost() <= TotalAbilityPoints)
	{
		// The form is valid but wasnt before, trigger a state change event
		if (!bIsValid)
		{
			bIsValid = true;
			OnValidationStateChange(bIsValid);
		}
	}
	else
	{
		// The form is not valid now but was before, trigger a state change event
		if (bIsValid)
		{
			bIsValid = false;
			OnValidationStateChange(bIsValid);
		}
	}
	return bIsValid;
}

void UAbilityBuilderMenu::Refresh()
{
	if (AbilityAttributesCostArray.IsValidIndex(CurrentAbilityIndex))
	{
		// Update the ability cost
		AbilityAttributesCostArray[CurrentAbilityIndex] = GetCurrentAbilityCost();

		// Recalculate cost
		RecalculateRemainingPoints();

		// Validate the current state
		Validate();
	}
}

void UAbilityBuilderMenu::SwitchAbility(int32 Index)
{
	// Update which ability is active
	UpdateActiveAbility(CurrentAbilityIndex, Index);

	// Update the current index
	CurrentAbilityIndex = Index;
	UAbilityAttributes* Attributes = AbilityAttributesArray[CurrentAbilityIndex];

	// Update the affinity
	ResetAffinities();
	if (Attributes->GetAffinity() != EAffinity::None)
	{
		AffinityWidgetsMap.FindChecked(Attributes->GetAffinity())->ToggleActive();
	}

	// Update the ability type
	if (Attributes->GetAbilityType())
	{
		AbilityTypeWidget->SetValue(Attributes->GetAbilityType()->GetFName().ToString());
	}

	// Update the attributes
	TArray<EAbilityAttributeName> Keys;
	AttributeWidgetsMap.GetKeys(Keys);
	for (int32 idx = 0; idx < Keys.Num(); idx++)
	{
		switch (Keys[idx])
		{
		case EAbilityAttributeName::Power:
			AttributeWidgetsMap.FindChecked(Keys[idx])->SetValue(Attributes->GetPower());
			break;
		case EAbilityAttributeName::Penetration:
			AttributeWidgetsMap.FindChecked(Keys[idx])->SetValue(Attributes->GetPenetration());
			break;
		case EAbilityAttributeName::Range:
			AttributeWidgetsMap.FindChecked(Keys[idx])->SetValue(Attributes->GetRange());
			break;
		case EAbilityAttributeName::Speed:
			AttributeWidgetsMap.FindChecked(Keys[idx])->SetValue(Attributes->GetSpeed());
			break;
		case EAbilityAttributeName::Size:
			AttributeWidgetsMap.FindChecked(Keys[idx])->SetValue(Attributes->GetSize());
			break;
		case EAbilityAttributeName::CastTime:
			AttributeWidgetsMap.FindChecked(Keys[idx])->SetValue(Attributes->GetCastTime());
			break;
		case EAbilityAttributeName::Duration:
			AttributeWidgetsMap.FindChecked(Keys[idx])->SetValue(Attributes->GetDuration());
			break;
		case EAbilityAttributeName::Delay:
			AttributeWidgetsMap.FindChecked(Keys[idx])->SetValue(Attributes->GetDelay());
			break;
		case EAbilityAttributeName::Cooldown:
			AttributeWidgetsMap.FindChecked(Keys[idx])->SetValue(Attributes->GetCooldown());
			break;
		case EAbilityAttributeName::Cost:
			AttributeWidgetsMap.FindChecked(Keys[idx])->SetValue(Attributes->GetCost());
			break;
		}
	}
}

void UAbilityBuilderMenu::UpdateAffinity(EAffinity Affinity, bool bIsActive)
{
	if (CurrentAbilityIndex >= 0)
	{
		UAbilityAttributes* Attributes = AbilityAttributesArray[CurrentAbilityIndex];

		// Deactivate the current affinity widget
		if (Attributes)
		{
			EAffinity CurrentAffinity = Attributes->GetAffinity();
			if (CurrentAffinity != EAffinity::None)
			{
				AffinityWidgetsMap.FindChecked(CurrentAffinity)->ToggleActive();
			}

			// Update the affinity
			Attributes->SetAffinity(Affinity);
		}

		// Refresh the builder display
		Refresh();
	}
}

void UAbilityBuilderMenu::UpdateAbilityType(FString AbilityType)
{
	if (CurrentAbilityIndex >= 0)
	{
		FAbilityBuilderTableRow Data = GetAbilityBuilderData(AbilityType);
		UAbilityAttributes* Attributes = AbilityAttributesArray[CurrentAbilityIndex];

		// Update the type
		if (Attributes && Data.AbilityType)
		{
			Attributes->SetAbilityType(Data.AbilityType);
		}

		// TODO: refresh attributes (which are used/displayed -> show/hide mechanic)
		ResetAttributes();

		// Refresh the builder display
		Refresh();
	}
}

void UAbilityBuilderMenu::UpdateAttributeValue(EAbilityAttributeName Name, int32 Value)
{
	if (CurrentAbilityIndex >= 0)
	{
		// Update the value of the specified attribute
		UAbilityAttributes* Attributes = AbilityAttributesArray[CurrentAbilityIndex];
		switch (Name)
		{
		case EAbilityAttributeName::Power:
			Attributes->SetPower(Value);
			break;
		case EAbilityAttributeName::Penetration:
			Attributes->SetPenetration(Value);
			break;
		case EAbilityAttributeName::Range:
			Attributes->SetRange(Value);
			break;
		case EAbilityAttributeName::Speed:
			Attributes->SetSpeed(Value);
			break;
		case EAbilityAttributeName::Size:
			Attributes->SetSize(Value);
			break;
		case EAbilityAttributeName::CastTime:
			Attributes->SetCastTime(Value);
			break;
		case EAbilityAttributeName::Duration:
			Attributes->SetDuration(Value);
			break;
		case EAbilityAttributeName::Delay:
			Attributes->SetDelay(Value);
			break;
		case EAbilityAttributeName::Cooldown:
			Attributes->SetCooldown(Value);
			break;
		case EAbilityAttributeName::Cost:
			Attributes->SetCost(Value);
			break;
		}

		// Refresh the builder display
		Refresh();
	}
}

void UAbilityBuilderMenu::UpdateGameInstance()
{
	UAscensionGameInstance* GameInstance = Cast<UAscensionGameInstance>(GetGameInstance());
	if (GameInstance)
	{
		for (int32 idx = 0; idx < AbilityAttributesArray.Num(); idx++)
		{
			if (AbilityAttributesArray[idx])
			{
				if (!GameInstance->AddAbilityAttributes(idx, AbilityAttributesArray[idx]))
				{
					UE_LOG(LogAscension, Error, TEXT("Failed to add ability %d to Game Instance."), idx);
				}
			}
		}
	}
}

void UAbilityBuilderMenu::InitAffinityWidget(UAffinityWidget* AffinityWidget, EAffinity Affinity)
{
	AffinityWidget->Init(Affinity);
	AffinityWidget->OnAffinityWidgetClicked.AddUObject(this, &UAbilityBuilderMenu::UpdateAffinity);
	AffinityWidgetsMap.Add(Affinity, AffinityWidget);
}

void UAbilityBuilderMenu::InitAbilityTypeWidget(UAbilityTypeWidget* NewAbilityTypeWidget)
{
	AbilityTypeWidget = NewAbilityTypeWidget;
	AbilityTypeWidget->OnAbilityTypeValueChanged.AddUObject(this, &UAbilityBuilderMenu::UpdateAbilityType);
}

void UAbilityBuilderMenu::InitAttributeWidget(UAttributeWidget* AttributeWidget, EAbilityAttributeName Name)
{
	AttributeWidget->Init(Name);
	AttributeWidget->OnAttributeWidgetValueChanged.AddUObject(this, &UAbilityBuilderMenu::UpdateAttributeValue);
	AttributeWidgetsMap.Add(Name, AttributeWidget);
}

void UAbilityBuilderMenu::Init()
{
	// Read constants from the data table
	FGameConstantsTableRow Data = GetGameConstantsData();
	BaseAbilityCost = Data.BaseAbilityCost;
	BaseAffinityCost = Data.BaseAffinityCost;
	MaxNumberOfAbilities = Data.MaxNumAbilities;
	TotalAbilityPoints = Data.MaxStartingAbilityPoints;
	RemainingAbilityPoints = TotalAbilityPoints;
	OnValidationStateChange(false);
}