#include "UMG/Menu/AbilityBuilder/AbilityWidget.h"

UAbilityWidget::UAbilityWidget(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

void UAbilityWidget::Init(int32 NewId)
{
	Id = NewId;
}

void UAbilityWidget::Update()
{
	OnAbilityWidgetClicked.Broadcast(Id);
}

void UAbilityWidget::ShowSelected(bool bIsSelected)
{
	// Update the current background color
	if (bIsSelected)
	{
		CurrentColor = SelectedColor;
	}
	else
	{
		CurrentColor = BaseColor;
	}
}