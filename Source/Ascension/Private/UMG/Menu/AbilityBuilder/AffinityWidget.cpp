#include "UMG/Menu/AbilityBuilder/AffinityWidget.h"

UAffinityWidget::UAffinityWidget(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

void UAffinityWidget::ToggleHighlight(bool bShouldHighlight)
{
	if (bShouldHighlight)
	{
		CurrentColor = HighlightColor;
	}
	else
	{
		CurrentColor = BaseColor;
	}
}

FText UAffinityWidget::GetWidgetName()
{
	return FText::FromString(GET_STRING_FROM_ENUM("EAffinity", Affinity));
}

bool UAffinityWidget::IsActive()
{
	return bIsActive;
}

void UAffinityWidget::ToggleActive()
{
	bIsActive = !bIsActive;
	ToggleHighlight(bIsActive);
}

void UAffinityWidget::Init(EAffinity NewAffinity)
{
	Affinity = NewAffinity;
	IconTexture = GetGameConstantsData().AffinityIconMap.FindChecked(Affinity);
}

void UAffinityWidget::Update()
{
	ToggleActive();

	// Fire/Call ability builder update
	OnAffinityWidgetClicked.Broadcast(Affinity, bIsActive);
}

void UAffinityWidget::Reset()
{
	bIsActive = false;
	ToggleHighlight(bIsActive);
}