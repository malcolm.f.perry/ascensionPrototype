#include "UMG/Menu/AbilityBuilder/AbilityTypeWidget.h"

UAbilityTypeWidget::UAbilityTypeWidget(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

void UAbilityTypeWidget::Update(FString NewOption)
{
	if (SelectedOption != NewOption)
	{
		SelectedOption = NewOption;
		OnAbilityTypeValueChanged.Broadcast(NewOption);
	}
}

FString UAbilityTypeWidget::GetValue()
{
	return SelectedOption;
}

void UAbilityTypeWidget::SetValue(FString NewOption)
{
	SelectedOption = NewOption;
	OnValueSet();
}

void UAbilityTypeWidget::Reset()
{
	OnValueReset();
}