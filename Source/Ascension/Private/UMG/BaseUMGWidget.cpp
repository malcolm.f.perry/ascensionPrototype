#include "UMG/BaseUMGWidget.h"

void UBaseUMGWidget::SwitchGameMenu(TSubclassOf<UBaseUMGWidget> WidgetClass)
{
	RemoveFromParent();
	UUserWidget* NewWidget = CreateWidget(GetOwningPlayer(), WidgetClass);
	NewWidget->AddToViewport();
}

FAbilityBuilderTableRow UBaseUMGWidget::GetAbilityBuilderData(FString RowName)
{
	FAbilityBuilderTableRow* Data = GetRowData<FAbilityBuilderTableRow>(GetWorld(), EDataTableName::AbilityBuilder, RowName);
	return *Data;
}

FGameConstantsTableRow UBaseUMGWidget::GetGameConstantsData()
{
	FGameConstantsTableRow* Data = GetRowData<FGameConstantsTableRow>(GetWorld(), EDataTableName::GameConstants, "Constants");
	return *Data;
}
